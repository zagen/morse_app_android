package mu.zz.zagen.morse;

import java.io.IOException;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.view.View.OnClickListener;
import mu.zz.zagen.morse.General.Utilities;

/**
 * Activity provide music player interface for listening song helping out learn international Morse code
 * @author zagen
 *
 */
public class BonusSingActivity extends Activity implements OnClickListener, OnCompletionListener, SeekBar.OnSeekBarChangeListener{

	/**
	 * animation of working radio
	 */
	private AnimationDrawable animation;
	
	/**
	 * media player for our song
	 */
	private MediaPlayer mPlayer;
	
	/**
	 * seekbar of playing progress
	 */
	private SeekBar songSeekBar;
	
	/**
	 *  Handler to update UI timer, progress bar
	 */
    private Handler mHandler = new Handler();
    
    /**
     * text field keep current duration of playing
     */
    private TextView songCurrentDurationLabel;
    
    /**
     * total duration of song
     */
    private TextView songTotalDurationLabel;
    
    /**
     * provide convert method for our player
     */
    private Utilities utils = new Utilities();
    
    /**
     * is playing thread is running
     */
    private Boolean wasRunning = false;
    
    /**
     * here stores app preferences like volume
     */
	private SharedPreferences preferences;
	
	/**
	 *for changing stream preferences 
	 */
	private AudioManager am;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//full screen no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.bsa_layout);
		
		//setting names for refill and ok buttons
		Button refillButton = (Button)findViewById(R.id.bsa_repeat_button);
		refillButton.setText("\u21BA");
		Button okButton = (Button)findViewById(R.id.bsa_ok_button);
		okButton.setText("\u2714");
		
		//get display metric(width) for calculating views sizes
		Display display = getWindowManager().getDefaultDisplay(); 
		@SuppressWarnings("deprecation")
		int width = display.getWidth();  // deprecated
		
		//get view of radio
		Button radioBtn = (Button)findViewById(R.id.bsa_radio);
		radioBtn.setBackgroundResource(R.anim.radio);
		//and its animation
		animation = (AnimationDrawable)radioBtn.getBackground();
		
		//set width of radio a little bit less then width of the screen
		width = width * 3 / 4;
		RelativeLayout.LayoutParams layoutParamsR = (RelativeLayout.LayoutParams) radioBtn.getLayoutParams();
		layoutParamsR.width = width - 10;
		layoutParamsR.height = width*3/4 ;
		radioBtn.setLayoutParams(layoutParamsR);
		
		songSeekBar = (SeekBar)findViewById(R.id.bsa_seekbar);
		songSeekBar.setOnSeekBarChangeListener(this);
		songCurrentDurationLabel = (TextView)findViewById(R.id.bsa_time_current_txt);
		songTotalDurationLabel = (TextView)findViewById(R.id.bsa_time_all_txt);
		
		///get preferences for app
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		
		mPlayer = new MediaPlayer();
		//get descriptor of file for playing(from assets)
		AssetFileDescriptor afd = getResources().openRawResourceFd(R.raw.bonus);
		mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		
		//try to prepare resource
		try {
			mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getDeclaredLength());
			mPlayer.prepare();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//set listener that will be called when playing'll be over
		mPlayer.setOnCompletionListener(this);
		updateProgressBar();
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	/**
	 * implementation view.onclicklistener interface
	 */
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bsa_radio:
			//check all preparations and play song if it possible. if player already work, then paused it
			if(mPlayer != null)
			{
				if(mPlayer.isPlaying()){
					mPlayer.pause();
					animation.stop();
				}else{
					mPlayer.start();
					animation.start();
				}
			}
			break;
		case R.id.bsa_repeat_button:
			//reset progress and start play
			if(mPlayer != null)
			{
				mPlayer.seekTo(0);
				if(!mPlayer.isPlaying()){
					mPlayer.start();
					animation.start();
				}
			}
			break;
		case R.id.bsa_ok_button:
			//get out of activity
			finish();
			break;
		default:
			break;
		}
		
	}

	/**
     * Update timer on seekbar
     * */
 
 
    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
           public void run() {
        	   
               long totalDuration = mPlayer.getDuration();
               long currentDuration = mPlayer.getCurrentPosition();
 
               // Displaying Total Duration time
               songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
               // Displaying time completed playing
               songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));
 
               // Updating progress bar
               int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));
               //Log.d("Progress", ""+progress);
               songSeekBar.setProgress(progress);
 
               // Running this thread after 100 milliseconds
               mHandler.postDelayed(this, 100);
           }
        };
    public void updateProgressBar() {
         mHandler.postDelayed(mUpdateTimeTask, 100);
    }   
    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
 
    }
 
    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }
 
    /**
     * When user stops moving the progress handler
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mPlayer.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
 
        // forward or backward to certain seconds
        mPlayer.seekTo(currentPosition);
 
        // update timer progress again
        updateProgressBar();
    }

	@Override
	public void onCompletion(MediaPlayer mp) {
		animation.stop();

		
	}
	
	@Override
	public void onPause(){
		super.onPause();
		mPlayer.pause();
		animation.stop();
		wasRunning = true;
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		super.onPause();
	}
	@Override
	public void onResume(){
		super.onResume();
		if(wasRunning){
			mPlayer.start();
			animation.start();
		}
	}
	
	
	@Override
	public void onDestroy(){
	    super.onDestroy();

	    //stop playing and release resources
	    mHandler.removeCallbacks(mUpdateTimeTask);
	    mPlayer.release();
	}
	 
}
