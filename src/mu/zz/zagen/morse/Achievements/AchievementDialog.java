/**
 * 
 */
//package Achievements;
package mu.zz.zagen.morse.Achievements;


import java.io.ByteArrayOutputStream;
import java.util.List;

import mu.zz.zagen.morse.R;
import mu.zz.zagen.morse.General.PositionView;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

/**
 * @author zagen
 *
 */
public class AchievementDialog extends Dialog {

	private List<Achievement> achievements = null;
	private ImageSwitcher imgSwitcher 	= null;
	private TextView titleTxt			= null;
	private TextView nameTxt			= null;
	private TextView descriptionTxt		= null;
	private int currentIndex = 0;
	private final String GOOGLEPLAYLINK = "https://play.google.com/store/apps/details?id=mu.zz.zagen.morse"; 
	
	private Animation right_in = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_right);
	private Animation left_in = AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left);
	private Animation right_out = AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_out_right);
	private Animation left_out = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_left);
	
	private PositionView posView 		= null;
	
	public AchievementDialog init(boolean fromSettings)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.achievement_dialog_layout);
		imgSwitcher = (ImageSwitcher)findViewById(R.id.achievementImgSwt);
		titleTxt  = (TextView)findViewById(R.id.achievementTitleTxt);
		nameTxt  = (TextView)findViewById(R.id.achievementNameTxt);
		descriptionTxt  = (TextView)findViewById(R.id.achievementDescriptionTxt);
		posView = (PositionView)findViewById(R.id.achievementPosView);
		
		imgSwitcher.setFactory(new ViewFactory() {
			
			@Override
			public View makeView() {
				ImageView imageView = new ImageView(getContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
				return  imageView;
			}
		});
		Animation in = AnimationUtils.loadAnimation(getContext(),R.anim.slide_in_down);
       
        // set the animation type to imageSwitcher
        imgSwitcher.setInAnimation(in);
        currentIndex = switchAchievement(currentIndex);
        
        if(achievements == null || achievements.size() == 0){
        	titleTxt.setText(R.string.no_achievement_dialog_title);
        	descriptionTxt.setText(R.string.no_achievement_description);
        	nameTxt.setText("");
        	imgSwitcher.setImageResource(R.drawable.no_trophy);
        }else
        	if(achievements.size() > 1){
	        	titleTxt.setText(String
	        			.format(getContext().getString(R.string.new_achievements_dialog_title), achievements.size()));
	        	posView.setPositionCount(achievements.size());
        	}
        if(fromSettings)
        {
        	titleTxt.setText(R.string.achievements_dialog_title);
        }
        
		return this;
	}
	protected AchievementDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener, List<Achievement> achievements) {
		super(context, cancelable, cancelListener);
		this.achievements = achievements;  
	}
	public AchievementDialog(Context context, int theme, List<Achievement> achievements) {
		super(context, theme);
		this.achievements = achievements;
		}
	public AchievementDialog(Context context, List<Achievement> achievements) {
		super(context);
		this.achievements = achievements;
	}
	
	public int switchAchievement(int index){
		if(achievements != null && achievements.size() > 0)
		{
			if(index > achievements.size() - 1)
				index = 0;
			if(index < 0)
				index = achievements.size() - 1;
			Achievement currentAchievement = achievements.get(index); 
			nameTxt.setText(currentAchievement.name());
			imgSwitcher.setImageDrawable(new BitmapDrawable(getContext().getResources(), currentAchievement.getPictureTrophy()));
			descriptionTxt.setText(currentAchievement.desription());
		}
		return index;
	}
	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	}
	
	private float x1, x2, y1, y2;
	{
		x1 = x2 = y1 = y2 = -1;
	}
	private long touchStartTime = 0;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//for detecting finger movement direction
		float  dx, dy;
		
		if(achievements == null || achievements.size() == 0)
			return super.onTouchEvent(event);
		
		switch(event.getAction()) {
		
		    case(MotionEvent.ACTION_DOWN):
		        x1 = event.getX();
		        y1 = event.getY();
		        touchStartTime = System.currentTimeMillis();
		        break;
		    case(MotionEvent.ACTION_UP):{
		    
		    	if(touchStartTime != 0 && System.currentTimeMillis() - touchStartTime > 700)
		    	{
		    		//sharing if long touch 
		    		final Intent intent = new Intent(Intent.ACTION_SEND);
		    		intent.setType("text/plain");
		    		intent.putExtra(Intent.EXTRA_SUBJECT, getContext().getString(R.string.morse_app_achievement));
		    		intent.putExtra(Intent.EXTRA_TEXT, 
		    				String.format(getContext().getString(R.string.share_achievement_text),
		    										achievements.get(currentIndex).name(), GOOGLEPLAYLINK));
		    		getContext().startActivity(Intent.createChooser(intent, getContext().getString(R.string.share_with_your_friend)));
		    		x1 = y1 = -1;
		    		touchStartTime = 0;
		    	}else
		    		
				    if(x1 != -1 && y1 != -1 && achievements.size() != 1)	
					{
					    x2 = event.getX();
					    y2 = event.getY();
					    dx = x2 - x1;
					    dy = y2 - y1;
			
					    // Use dx and dy to determine the direction
					    if(Math.abs(dx) > Math.abs(dy)) {
					    	if(dx > 0){
					    		imgSwitcher.setOutAnimation(right_out);
					    		imgSwitcher.setInAnimation(left_in);
					    		currentIndex = switchAchievement(currentIndex - 1);
					    		posView.previous();
					    	}
					        else{
					    		imgSwitcher.setOutAnimation(left_out);
					    		imgSwitcher.setInAnimation(right_in);
					        	currentIndex = switchAchievement(currentIndex + 1);
					        	posView.next();
					        }
					    }
					}
		    }
		}
		return super.onTouchEvent(event);
	}
}
