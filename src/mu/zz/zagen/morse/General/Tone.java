package mu.zz.zagen.morse.General;
/**
 * 
 */

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

/**
 * @author zagen
 *
 */
public class Tone {

	private double duration ;                			// seconds
	private double frequence;           				// hz
	private int sampleRate = 8000;             			 // a number

	private double dnumSamples;
    
	private int numSamples;
	private double sample[];
	private byte generatedSnd[];


	private AudioTrack audioTrack = null;                                   // Get audio track
    public Tone(double duration, double frequence){

    	this.duration = duration;                // mseconds
    	this.frequence = frequence;           				// hz
    	
    	dnumSamples = Math.ceil(this.duration * sampleRate/1000);
        
    	numSamples = (int) dnumSamples;
    	sample= new double[numSamples];
    	generatedSnd = new byte[2 * numSamples];

    	numSamples /=2;

        for (int i = 0; i < numSamples; ++i) {      // Fill the sample array
            sample[i] = Math.sin(this.frequence * 2 * Math.PI * i / (sampleRate));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalized.
        int idx = 0;
        int i = 0 ;

        int ramp = numSamples / 20 ;                                    // Amplitude ramp as a percent of sample count


        for (i = 0; i< ramp; ++i) {                                     // Ramp amplitude up (to avoid clicks)
            double dVal = sample[i];
                                                                        // Ramp up to maximum
            final short val = (short) ((dVal * 32767 * i/ramp));
                                                                        // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }


        for (; i< numSamples - ramp; ++i) {                        // Max amplitude for most of the samples
            double dVal = sample[i];
                                                                        // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
                                                                        // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        for (; i< numSamples; ++i) {                               // Ramp amplitude down
            double dVal = sample[i];
                                                                        // Ramp down to zero
            final short val = (short) ((dVal * 32767 * (numSamples - i - 1)/(ramp)));
                                                                        // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
        numSamples *=2;
        
    }


   
    public void play(){
        try {
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, (int)numSamples*2,
                    AudioTrack.MODE_STATIC);
            audioTrack.write(generatedSnd, 0, generatedSnd.length);     // Load the track
            audioTrack.play();                                          // Play the track
        }
        catch (Exception e){
            e.printStackTrace();
        }

        int x =0;
        do{                                                     // Montior playback to find when done
             if (audioTrack != null) 
                 x = audioTrack.getPlaybackHeadPosition(); 
             else 
                 x = numSamples;            
        }while (x<numSamples);
        
        if (audioTrack != null) audioTrack.release();           // Track play done. Release track. }

    }
}
     
