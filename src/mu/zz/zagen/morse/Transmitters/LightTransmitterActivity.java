package mu.zz.zagen.morse.Transmitters;

import mu.zz.zagen.morse.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;


public class LightTransmitterActivity extends Activity {

	public static final String BRIGHTNESS_ID = "brightness";
	public static final String MORSECODE_ID = "message";
	public static final String DURATION_ID = "duration";
	public static final String IS_LOOP = "is_loop";
	
	private String encodedText;
	private int pos;
	private int duration;
	private boolean isLoop = false;
	
	
	private LinearLayout lta_layout;
	@SuppressLint("HandlerLeak")
	private final Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg)
		{
			boolean isWhite = msg.getData().getBoolean("on"); 
			if(isWhite)
			{
				lta_layout.setBackgroundColor(Color.WHITE);
			}
			else
			{
				lta_layout.setBackgroundColor(Color.BLACK);
			}
		}
	};
	//
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//made our window on full screen and without any text
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.lta_layout);
		lta_layout = (LinearLayout)findViewById(R.id.lta);
		
		final WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = getIntent().getExtras().getFloat(BRIGHTNESS_ID);
        isLoop = getIntent().getExtras().getBoolean(IS_LOOP);
        getWindow().setAttributes(layout);
        
	    Bundle extras = getIntent().getExtras();
		encodedText = extras.getString(MORSECODE_ID);
		duration = extras.getInt(DURATION_ID);
		
	}

	public void onStart()
	{
		super.onStart();
		Thread background = new Thread( new Runnable(){

			@Override
			public void run() {
				do {
					pos = 0;
					while(pos < encodedText.length())
					{
						try{
							if(pos == 0){
								Thread.sleep(1000);
							}
							if(encodedText.charAt(pos) == '-')
							{	
								Message msgOn = handler.obtainMessage();
								Bundle msgOnData = new Bundle();
								msgOnData.putBoolean("on", true);
								msgOn.setData(msgOnData);
								handler.sendMessage(msgOn);
								Thread.sleep(duration * 3 );
								
								Message msgOff = handler.obtainMessage();
								Bundle msgOffData = new Bundle();
								msgOffData.putBoolean("on", false);
								msgOff.setData(msgOffData);
								handler.sendMessage(msgOff);
								Thread.sleep(duration );
								
							}else{ if(encodedText.charAt(pos) == '.')
							{
								Message msgOn = handler.obtainMessage();
								Bundle msgOnData = new Bundle();
								msgOnData.putBoolean("on", true);
								msgOn.setData(msgOnData);
								handler.sendMessage(msgOn);
								Thread.sleep(duration );
								
								Message msgOff = handler.obtainMessage();
								Bundle msgOffData = new Bundle();
								msgOffData.putBoolean("on", false);
								msgOff.setData(msgOffData);
								handler.sendMessage(msgOff);
								Thread.sleep(duration );
								  					
							}else{ if(encodedText.charAt(pos) == ' '){
								Thread.sleep(duration*3);
							}else{
								Thread.sleep(duration* 7);
								    
								}}}
						}catch(InterruptedException e)
						{
							Log.e("Error", "Thread interrupted");
						}
						pos++;
					}
					
				}while(isLoop);
				LightTransmitterActivity.this.finish();
			
			}
		});
		background.start();
		
	}

}
