/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.TextCodec;
import mu.zz.zagen.morse.General.WordReader;
import mu.zz.zagen.morse.Transmitters.ToneTransmitter;
import mu.zz.zagen.morse.Transmitters.Transmitter;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * @author zagen
 *
 */
public class PlayingFileService extends Service {

	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	
	protected ToneTransmitter transmitter;
	protected TextCodec 	  codec;
	public static final String EXTRA_STRING_TYPE = "pfsActionType";
	
	public static final int EXTRA_TYPE_ID_PLAY = 100;
	public static final int EXTRA_TYPE_ID_PAUSE = 101;
	public static final int EXTRA_TYPE_ID_STOP = 109;
	public static final int EXTRA_TYPE_ID_PLAY_FORWARD_1 = 102;
	public static final int EXTRA_TYPE_ID_PLAY_FORWARD_2 = 103;
	public static final int EXTRA_TYPE_ID_PLAY_BACKWARD_1 = 104;
	public static final int EXTRA_TYPE_ID_PLAY_BACKWARD_2 = 105;
	public static final int EXTRA_TYPE_ID_OPEN_URI = 106;
	public static final int EXTRA_TYPE_ID_GOTO_PERCENT = 107;
	public static final int EXTRA_TYPE_ID_GOTO_POSITION = 110;
	public static final int EXTRA_TYPE_ID_STOP_SERVICE = 108;
	
	
	public static final String EXTRA_STRING_PERCENT = "pfsExtraPercent";
	public static final String EXTRA_STRING_FILE_URI = "pfsExtraFileUri";
	public static final String EXTRA_STRING_POSITION = "psfExtraPosition";
	
	public static final String ACTION_STRING_SERVICE = "pfsActionService";
	
	private String currentWord;
	
	private int sizeOfFastWindBig = 5;
	
	private Transmitter.OnTransmittanceEndedListener mainListener = new Transmitter.OnTransmittanceEndedListener() {
		
		@Override
		public void end() {
			if(!isPaused && wr != null){
				setCurrentWord(wr.nextWord());
				play();
				sendCurrentPercent(wr.getCurrentPercent());
			}else{
				sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PAUSED);
			}
		}
	};
	
	
	private WordReader wr = null;
	private SharedPreferences preferences;
	private boolean isPaused = true;

	private BroadcastReceiver broadcastReceiver;
	
	@Override
	public IBinder onBind(Intent intent) {

		return null;
	}
	@Override
	public void onCreate() {
		codec = new TextCodec();
		transmitter = new ToneTransmitter(this, codec, mainListener);
		codec.setLocale(TextCodec.ALL);
		
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		
		broadcastReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, final Intent intent) {
	        	if(intent.getAction().equals(ACTION_STRING_SERVICE)){
	        		Bundle bundle = intent.getExtras();
	        		switch (bundle.getInt(EXTRA_STRING_TYPE)) {
	        		
					case EXTRA_TYPE_ID_PLAY:
						if(wr != null){
							play();							
						}
						break;

					case EXTRA_TYPE_ID_PAUSE:
						isPaused = true;
		        		transmitter.pause();
		        		sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PAUSED);
						//Toast.makeText(getApplicationContext(), "EXTRA_TYPE_ID_PAUSE", Toast.LENGTH_SHORT).show();
						break;
					case EXTRA_TYPE_ID_STOP:
						isPaused = true;
						transmitter.stop();
						break;
						
					case EXTRA_TYPE_ID_PLAY_BACKWARD_1:
						
						if(wr != null){
							if(!isPaused){
								transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
									
									@Override
									public void end() {
											transmitter.setTransmittanceEndedListener(mainListener);
											setCurrentWord(wr.previousWord());
											try {
												Thread.sleep(500);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											play();
									}
								});
								transmitter.stop();
							}else{
								if(transmitter.isPaused()){
									transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
										
										@Override
										public void end() {
											setCurrentWord(wr.previousWord());
											transmitter.setTransmittanceEndedListener(mainListener);
										}
									});
									transmitter.stop();									
								}else{
									setCurrentWord(wr.previousWord());
								}
							}
							transmitter.stop();
						}
						//Toast.makeText(getApplicationContext(), "EXTRA_TYPE_ID_PLAY_BACKWARD_1", Toast.LENGTH_SHORT).show();
						break;
						
					case EXTRA_TYPE_ID_PLAY_BACKWARD_2:

						if(wr != null){
							if(!isPaused){
								transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
									
									@Override
									public void end() {
											transmitter.setTransmittanceEndedListener(mainListener);
											for(int i = 0; i < sizeOfFastWindBig; i++){
												setCurrentWord(wr.previousWord());
											}
											try {
												Thread.sleep(500);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											play();
									}
								});
								transmitter.stop();
							}else{
								if(transmitter.isPaused()){
									transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
										
										@Override
										public void end() {
											for(int i = 0; i < sizeOfFastWindBig; i++){
												setCurrentWord(wr.previousWord());
											}
											transmitter.setTransmittanceEndedListener(mainListener);
										}
									});
									transmitter.stop();									
								}else{
									for(int i = 0; i < sizeOfFastWindBig; i++){
										setCurrentWord(wr.previousWord());
									}
								}
							}
							transmitter.stop();
						}	
						//Toast.makeText(getApplicationContext(), "EXTRA_TYPE_ID_PLAY_BACKWARD_2", Toast.LENGTH_SHORT).show();
						break;
						
					case EXTRA_TYPE_ID_PLAY_FORWARD_1:
						if(wr != null){
							if(!isPaused){
								transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
									
									@Override
									public void end() {
											transmitter.setTransmittanceEndedListener(mainListener);
											setCurrentWord(wr.nextWord());
											try {
												Thread.sleep(500);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											play();
									}
								});
								transmitter.stop();
							}else{
								if(transmitter.isPaused()){
									transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
										
										@Override
										public void end() {
											setCurrentWord(wr.nextWord());
											transmitter.setTransmittanceEndedListener(mainListener);
										}
									});
									transmitter.stop();									
								}else{
									setCurrentWord(wr.nextWord());
								}
							}
							transmitter.stop();
						}
						break;
					case EXTRA_TYPE_ID_PLAY_FORWARD_2:
						if(wr != null){
							if(!isPaused){
								transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
									
									@Override
									public void end() {
											transmitter.setTransmittanceEndedListener(mainListener);
											for(int i = 0; i < sizeOfFastWindBig; i++){
												setCurrentWord(wr.nextWord());
											}
											try {
												Thread.sleep(500);
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											play();
									}
								});
								transmitter.stop();
							}else{
								if(transmitter.isPaused()){
									transmitter.setTransmittanceEndedListener(new Transmitter.OnTransmittanceEndedListener() {
										
										@Override
										public void end() {
											for(int i = 0; i < sizeOfFastWindBig; i++){
												setCurrentWord(wr.nextWord());
											}
											transmitter.setTransmittanceEndedListener(mainListener);
										}
									});
									transmitter.stop();									
								}else{
									for(int i = 0; i < sizeOfFastWindBig; i++){
										setCurrentWord(wr.nextWord());
									}
								}
							}
							transmitter.stop();
						}	
						break;
					case EXTRA_TYPE_ID_GOTO_PERCENT:
						double percent = bundle.getDouble(EXTRA_STRING_PERCENT);
						if(wr != null){
							transmitter.setTransmittanceEndedListener(null);
							transmitter.stop();
							wr.moveToPercent(percent);
							setCurrentWord(wr.nextWord());
							isPaused = true;
							transmitter.setTransmittanceEndedListener(mainListener);
							sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PAUSED);
						}
						/*Toast.makeText(getApplicationContext(), 
								"EXTRA_TYPE_ID_GOTO_PERCENT " + Double.toString(bundle.getDouble(EXTRA_STRING_PERCENT)), 
								Toast.LENGTH_SHORT).show();*/
						break;
					case EXTRA_TYPE_ID_GOTO_POSITION:
						long position = bundle.getLong(EXTRA_STRING_POSITION);
						try {
							wr.setCurrentPosition(position);
							setCurrentWord(wr.nextWord());
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case EXTRA_TYPE_ID_OPEN_URI:
						
						Intent responseIntent = new Intent();
						responseIntent.setAction(ListeningActivity.ACTION_STRING_ACTIVITY);
						responseIntent.putExtra(ListeningActivity.EXTRA_STRING_TYPE,
												ListeningActivity.EXTRA_TYPE_ID_FILE_STATUS);
						String filename = bundle.getString(EXTRA_STRING_FILE_URI);
						
						wr = new WordReader(filename);
						if(wr == null){

							responseIntent.putExtra(ListeningActivity.EXTRA_FILE_STATUS_STRING, 
													ListeningActivity.EXTRA_FILE_STATUS_ID_FILE_FAILURE);
							
						}else if(wr.isEmpty()){
							responseIntent.putExtra(ListeningActivity.EXTRA_FILE_STATUS_STRING, 
									ListeningActivity.EXTRA_FILE_STATUS_ID_FILE_EMPTY);
						}else{
						
							SharedPreferences.Editor editor = preferences.edit();
							editor.putString(SettingsActivity.LAST_FILE_URI_STRING, filename);
							editor.commit();
							
							transmitter.setTransmittanceEndedListener(null);
							transmitter.stop();
							currentWord = "";
							isPaused = true;
							setCurrentWord(wr.nextWord());
							
							responseIntent.putExtra(ListeningActivity.EXTRA_FILE_STATUS_STRING, 
									ListeningActivity.EXTRA_FILE_STATUS_ID_FILE_OPENED);
							transmitter.setTransmittanceEndedListener(mainListener);
							sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PAUSED);
						}
						sendBroadcast(responseIntent);

						break;
					case EXTRA_TYPE_ID_STOP_SERVICE:
						isPaused = true;
						transmitter.setTransmittanceEndedListener(null);
						transmitter.stop();
						stopSelf();
						break;
					default:
						break;
					}
	        			
	        	}
	        }
	    };
	    Log.d("Service", "onCreate");

	    if (broadcastReceiver != null) {
	    	//Create an intent filter to listen to the broadcast sent with the action "ACTION_STRING_SERVICE"
	    	IntentFilter intentFilter = new IntentFilter(ACTION_STRING_SERVICE);
	    	registerReceiver(broadcastReceiver, intentFilter);
	    }


	
		super.onCreate();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Intent intentStart  = new Intent();
		intentStart.setAction(ListeningActivity.ACTION_STRING_ACTIVITY);
		intentStart.putExtra(ListeningActivity.EXTRA_STRING_TYPE, ListeningActivity.EXTRA_TYPE_ID_SERVICE_STARTED);
		sendBroadcast(intentStart);
	    return Service.START_NOT_STICKY;
	}
	
	private void sendCurrentWord(String word){
		Intent intent = new  Intent();
		intent.setAction(ListeningActivity.ACTION_STRING_ACTIVITY);
		intent.putExtra(ListeningActivity.EXTRA_STRING_TYPE, ListeningActivity.EXTRA_TYPE_ID_CURRENT_WORD);
		intent.putExtra(ListeningActivity.EXTRA_STRING_CURRENT_WORD, word);
		sendBroadcast(intent);
	}
	
	private void sendCurrentPercent(double percent){
		Intent intent = new  Intent();
		intent.setAction(ListeningActivity.ACTION_STRING_ACTIVITY);
		intent.putExtra(ListeningActivity.EXTRA_STRING_TYPE, ListeningActivity.EXTRA_TYPE_ID_CURRENT_PERCENT);
		intent.putExtra(ListeningActivity.EXTRA_STRING_CURRENT_PERCENT, percent);
		sendBroadcast(intent);
	}
	private void sendPlayStatus(int isPlayed){
		Intent intent = new  Intent();
		intent.setAction(ListeningActivity.ACTION_STRING_ACTIVITY);
		intent.putExtra(ListeningActivity.EXTRA_STRING_TYPE, ListeningActivity.EXTRA_TYPE_ID_PLAY_STATUS);
		
		intent.putExtra(ListeningActivity.EXTRA_PLAY_STATUS_STRING, isPlayed);
		
		sendBroadcast(intent);
	}
	
	private void play(){
		sendCurrentPercent(wr.getCurrentPercent());
		if( transmitter.isPaused()){
			transmitter.transmit();
			sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PLAYING);
			isPaused = false;
		}else{
			if(currentWord.trim().length() != 0){
				codec.setText(currentWord);
				transmitter.transmit();
				sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PLAYING);
				isPaused = false;
			}else if(wr.isEnded() || wr.isBegin()){
				wr.reset();
				sendPlayStatus(ListeningActivity.EXTRA_PLAY_STATUS_ID_PAUSED);
				setCurrentWord(wr.nextWord());
			}
		}
	}
	@Override
	public void onDestroy() {
		isPaused = true;
		transmitter.setTransmittanceEndedListener(null);
		transmitter.stop();

		long position = 0;
		
		try{
			position = wr.getPreviousWordPosition();
		}catch(Exception e){
			position = 0;
			e.printStackTrace();
		}
		
		SharedPreferences.Editor editor = preferences.edit();
		editor.putLong(SettingsActivity.LAST_FILE_POSITION_LONG, position);
		editor.commit();
		
		if(broadcastReceiver != null){
			unregisterReceiver(broadcastReceiver);
		}
		super.onDestroy();
	}
	
	/**
	 * @return the currentWord
	 */
	public String getCurrentWord() {
		return currentWord;
	}
	/**
	 * @param currentWord the currentWord to set
	 */
	public void setCurrentWord(String currentWord) {
		this.currentWord = currentWord;
		sendCurrentWord(currentWord);
		sendCurrentPercent(wr.getCurrentPercent());
	}
	/**
	 * @return the sizeOfFastWindBig
	 */
	public int getSizeOfFastWindBig() {
		return sizeOfFastWindBig;
	}
	/**
	 * @param sizeOfFastWindBig the sizeOfFastWindBig to set
	 */
	public void setSizeOfFastWindBig(int sizeOfFastWindBig) {
		if(sizeOfFastWindBig >= 5)
			this.sizeOfFastWindBig = sizeOfFastWindBig;
	}

}
