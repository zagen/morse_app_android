/**
 * 
 */
package mu.zz.zagen.morse.Achievements;

import mu.zz.zagen.morse.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @author zagen
 *
 */
public class NewRecordAchievement extends Achievement {
	public final static String NAME = "NEW_RECORD_NAME";

	public NewRecordAchievement(Context context, int rightAnswerAmount,
			int totalCharAmount) {
		super(context, rightAnswerAmount, totalCharAmount);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#name()
	 */
	@Override
	public String name() {
		return context.getString(R.string.new_record_trophy_name);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#getPictureTrophy()
	 */
	@Override
	public Bitmap getPictureTrophy() {
		return BitmapFactory.decodeResource(context.getResources(), R.drawable.new_record_trophy);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#desription()
	 */
	@Override
	public String desription() {
		return String.format(context.getString(R.string.new_record_trophy_description), rightAnswerAmount);
	}

}
