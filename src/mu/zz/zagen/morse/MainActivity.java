package mu.zz.zagen.morse;


import mu.zz.zagen.morse.Achievements.Achievement;
import mu.zz.zagen.morse.Achievements.AchievementDialog;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;


public class MainActivity extends Activity implements View.OnClickListener{

	
	static public final String RESULT = "result";
	static public final String TEST_VALUE = "test_value";
	static public final String INPUT_VALUE = "input_value";
	@SuppressWarnings("unused")
	private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoffwt7pds3vp2NelORJCeAGE0PcQUZ4Ad6thpOBGraHXGR9ShxqsQu8Osr/lkEyQ+399bffh1QonL7o9jvxdXxXb3LeN4EpCV+wCxKzUCjJi03Qy0ind+xGUQKTYmJ2nM8Ceu4fFibTS1gNa5kg4rZZI9YBUT+ryH7xmGw2yUzvXDHH4ssfqPS608hbzx7ieXszTCgC5ERomqHkXVqcE3dAhXZsOvmWQMnp1Uc7AIcWj3LwOBNc5uUNEZ2w+DMCy0CPLFLLtxt8hH+JTrrhGQ0PGEH+Cv8sRNZI1wZM4q8YlSiNAn/7g7vqb3cpJBctsCCuL3w2UADgAHdJJfA9THQIDAQAB";
	
	
	private int width;

	private Button transmittBtn, receiveBtn, handbookBtn, settingsBtn, extraBtn, achievementsBtn;

	private Button namePanel;
	private Animation downAnimation;
	private Animation upAnimation;
	
	private boolean isAnimationMenuOn = true;
	
	private Handler 						timerHandler = new Handler();
	private long timerStartTime;

	private SharedPreferences preferences;
	private AudioManager am;
	private Runnable 						timerRunnable = new Runnable() {

    
		@Override
        public void run() {
            long millis = System.currentTimeMillis() - timerStartTime;
            if(millis > 1500L){
            	timerHandler.removeCallbacks(this);
            	if(namePanel != null && upAnimation!= null)
            	{
            		namePanel.startAnimation(upAnimation);
            		namePanel.setVisibility(View.INVISIBLE);

            	}
            	
            }else
            	timerHandler.postDelayed(this, 50);
        }
    };
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		
		transmittBtn = (Button)findViewById(R.id.main_transmitt_button);
		receiveBtn = (Button)findViewById(R.id.main_receive_button);
		handbookBtn = (Button)findViewById(R.id.main_handbook_button);
		settingsBtn = (Button)findViewById(R.id.main_settings_button);
		extraBtn  = (Button)findViewById(R.id.main_extra_button);
		achievementsBtn = (Button)findViewById(R.id.main_achievements_button);
		namePanel		= (Button)findViewById(R.id.main_name_panel);
		downAnimation 	= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.popup_in_down);
		upAnimation 	= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.popup_out_up);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		width = displaymetrics.widthPixels;
		int margin = 10;
				
		int sideOfButton = (width/2 - margin * 4);

		RelativeLayout.LayoutParams layoutParams =  (RelativeLayout.LayoutParams)transmittBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		transmittBtn.setLayoutParams(layoutParams);
		
		layoutParams =  (RelativeLayout.LayoutParams)receiveBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		receiveBtn.setLayoutParams(layoutParams);
		
		layoutParams =  (RelativeLayout.LayoutParams)handbookBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		handbookBtn.setLayoutParams(layoutParams);
		
		layoutParams =  (RelativeLayout.LayoutParams)settingsBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		settingsBtn.setLayoutParams(layoutParams);
		
		layoutParams =  (RelativeLayout.LayoutParams)extraBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		extraBtn.setLayoutParams(layoutParams);
		
		layoutParams = (RelativeLayout.LayoutParams)achievementsBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		achievementsBtn.setLayoutParams(layoutParams);
		
		///get preferences for app
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	
	}



	private int currentItemSelected = -1; 
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.main_receive_button:
			currentItemSelected = R.id.main_receive_button;
			namePanel.setText(R.string.receive_training);
			break;
		case R.id.main_transmitt_button:
			currentItemSelected = R.id.main_transmitt_button;	
			namePanel.setText(R.string.transmit_training);
			break;
		case R.id.main_handbook_button:
			currentItemSelected = R.id.main_handbook_button;	
			namePanel.setText(R.string.handbook);
			break;
			
		case R.id.main_settings_button:
			currentItemSelected = R.id.main_settings_button;
			namePanel.setText(R.string.settings);
			break;
		case R.id.main_extra_button:
			currentItemSelected = R.id.main_extra_button;
			namePanel.setText(R.string.extra);
			break;
		case R.id.main_achievements_button:
			currentItemSelected = R.id.main_achievements_button;
			namePanel.setText(R.string.achievements_dialog_title);
			break;
		case R.id.main_name_panel:
			selectMenuItem(currentItemSelected);
			currentItemSelected = -1;
			break;
		default:
			break;
		}
		if(!isAnimationMenuOn && currentItemSelected != -1){
			selectMenuItem(currentItemSelected);
		}else if(currentItemSelected != -1){
			timerStartTime = System.currentTimeMillis();
			namePanel.setVisibility(View.VISIBLE);
			namePanel.startAnimation(downAnimation);
			timerHandler.postDelayed(timerRunnable, 0);
		}
	}
	
	
	
	private void selectMenuItem(int buttonId)
	{
		switch (buttonId) {
	
		case R.id.main_receive_button:
		{
			Intent receiveActivityIntent = new Intent();
			receiveActivityIntent.setClass(this, ReceivingActionActivity.class);
			startActivity(receiveActivityIntent);
		}
			break;
		case R.id.main_transmitt_button:
			Intent transmittActivityIntent = new Intent();
			transmittActivityIntent.setClass(this, TransmitterActionActivity.class);
			startActivity(transmittActivityIntent);
			break;
		case R.id.main_handbook_button:
			Intent handbookActivityIntent = new Intent();
			handbookActivityIntent.setClass(this, HandbookActivity.class);
			startActivity(handbookActivityIntent);
			break;
			
		case R.id.main_settings_button:
			Intent settingsActivityIntent = new Intent();
			settingsActivityIntent .setClass(this, SettingsActivity.class);
			startActivity(settingsActivityIntent );
			break;
		case R.id.main_extra_button:
			Intent extraActivityIntent = new Intent();
			extraActivityIntent .setClass(this, BonusSingActivity.class);
			startActivity(extraActivityIntent );
			break;
		case R.id.main_achievements_button:
			
			new AchievementDialog(this, Achievement.getAchievedAchievement(this)).init(true).show();
			break;
		default:
			break;
		}
	}


	
	@Override
	protected void onResume() {
		if(preferences != null){
			isAnimationMenuOn = preferences.getBoolean(SettingsActivity.IS_MAIN_MENU_ANIMATION_ON, isAnimationMenuOn);
		}
		super.onResume();
	}



	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		super.onPause();
	}
	
}
