package mu.zz.zagen.morse.Transmitters;

import mu.zz.zagen.morse.SettingsActivity;
import mu.zz.zagen.morse.General.TextCodec;
import android.content.Context;
import android.content.SharedPreferences;

abstract public class Transmitter {
	public interface OnTransmittanceEndedListener{
		void end();
	}
	
	private TextCodec codec;
	private Context context; 
	private int duration = 100;
	//private boolean isAddLongSpaceinEndOfTransmittance = false;
	protected OnTransmittanceEndedListener transmittanceEndedListener = null;

	protected Boolean stopFlag = false;
	
	
	public Transmitter(Context context, TextCodec codec)
	{
		this.codec = codec;
		this.context = context;
		SharedPreferences settings = this.context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		duration = settings.getInt(SettingsActivity.DURATION, 100);
		int locale = settings.getInt(SettingsActivity.LOCALE, 0);
		codec.setLocale(locale);
		codec.setLocaleByDefault(locale);
	}
	public Transmitter(Context context, TextCodec codec, Transmitter.OnTransmittanceEndedListener listener)
	{
		this(context, codec);
		transmittanceEndedListener = listener;
	}
	public final TextCodec getCodec()
	{
		return codec;
	}
	public final Context getContext()
	{
		return context;
	}
	abstract public  void transmit();
	
	public void stop(){
		stopFlag = true;
	}
	public void setDuration(final int duration)
	{
		this.duration = duration;
	}
	public int getDuration()
	{
		return duration;
	}
	public void setTransmittanceEndedListener(
			OnTransmittanceEndedListener transmittanceEndedListener) {
		this.transmittanceEndedListener = transmittanceEndedListener;
	}
	/**
	 * @return the isAddLongSpaceinEndOfTransmittance
	 
	public boolean isAddLongSpaceinEndOfTransmittance() {
		return isAddLongSpaceinEndOfTransmittance;
	}*/
	/**
	 * @param isAddLongSpaceinEndOfTransmittance the isAddLongSpaceinEndOfTransmittance to set
	
	public void setAddLongSpaceinEndOfTransmittance(
			boolean isAddLongSpaceinEndOfTransmittance) {
		this.isAddLongSpaceinEndOfTransmittance = isAddLongSpaceinEndOfTransmittance;
	}
	 */
	public abstract void pause();
}
