/**
 * 
 */
package mu.zz.zagen.morse;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * @author zagen
 *
 */
public class TouchPad extends View implements View.OnTouchListener {

	//private Context context;
 
	private Paint paint = new Paint();
	private Handler handler = null;
	
	//circle params
	private float radius;
	private float cx;
	private float cy;
	
	//tone params
	private SharedPreferences settings;
	private double frequence;           							// hz
	private int prefDuration;
	private int sampleRate = 8000;             
	private int minSize;
	private byte genSoundUp[];
	private byte genSound[];
	private byte silence[];
	private byte genSoundDown[];
	private boolean isPlaying = true;
	private boolean isTone = false;
	
	private boolean isAvailable = true;
	 
	//private short watch[];
	
	//input params
	public static final String MESSAGE_DATA_CHARACTER 	= "message_data_character";
	public static final char LONG_DELAY_SYMBOL			= '\u231A'; 


	//timing for distinguish space character
	private boolean isTimerThreadAlive = true;
	private boolean isTicking = false;
	private long lastTimeInputCharacterEnded = 0;
	
	private long touchDown = 0;
	
	/**
	 * This method check if set a external handler and send inputed morse code(dash, dot, space or long space)
	 * to outer code
	 * @param character Inputed character
	 */
	private void sendInputedCharacter(char character){
		if(handler != null){
			Message message = new Message();
			Bundle data = new Bundle();
			data.putChar(MESSAGE_DATA_CHARACTER, character);
			message.setData(data);
			handler.sendMessage(message);
		}
	}
	
	/*
	 * Thread that make sound when set on flag isTone 
	 */
	private Thread playThread = new Thread(new Runnable() {
		
		@Override
		public void run() {
			
			AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
	                sampleRate, AudioFormat.CHANNEL_OUT_MONO,
	                AudioFormat.ENCODING_PCM_16BIT, 
	                AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT),
	                AudioTrack.MODE_STREAM);
			if(audioTrack != null){
				
				audioTrack.play();
				while(isPlaying){
					if(isTone)
						audioTrack.write(genSound, 0, genSound.length);
					else
						audioTrack.write(silence, 0, silence.length);
				}
				audioTrack.release();
				audioTrack = null;
			}
			
			
		}
		
	}); 
	private boolean isSpaceSend = false;
	private Thread timerThread = new Thread(new Runnable() {
		private long timeInterval = 0;

		@Override
		public void run() {
			while(isTimerThreadAlive){
				if(isTicking){
					timeInterval = System.currentTimeMillis() - lastTimeInputCharacterEnded;
					if(timeInterval > prefDuration * 3 && !isSpaceSend){
						isSpaceSend = true;
						sendInputedCharacter(' ');
					}
					if(timeInterval > prefDuration * 9){
						isTicking = false;
						isSpaceSend = false;
						sendInputedCharacter(LONG_DELAY_SYMBOL);
						
						
					}
					//maybe need add long space?
				}
			}
		}
	});
	
	private void generateSounds(){
		minSize = 400; 
		double sample[] = new double[minSize];
		for (int i = 0; i < minSize; ++i) {      // Fill the sample array
	            sample[i] = Math.sin(this.frequence * 2 * Math.PI * i / (sampleRate));
	    }
		genSoundUp = new byte[minSize*2];

		for(int i = 0; i < sample.length; i++){
			short currValue = (short)(Short.MAX_VALUE * sample[i] *  (double)i / (double)sample.length); 
			genSoundUp[2*i] = (byte) (currValue & 0x00ff);
			genSoundUp[2*i + 1] = (byte) ((currValue & 0xff00) >>> 8);
		}
		
		genSound = new byte[minSize*2];
		silence = new byte[minSize];

		for(int i = 0; i < sample.length; i++){
			short currValue = (short)(Short.MAX_VALUE * sample[i]); 
			genSound[2*i] = (byte) (currValue & 0x00ff);
			genSound[2*i + 1] = (byte) ((currValue & 0xff00) >>> 8);
		}
		
		genSoundDown = new byte[minSize*2];

		for(int i = 0; i < sample.length; i++){
			short currValue = (short)(Short.MAX_VALUE * sample[i] *  ((double)sample.length - (double)i)/ (double)sample.length); 
			genSoundDown[2*i] = (byte) (currValue & 0x00ff);
			genSoundDown[2*i + 1] = (byte) ((currValue & 0xff00) >>> 8);
		}
		
		
		playThread.start();
		
	}
	
	private void init(Context context ){
		setOnTouchListener(this);
		setBackgroundColor(Color.BLACK);
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(5f);
		///setting up env for audio
		settings = context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		frequence = settings.getInt(SettingsActivity.FREQUENCE, 1000);
		prefDuration = settings.getInt(SettingsActivity.DURATION, 100);
		//reading preferences
		int volume = settings.getInt(SettingsActivity.VOLUME, 10);
		AudioManager manager = (AudioManager)getContext().getSystemService(Context.AUDIO_SERVICE);
		//setting volume from our preference
		manager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

		generateSounds();
		timerThread.start();
		setSoundEffectsEnabled(false);
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		radius = (getWidth() > getHeight())? getHeight() : getWidth();
		radius /= 2;
		cx = getWidth()/2;
		cy = getHeight()/2;

		super.onLayout(changed, left, top, right, bottom);
	}

	public TouchPad(Context context) {
		super(context);
		init(context);
		
	}

	public TouchPad(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		
	}

	public TouchPad(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			if(Math.sqrt((cx - x) * (cx - x) + (cy - y)*(cy - y)) < radius && isAvailable){
				
				/*if(isTicking){
					if((lastTimeInputCharacterEnded - touchDown) > 3 * prefDuration)
						sendInputedCharacter(' ');
				}*/
				//stop timer for future restarting
				isTicking = false;
				//isSpaceSend= false;
				//changing color of the pad and make view invalidate for redraw
				paint.setStyle(Paint.Style.FILL);
				invalidate();
				//turn on sound
				isTone = true;
				touchDown = System.currentTimeMillis();
				
			}
			
			break;
		case MotionEvent.ACTION_OUTSIDE:
		case MotionEvent.ACTION_UP:
			paint.setStyle(Paint.Style.STROKE);
			invalidate();

			isTone = false;
			if(isAvailable && touchDown != 0){
				long eventDuration =  System.currentTimeMillis() - touchDown;
						//event.getEventTime() - event.getDownTime();
				
				touchDown = 0;
				
				if(eventDuration < 3 * prefDuration){
					sendInputedCharacter('.');
				}else{
					sendInputedCharacter('-');
				}
				isSpaceSend= false;
				lastTimeInputCharacterEnded = System.currentTimeMillis();
				isTicking = true;
				
			}
			
			
			break;
			default:
				return false;
		}
		
		return true;
		}

	@Override
	protected void onDraw(Canvas canvas) {
		
		canvas.drawCircle(cx, cy, radius - 5, paint);
		super.onDraw(canvas);
	}
	
	public void release(){
		isPlaying = false;
		isTimerThreadAlive = false;
	}


	/**
	 * @param handler the handler to set
	 */
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	/**
	 * @return the isAvailable
	 */
	public boolean isAvailable() {
		return isAvailable;
	}

	/**
	 * @param isAvailable the isAvailable to set
	 */
	public void setAvailable(boolean isAvailable) {
		//
		this.isAvailable = isAvailable;
	}

}
