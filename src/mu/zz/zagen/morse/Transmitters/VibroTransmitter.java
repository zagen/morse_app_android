package mu.zz.zagen.morse.Transmitters;

import mu.zz.zagen.morse.General.TextCodec;
import android.content.Context;

import android.os.Vibrator;

public class VibroTransmitter extends Transmitter {

	private Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
	private boolean isPause = false;
	
	public VibroTransmitter(Context context, TextCodec codec) {
		super(context, codec);
	}
	public VibroTransmitter(Context context, TextCodec codec, Transmitter.OnTransmittanceEndedListener listener) {
		super(context, codec, listener);
	}
	
	@Override
	public void transmit() {

		if(isPause){
			isPause = false;
			return;
		}
		stopFlag = false;
		final String encodedText = getCodec().getEncodedText(); 
		if(encodedText.length() == 0)
		{

			if(transmittanceEndedListener != null)
				transmittanceEndedListener.end();
			return;
		}
		
		
		Thread playThread = new Thread(new Runnable() {
			@Override
				public void run() {
					int currentPos = 0;
		
					int duration = getDuration();

					while(currentPos < encodedText.length() && !stopFlag)
					{
						if(!isPause){
							try {
								if(encodedText.charAt(currentPos) == '-')
								{				
									vibrator.vibrate(duration * 3);
									Thread.sleep(duration * 3);
									
									Thread.sleep(duration);
								}else{ if(encodedText.charAt(currentPos) == '.')
								{
									vibrator.vibrate(duration);
									Thread.sleep(duration);
	
									Thread.sleep(duration);
									  					
								}else{ if(encodedText.charAt(currentPos) == ' '){
									Thread.sleep(duration*3);
								}else{
									Thread.sleep(duration*7);
									    
									}}}
							 } catch (InterruptedException e) {
							      e.printStackTrace();
							}
							currentPos++;
						}
					}
					vibrator.cancel();
					if(transmittanceEndedListener != null)
						transmittanceEndedListener.end();
					
				}
			});
		playThread.start();
		


	}
	@Override
	public void pause() {
		isPause  = true;
		
	}

}
