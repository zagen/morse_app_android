/**
 * 
 */
package mu.zz.zagen.morse.General;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author zagen
 *
 */
public class PositionView extends View {

	private int radius = 10;
	private int countPosition = 0;
	private int currentPosition = 0;
	private final int ALPHA = 190;
	private Paint paint = new Paint();
	{
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.FILL);
		paint.setAlpha(ALPHA);
	}
	

	
	public PositionView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

	}

	public PositionView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public PositionView(Context context) {
		super(context);

	}

	
/* (non-Javadoc)
 * @see android.view.View#onMeasure(int, int)
 * set correct view size. Recalculate before each invalidate() method 
 */
@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int height = radius;
		final int width =((countPosition - 1) * 2  + 3)*radius;
		setMeasuredDimension(width, height);
}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int x = 0;
		
		for(int i = 0 ; i < countPosition; i++){
			
			//dash is a current position
			if(i == currentPosition){
				paint.setAlpha(255);
				canvas.drawRect(x, 0, x + radius * 3, radius, paint);
				x += radius * 4;
				paint.setAlpha(ALPHA);
			}else{
				//the other  are dots
				canvas.drawCircle(x + radius/2, radius/2, radius/2, paint);
				x += radius*2;
			}
				
		}
	}
	
	
	public void setPositionCount(int positionCount){
		this.countPosition = positionCount;
		invalidate();
	}
	public void next()
	{
		currentPosition = ++currentPosition % countPosition;
		invalidate();
	}
	public void previous()
	{
		if(currentPosition == 0)
			currentPosition = countPosition - 1;
		else
			currentPosition--;
		
		invalidate();
	}
	
}

