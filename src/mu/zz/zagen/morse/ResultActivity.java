/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.Statistics;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author zagen
 * activity displaying result from training
 */
public class ResultActivity extends Activity {
	
	private TextView totalTextView;
	private ListView listView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.result_layout);
		totalTextView		= (TextView)findViewById(R.id.result_procentes_txt);
		
		listView			= (ListView)findViewById(R.id.result_listview);
		
		Bundle extras = getIntent().getExtras();
		String jsonstring = extras.getString(MainActivity.RESULT);
		Display display = getWindowManager().getDefaultDisplay(); 
		@SuppressWarnings("deprecation")
		int width = display.getWidth();  // deprecated
		listView.setAdapter(new ResultListAdapter(this, jsonstring, width));
		Statistics stats = new Statistics(jsonstring);
		totalTextView.setText(getString(R.string.total_score) + ":\n" + 
							  stats.getCorrectInputedCharacters() + " - " + stats.getTotalCharacters() + "\n" + 
							  stats.getPercentOfCorrectness() + "% - " + "100%");
	}
	
	
	
	
}
