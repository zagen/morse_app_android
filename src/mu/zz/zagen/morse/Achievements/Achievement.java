/**
 * Abstract class - interface for current and future trophy. 
 * Provide all info about achievement and posting in social network.
 */
package mu.zz.zagen.morse.Achievements;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import mu.zz.zagen.morse.SettingsActivity;
import mu.zz.zagen.morse.General.Statistics;



/**
 * @author zagen
 *
 */
public abstract class Achievement {
	
	protected int rightAnswerAmount = 0;
	protected int totalCharAmount = 0;
	protected Context context;
	
	/**
	 * @param jsonstring
	 * Constructor for subclasses. 
	 */
	public Achievement(Context context, final int rightAnswerAmount, final int totalCharAmount){
		this.context = context;
		this.rightAnswerAmount = rightAnswerAmount;
		this.totalCharAmount  = totalCharAmount;
	}
	
	/**
	 * Factory method. Return achievements if any of it was achieved
	 * @param jsonstring
	 * @return achievement
	 */
	static public final List<Achievement> getAchievement(Context context, String jsonstring){
		Statistics statistics = new Statistics(jsonstring);
		int rightCount 	= statistics.getCorrectInputedCharacters();
		int total 		= statistics.getTotalCharacters();
		SharedPreferences preferences = context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		
		
		List<Achievement> results = null;
		
		boolean isNoMistakeAchieved = preferences.getBoolean(NoMistakeAchievement.NAME, false);
		if( rightCount == total && rightCount >= 10 && !isNoMistakeAchieved){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new NoMistakeAchievement(context, rightCount, total));
			isNoMistakeAchieved = true;
		}
		
		boolean isNewbieAchieved = preferences.getBoolean(NewbieAchievement.NAME, false);
		if( rightCount >= 5 && !isNewbieAchieved){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new NewbieAchievement(context, rightCount, total));
			isNewbieAchieved = true;
		}
		boolean isApprenticeAchieved = preferences.getBoolean(ApprenticeAchievement.NAME, false);
		if( rightCount >= 15 && !isApprenticeAchieved){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new ApprenticeAchievement(context, rightCount, total));
			isApprenticeAchieved = true;
		}
		
		boolean isMasterAchieved = preferences.getBoolean(MasterAchievement.NAME, false);
		if( rightCount >= 30  && !isMasterAchieved){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new MasterAchievement(context, rightCount, total));
			isMasterAchieved = true;
		}
		boolean isGrandMasterAchieved = preferences.getBoolean(GrandMasterAchievement.NAME, false);
		if( rightCount >= 50 && !isGrandMasterAchieved){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new GrandMasterAchievement(context, rightCount, total));
			isGrandMasterAchieved = true;
		}
		
		int personalBest = preferences.getInt(NewRecordAchievement.NAME, 0);
		if( rightCount > personalBest){
			if(results == null)
				results = new ArrayList<Achievement>();
			results.add(new NewRecordAchievement(context, rightCount, total));
			personalBest = rightCount;
		}
		
		
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(NoMistakeAchievement.NAME, isNoMistakeAchieved);
		editor.putBoolean(NewbieAchievement.NAME, isNewbieAchieved);
		editor.putBoolean(ApprenticeAchievement.NAME, isApprenticeAchieved);
		editor.putBoolean(MasterAchievement.NAME, isMasterAchieved);
		editor.putBoolean(GrandMasterAchievement.NAME, isGrandMasterAchieved);
		editor.putInt(NewRecordAchievement.NAME, personalBest);
		editor.commit();
		
		return results;
	}
	static public final List<Achievement> getAchievedAchievement(Context context){
		
		List<Achievement> results = new ArrayList<Achievement>();
		SharedPreferences preferences = context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		
		int rightCount = 0;
		int total = 0;
		
		boolean isNoMistakeAchieved = preferences.getBoolean(NoMistakeAchievement.NAME, false);
		if( isNoMistakeAchieved){
			results.add(new NoMistakeAchievement(context, rightCount, total));
		}
		
		boolean isNewbieAchieved = preferences.getBoolean(NewbieAchievement.NAME, false);
		if(isNewbieAchieved){
			results.add(new NewbieAchievement(context, rightCount, total));
		}
		boolean isApprenticeAchieved = preferences.getBoolean(ApprenticeAchievement.NAME, false);
		if(isApprenticeAchieved){
			results.add(new ApprenticeAchievement(context, rightCount, total));
		}
		
		boolean isMasterAchieved = preferences.getBoolean(MasterAchievement.NAME, false);
		if(isMasterAchieved){
			results.add(new MasterAchievement(context, rightCount, total));
		}
		boolean isGrandMasterAchieved = preferences.getBoolean(GrandMasterAchievement.NAME, false);
		if( isGrandMasterAchieved){
			results.add(new GrandMasterAchievement(context, rightCount, total));
		}
		
		int personalBest = preferences.getInt(NewRecordAchievement.NAME, 0);
		if( personalBest != 0){
			results.add(new NewRecordAchievement(context, personalBest, personalBest));
		}
		return results;
	}
	/**
	 * @return name of achievement
	 */
	abstract public String name();
	abstract public Bitmap getPictureTrophy();
	abstract public String desription();
	
	/**
	 * Publish trophy and description on wall of your profile in social network 
	 * @param SocialNetwork implementation
	 * @return true if post was succeed
	
	public boolean publish(SocialNetwork sn) {
		return sn.post(this);
	}
		mb feature
	 */
	
	/**
	 * @return the rightAnswerAmount
	 */
	public int getRightAnswerAmount() {
		return rightAnswerAmount;
	}

	/**
	 * @return the totalCharAmount
	 */
	public int getTotalCharAmount() {
		return totalCharAmount;
	}

}
