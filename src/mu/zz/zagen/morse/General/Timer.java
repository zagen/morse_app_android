/**
 * 
 */
package mu.zz.zagen.morse.General;

/**
 * @author zagen
 *
 */
public class Timer {
	public interface OnTimerClockedListener{
		void onTimerClocked();
	}
	
	private OnTimerClockedListener listener = null;
	
	private long time;
	
	public Timer(long timeMS, OnTimerClockedListener onTimerClockedListener){
		this.time = timeMS;
		this.listener = onTimerClockedListener;
	}
	
	public void run(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				long beginTime = System.currentTimeMillis();
				long currentTime =  0;
				while(currentTime < time){
					currentTime = System.currentTimeMillis() - beginTime;
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if(listener != null){
					listener.onTimerClocked();
				}
			}
		}).start();
	}
}
