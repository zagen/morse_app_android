package mu.zz.zagen.morse.General;

import mu.zz.zagen.morse.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Statistics{
	private JSONArray jsonarray;
	private int totalCharacters = 0;
	private int correctInputedCharacters = 0;
	private double percentOfCorrectness = 0;
	
	private int getCountOfMatchingCharacters(String firstSequence,String secondSequence){
		int count = 0;
		int size = (firstSequence.length() > secondSequence.length())
					? secondSequence.length() : firstSequence.length();
		for(int i = 0; i < size; i++){
			if(firstSequence.charAt(i) == secondSequence.charAt(i))
				count++;
		}
		return count;
	}
	public Statistics(String jsonstring) {
		try {
			jsonarray = new JSONArray(jsonstring);
			for(int i = 0; i < jsonarray.length(); i++){
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				String firstSequence = jsonobject.getString(MainActivity.TEST_VALUE);
				String secondSequence = jsonobject.getString(MainActivity.INPUT_VALUE);
				totalCharacters += firstSequence.length();
				correctInputedCharacters += getCountOfMatchingCharacters(firstSequence, secondSequence);
			}
			percentOfCorrectness =  (double)correctInputedCharacters * 100/totalCharacters ;
			percentOfCorrectness = Math.round(percentOfCorrectness*100)/100d;
		} catch (JSONException e) {
			jsonarray = null;
			e.printStackTrace(); 
		}
		
	}

	public double getPercentOfCorrectness() {
		return percentOfCorrectness;
	}

	/**
	 * @return the correctInputedCharacters
	 */
	public int getCorrectInputedCharacters() {
		return correctInputedCharacters;
	}

	/**
	 * @return the totalCharacters
	 */
	public int getTotalCharacters() {
		return totalCharacters;
	}

	

}