/**
 * 
 */
package mu.zz.zagen.morse;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

/**
 * @author zagen
 *
 */
public class ReceivingActionActivity extends Activity implements View.OnClickListener{

	
	private SharedPreferences preferences;
	private AudioManager am;
	private final static int ACTION_RECEIVE_EXERCISE_RESULT = 1001;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//set content from layout
		setContentView(R.layout.select_two_item_activity_layout);
		
		Button rcaLaunchBtn = (Button)findViewById(R.id.select_activity_1st_button);
		rcaLaunchBtn.setBackgroundResource(R.drawable.receiving_exercise);
		rcaLaunchBtn.setOnClickListener(this);
		
		Button listeningActivityLaunchBtn = (Button)findViewById(R.id.select_activity_2nd_button);
		listeningActivityLaunchBtn.setBackgroundResource(R.drawable.txt_listening);
		listeningActivityLaunchBtn.setOnClickListener(this);
		
		TextView label = (TextView)findViewById(R.id.select_activity_textview);
		label.setText(R.string.select_receive_training);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		int width = displaymetrics.widthPixels;
		//int height = displaymetrics.heightPixels;
		
		int margin = 10;
		int sideOfButton = (width/2 - margin * 4);

		RelativeLayout.LayoutParams layoutParams = (LayoutParams) rcaLaunchBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		rcaLaunchBtn.setLayoutParams(layoutParams);
		
		layoutParams = (LayoutParams) listeningActivityLaunchBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		listeningActivityLaunchBtn.setLayoutParams(layoutParams);
		
		
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.select_activity_1st_button:
			Intent receiveActivityIntent = new Intent();
			receiveActivityIntent.setClass(this, ReceiveCheckActivity.class);
			startActivityForResult(receiveActivityIntent, ACTION_RECEIVE_EXERCISE_RESULT);
			break;
		case R.id.select_activity_2nd_button:
			Intent listeningActivityIntent = new Intent();
			listeningActivityIntent.setClass(this, ListeningActivity.class);
			startActivity(listeningActivityIntent);
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPause();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case ACTION_RECEIVE_EXERCISE_RESULT:
			if(resultCode == RESULT_OK){
				String jsonstring = data.getStringExtra(MainActivity.RESULT);
				if(!jsonstring.equals("[]")){
					Intent resultIntent = new Intent();
					resultIntent.setClass(getApplicationContext(), ResultActivity.class);
					resultIntent.putExtra(MainActivity.RESULT, jsonstring);
					startActivity(resultIntent);
				}
			
			}
			break;

		default:
				
		}
	}
	
}
