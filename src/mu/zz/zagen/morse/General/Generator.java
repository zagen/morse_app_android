package mu.zz.zagen.morse.General;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.Random;


/**
 * @author zagen
 *
 */
public class Generator {
	/*
	 * available locals
	 */
	public static final int RUS = 1;
	public static final int INT = 0;

	public static final int WORDS = 1;
	public static final int CHARS = 0;
	
	private int currentLocale = INT;
	
	private int size;
	private Random random = new Random();
	private StringBuilder sb = new StringBuilder();
	private String abc = "";
	private static final String abcInt = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!";
	private static final String abcRus = "��������������������������������1234567890.,:;()\"'-/?!";
	private int sequenceType = CHARS;
	private WordsAccessor wordAccessor;
	
	private final static int GROUP_SIZE = 5;
	
	/*
	 * all characters split into GROUP_SIZE groups in accordance with their frequency of appear in texts
	 */
	private final String[] groups = new String[GROUP_SIZE];{
		groups[0] = "�����������etaonrish";
		groups[1] = "�����������dlfcmugyp";
		groups[2] = "�����������wbvkxjqz";
		groups[3] = "1234567890";
		groups[4] = ".,:;()\"'-/?!";
	}
	/*
	 * current abc split into fit groups
	 */
	private String[] currentGroups = new String[GROUP_SIZE];{
		currentGroups[0] = "";
		currentGroups[1] = "";
		currentGroups[2] = "";
		currentGroups[3] = "";
		currentGroups[4] = "";
	}
	
	/*
	 * probability of appearance each group (?/1000)
	 */
	private int[] currentGroupChance = new int[GROUP_SIZE];{
		currentGroupChance[0] = 1000;
		currentGroupChance[1] = 200;
		currentGroupChance[2] = 150;
		currentGroupChance[3] = 80;
		currentGroupChance[4] = 80;
	}
	
	/*
	 * constructors
	 */
	public Generator(Context context){
		size = 1;
		wordAccessor = WordsAccessor.getInstance(context);
	}
	
	public Generator(Context context, int size, int locale){
		this.size = size;
		currentLocale = locale;
		wordAccessor = WordsAccessor.getInstance(context);
	}
	public void setLocale(int locale)
	{
		currentLocale = locale;
	}
	public void setSize(int size)
	{
		this.size = size;
	}
	public int getSize()
	{
		return size;
	}
	public int getLocale()
	{
		return currentLocale;
	}
	
	/*
	 * Here general function that generate unique sequence of symbols in accordance with setted abc
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@SuppressLint("DefaultLocale")
	public String toString(){
		if(sb.length() != 0)
			sb.delete(0, sb.length());

		//if generated type is words read random word from file
		if(sequenceType == Generator.WORDS){
			String word = wordAccessor.getRandomWord(currentLocale);
			if(word.length() != 0){
				sb.append(word);
			}else
			{
				sequenceType = Generator.CHARS;
			}
		}
		
		if(sequenceType == Generator.CHARS){
			if(abc.length() == 0) //if abc is not set yet set it
			{
				if(currentLocale == RUS){
					setAbc(abcRus);
				}else
					setAbc(abcInt);
			}
			for(int i = 0; i < size; i++)
			{
				//here will be our generated symbol
				char generatedChar = 0;

				//go down for our groups 
				for(int j = GROUP_SIZE-1; j >= 0; j--){
					
					//generate random number from 0 to 999;
					int randomNumber = random.nextInt(1000);
					
					//check if random number fit probability of current group
					if(randomNumber < currentGroupChance[j]){
						if(!isGroupEmpty(j)){
							//if so and current group is not empty, then get random character from group and go out from inner loop
							generatedChar = getRandomCharFromGroup(j);
							break;
						}
					}
				}
				//if we don't get no one symbol, then get random character from all abc was set
				if(generatedChar == 0){
					generatedChar = abc.charAt(random.nextInt(abc.length()));
				}
				//and add it to current string builder
				sb.append(generatedChar);

			}
		}
		return sb.toString().toUpperCase();
		
	}
	
	
	/**
	 * @return current alphabet
	 */
	public String getAbc() {
		return abc;
	}
	
	public void setAbc(String abc) {
		//set field abc
		this.abc = abc;

		//here current character from abc in string format will be
		String currentCharacterStr;
		
		//same only in Character format;
		Character currentChar;
		
		//clear currentGroups
		for(int i = 0; i < GROUP_SIZE; i++){
			currentGroups[i] = "";
		}
		
		//go through all symbols of setting alphabet
		for(int i = 0; i < abc.length(); i++){
			//get current char and convert it to string
			currentChar = abc.charAt(i);
			currentCharacterStr = currentChar.toString();
			
			//check sequentially all group and if it contains current symbol add it fit current group
			for(int j = 0; j < GROUP_SIZE; j++){
				if(groups[j].contains(currentCharacterStr)){
					currentGroups[j] += currentCharacterStr;
					break;
				}
			}
		}
	}

	/**
	 * @param group from our symbol would be taken
	 * @return random character from group
	 */
	private char getRandomCharFromGroup(int group){
		return currentGroups[group].charAt(random.nextInt(currentGroups[group].length()));
	}
	
	/**
	 * @param group that check
	 * @return true if group empty
	 */
	private boolean isGroupEmpty(int group){
		return currentGroups[group].length() == 0;
	}
	
	/**
	 * @param sequenceType the sequenceType to set. Maybe WORDS = 1 or CHARS = 0
	 */
	public void setSequenceType(int sequenceType) {
		this.sequenceType = sequenceType;
	}
	
	/**
	 * return sequence type
	 */
	public int getSequenceType() {
		return sequenceType;
	}
}
