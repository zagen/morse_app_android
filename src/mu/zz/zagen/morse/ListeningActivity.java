/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.OpenFileDialog;
import mu.zz.zagen.morse.General.PercentChooseDialogBuilder;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author zagen
 *
 */
public class ListeningActivity extends Activity implements View.OnClickListener{

	public static final String ACTION_STRING_ACTIVITY = "pfsActionActivity";
	public static final String EXTRA_STRING_TYPE = "pfsActionTypeActivity";
	
	public static final int EXTRA_TYPE_ID_SERVICE_STARTED = 104;
	public static final int EXTRA_TYPE_ID_FILE_STATUS = 100;
	public static final int EXTRA_TYPE_ID_CURRENT_WORD = 101;
	public static final int EXTRA_TYPE_ID_CURRENT_PERCENT = 102;
	public static final int EXTRA_TYPE_ID_PLAY_STATUS = 103;
	
	
	public static final String EXTRA_STRING_CURRENT_WORD = "pfsActivityExtraCurrentWord";
	public static final String EXTRA_STRING_CURRENT_PERCENT = "pfsActivityExtraCurrentPercent";
	
	public static final String EXTRA_FILE_STATUS_STRING = "pfsActivityExtraFileStatus";
	public static final int EXTRA_FILE_STATUS_ID_FILE_OPENED = 201;
	public static final int EXTRA_FILE_STATUS_ID_FILE_FAILURE = 202;
	public static final int EXTRA_FILE_STATUS_ID_FILE_EMPTY = 203;
	
	
	public static final String EXTRA_PLAY_STATUS_STRING = "pfsActivityExtraPlayStatus";
	public static final int EXTRA_PLAY_STATUS_ID_PLAYING= 301;
	public static final int EXTRA_PLAY_STATUS_ID_PAUSED = 302;
	//public static final int EXTRA_PLAY_STATUS_ID_STOP = 303;
	
	private final static double RADIO_VIEW_WIDTH = 0.55;
	private final static double RADIO_VIEW_HEIGH_IN_PART_OF_WIDTH = 0.75;
	private AnimationDrawable animation;
	private boolean isPlay = false;
	private String currentURIStr = null;
	private boolean isCurrentURIPlayable = false;
	
	private Button playControlBtn;
	private Button percentOfSeekView;
	private Button openFileBtn;
	
	private TextView wordView;
	
    private BroadcastReceiver activityReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
        	String action = intent.getAction();
        	if(action.equals(ACTION_STRING_ACTIVITY)){
        		Bundle extras = intent.getExtras();
        		switch(extras.getInt(EXTRA_STRING_TYPE)){
        		case EXTRA_TYPE_ID_FILE_STATUS:
        			if(extras.getInt(EXTRA_FILE_STATUS_STRING) == EXTRA_FILE_STATUS_ID_FILE_FAILURE){
        				Toast.makeText(ListeningActivity.this, R.string.cant_open_selected_file, Toast.LENGTH_SHORT)
        					 .show();
        			}else if(extras.getInt(EXTRA_FILE_STATUS_STRING) == EXTRA_FILE_STATUS_ID_FILE_EMPTY) {
        				Toast.makeText(ListeningActivity.this, R.string.file_is_empty, Toast.LENGTH_SHORT)
        					 .show();
        			}else{
        				Toast.makeText(ListeningActivity.this, R.string.file_opened, Toast.LENGTH_SHORT)
        					 .show();
        				openFileBtn.setText(Uri.parse(currentURIStr).getLastPathSegment());
        				isCurrentURIPlayable = true;
        			}
        			break;
        		case EXTRA_TYPE_ID_CURRENT_PERCENT:
        			double procent = extras.getDouble(EXTRA_STRING_CURRENT_PERCENT);
        			percentOfSeekView.setText(String.format("%2.2f%s",procent, "%"));
        			break;
        		case EXTRA_TYPE_ID_CURRENT_WORD:
        			String word = extras.getString(EXTRA_STRING_CURRENT_WORD);
        			wordView.setText(word);
        			break;
        		case EXTRA_TYPE_ID_PLAY_STATUS:
        			if(extras.getInt(EXTRA_PLAY_STATUS_STRING) == EXTRA_PLAY_STATUS_ID_PLAYING){
        				isPlay = true;
        				animation.start();
        				playControlBtn.setBackgroundResource(R.drawable.pause_button);
        			}/*else if(extras.getInt(EXTRA_PLAY_STATUS_STRING) == EXTRA_PLAY_STATUS_ID_STOP) {
        				isPlay = false;
        				wordView.setText("");
        				animation.stop();
        				playControlBtn.setBackgroundResource(R.drawable.play_button);
        			}*/else if(extras.getInt(EXTRA_PLAY_STATUS_STRING) == EXTRA_PLAY_STATUS_ID_PAUSED){
        				isPlay = false;
        				animation.stop();
        				playControlBtn.setBackgroundResource(R.drawable.play_button);
        			}
        			break;
        		case EXTRA_TYPE_ID_SERVICE_STARTED:
        			SharedPreferences preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
    				
    				String lastFileUri = preferences.getString(SettingsActivity.LAST_FILE_URI_STRING, "");
    				long position = preferences.getLong(SettingsActivity.LAST_FILE_POSITION_LONG, 0);
    				
    				if(lastFileUri.length() > 0){
    					currentURIStr = lastFileUri;
    					Intent intentURI = new Intent();
    					intentURI.setAction(PlayingFileService.ACTION_STRING_SERVICE);
    					intentURI.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_OPEN_URI);
    					intentURI.putExtra(PlayingFileService.EXTRA_STRING_FILE_URI, lastFileUri);
    					sendBroadcast(intentURI);
    					Intent intentPosition = new Intent();
    					intentPosition.setAction(PlayingFileService.ACTION_STRING_SERVICE);
    					intentPosition.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_GOTO_POSITION);
    					intentPosition.putExtra(PlayingFileService.EXTRA_STRING_POSITION, position);
    					sendBroadcast(intentPosition);
    					
    					
    				}
        			break;
        		}
        	}
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//make activity fullscreen and without any titlebar and actiobar
				requestWindowFeature(Window.FEATURE_NO_TITLE);
				getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
				
				//set content from layout
				setContentView(R.layout.listening_activity_layout);
				percentOfSeekView = (Button)findViewById(R.id.listening_procent_of_seek_txt);
				
				final Button radioViewBtn = (Button)findViewById(R.id.listening_radioview_btn);
				radioViewBtn.setBackgroundResource(R.anim.radio);
				animation = (AnimationDrawable)radioViewBtn.getBackground();
				DisplayMetrics metrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(metrics);
				
				LayoutParams params = radioViewBtn.getLayoutParams();
				
				params.width = ((int)(metrics.widthPixels*RADIO_VIEW_WIDTH));
				params.height = (int)(params.width * RADIO_VIEW_HEIGH_IN_PART_OF_WIDTH);
				radioViewBtn.setLayoutParams(params);
				
				playControlBtn = (Button)findViewById(R.id.listening_play_control_btn);
				openFileBtn = (Button)findViewById(R.id.listening_openfile_btn);
				wordView = (TextView)findViewById(R.id.listening_word_view);
				Button percentButton = (Button)findViewById(R.id.listening_percent_button);
				percentButton.setText("\u0025");
				Button okButton = (Button)findViewById(R.id.listening_ok_button);
				okButton.setText("\u2714");
				
				if (activityReceiver != null) {
					//Create an intent filter to listen to the broadcast sent with the action "ACTION_STRING_ACTIVITY"
					IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
					//Map the intent filter to the receiver
					registerReceiver(activityReceiver, intentFilter);
				}

				
					//Start the service on launching the application
				startService(new Intent(this, PlayingFileService.class));
				
				
				setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}
	@Override
	public void onClick(View v) {
		final Intent intent = new Intent();
		intent.setAction(PlayingFileService.ACTION_STRING_SERVICE);
		switch(v.getId()){
			case R.id.listening_radioview_btn: 
			case R.id.listening_play_control_btn:
				if(isCurrentURIPlayable){
					isPlay = !isPlay;
					if(isPlay){
						intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PLAY);
						sendBroadcast(intent);
					}else{
						intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PAUSE);
						sendBroadcast(intent);
					}
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.listening_ok_button:
				//stop service and finish activity
				intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_STOP_SERVICE);
				sendBroadcast(intent);
				finish();
				break;
			case R.id.listening_procent_of_seek_txt:
			case R.id.listening_percent_button:
				if(isCurrentURIPlayable){
					new PercentChooseDialogBuilder(this)
						.setOnFinishListener(new PercentChooseDialogBuilder.OnFinishDialogListener() {
							
							@Override
							public void onFinish(double percent) {
								percentOfSeekView.setText(String.format("%.2f%s", percent, "%"));
								Intent intent = new Intent();
								intent.setAction(PlayingFileService.ACTION_STRING_SERVICE);
								intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_GOTO_PERCENT);
								intent.putExtra(PlayingFileService.EXTRA_STRING_PERCENT, percent);
								sendBroadcast(intent);
							}
						})
						.setTitle(R.string.goto_percent)
						.create().show();
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.listening_handbook_button:
				Intent handbookIntent = new Intent();
				handbookIntent.setClass(this, HandbookActivity.class);
				startActivity(handbookIntent);
				break;
			case R.id.listening_open_button:
			case R.id.listening_openfile_btn:
				new OpenFileDialog(this)
					.setAccessDeniedMessage(getString(R.string.cant_open_this_directory))
					.setFilter("(.*)\\.txt") //with txt extension only!!!
					.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {
						
						@Override
						public void OnSelectedFile(String fileName) {
							Intent intent = new Intent();
							intent.setAction(PlayingFileService.ACTION_STRING_SERVICE);
							intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_OPEN_URI);
							intent.putExtra(PlayingFileService.EXTRA_STRING_FILE_URI, fileName);
							sendBroadcast(intent);
							currentURIStr = fileName;
						}
					}).show();
				
				break;
			case R.id.listening_play_back_1_btn:
				if(isCurrentURIPlayable){
					intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PLAY_BACKWARD_1);
					sendBroadcast(intent);
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.listening_play_back_2_btn:
				if(isCurrentURIPlayable){
					intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PLAY_BACKWARD_2);
					sendBroadcast(intent);
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.listening_play_forward_1_btn:
				if(isCurrentURIPlayable){
					intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PLAY_FORWARD_1);
					sendBroadcast(intent);
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.listening_play_forward_2_btn:
				if(isCurrentURIPlayable){
					intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_PLAY_FORWARD_2);
					sendBroadcast(intent);
				}else{
					Toast.makeText(this, R.string.file_is_not_selected, Toast.LENGTH_SHORT).show();
				}
				break;
				
		}
		
		
	}
	@Override
	protected void onDestroy() {
		final Intent intent = new Intent();
		intent.setAction(PlayingFileService.ACTION_STRING_SERVICE);
		intent.putExtra(PlayingFileService.EXTRA_STRING_TYPE, PlayingFileService.EXTRA_TYPE_ID_STOP_SERVICE);
		sendBroadcast(intent);
		unregisterReceiver(activityReceiver);
		super.onDestroy();
	}
	


}
