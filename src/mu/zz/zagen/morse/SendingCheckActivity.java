package mu.zz.zagen.morse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mu.zz.zagen.morse.General.Generator;
import mu.zz.zagen.morse.General.StackMorseView;
import mu.zz.zagen.morse.General.TextCodec;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;


public class SendingCheckActivity extends Activity implements View.OnClickListener {

	private StackMorseView 					stackView;
	private Generator 						generator ;
	private static String 					abc;
	private String 							currentSymbols;
	private Button 							generalViewButton;
	private TextCodec 						codec = new TextCodec();
	private String 							inputText = "";
	private Boolean 						inputFinished = false;
	private int 							width;
	private int 							height;
	private TextView 						statusAnswerTextView;
	private int	    						locale = 0;
	private int								type = 0;
	private Typeface 						typeface;
	private JSONArray 						resultJSONArray = new JSONArray();
	private AudioManager am;
	private SharedPreferences preferences;
	

	//for fitting text into button;
	private void correctWidth(Button button, int desiredWidth)
	{
		Paint paint = new Paint();
	    Rect bounds = new Rect();

	    paint.setTypeface(button.getTypeface());
	    float textSize = button.getTextSize() + 20;
	    //button.getTextSize();
	    paint.setTextSize(textSize);
	    String text = button.getText().toString();
	    paint.getTextBounds(text, 0, text.length(), bounds);
	    
	    while (bounds.width() > desiredWidth ||  bounds.height() >= height/7)
	    {
	        textSize--;
	        paint.setTextSize(textSize);
	        paint.getTextBounds(text, 0, text.length(), bounds);
	    }

	    button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
	}
	
	private void refill()
	{
		//generation new character sequence
		//all statuses set to none
		statusAnswerTextView.setVisibility(View.INVISIBLE);
		
		inputText = "";
		stackView.clear();
		
		String txt = generator.toString();
		generalViewButton.setText(txt);
		codec.setText(txt);	
		//encode to morse code
		if(generator.getSequenceType() == Generator.WORDS)
			correctWidth(generalViewButton, width - 100);//correct  width of generating text to fit one line
		inputFinished = false;
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.sca_layout);
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		width = displaymetrics.widthPixels;
		height = displaymetrics.heightPixels;
		
		int sideOfButton = (width - 10 * 8) / 4;
		
		Button dotBtn = (Button)findViewById(R.id.dot_button);
		RelativeLayout.LayoutParams layoutParams = (LayoutParams) dotBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(10, 0, 10, 0);
		dotBtn.setLayoutParams(layoutParams);
		
		Button dashBtn = (Button)findViewById(R.id.dash_button);
		layoutParams = (LayoutParams) dashBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(10, 0, 10, 0);
		dashBtn.setLayoutParams(layoutParams);
		
		Button spaceBtn = (Button)findViewById(R.id.space_button);
		layoutParams = (LayoutParams) spaceBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(10, 0, 10, 0);
		spaceBtn.setLayoutParams(layoutParams);
		
		Button endWordBtn = (Button)findViewById(R.id.end_of_word_button);
		layoutParams = (LayoutParams) endWordBtn.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(10, 0, 10, 0);
		endWordBtn.setLayoutParams(layoutParams);
		
		Button refillButton = (Button)findViewById(R.id.refill_button);
		refillButton.setText("\u21BA");
		Button okButton = (Button)findViewById(R.id.sca_ok_send_act_button);
		okButton.setText("\u2714");
		
		stackView = (StackMorseView)findViewById(R.id.stack_view);
		
		generalViewButton = (Button)findViewById(R.id.generator_view_button);
		
		statusAnswerTextView = (TextView)findViewById(R.id.sca_status_answer);
		
		generator = new Generator(this);

		
		preferences =  getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get locale 
		locale = preferences .getInt(SettingsActivity.LOCALE, 0);
		generator.setLocale(locale);
		codec.setLocale(locale);
		
		//get size
		int size = preferences .getInt(SettingsActivity.GEN_LENGTH, 2);
		generator.setSize(size);
		
		if(locale == TextCodec.RUS){
			abc = "��������������������������������1234567890.,:;()\"'-/?!";
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_RUS, "��������������������������������");
		}else
		{
			abc = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!"; 
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_INT, "abcdefghijklmnopqrstuvwxyz");
		}
		generator.setAbc(currentSymbols);
		
		
		type = preferences.getInt(SettingsActivity.SEQUENCE_TYPE, Generator.CHARS);
		generator.setSequenceType(type);
		
		typeface = Typeface.createFromAsset(getAssets(), "fonts/general.ttf");
		

		
		String txt = generator.toString();
		generalViewButton.setTypeface(typeface);
		generalViewButton.setText(txt);
		codec.setText(txt);//encode to morse code
		
		if(generator.getSequenceType() == Generator.CHARS){
			generalViewButton.setText("����������".substring(0, size));
			correctWidth(generalViewButton, width);//correct  width of generating text to fit one line
			generalViewButton.setText(txt);
		}else{
			correctWidth(generalViewButton, width - 100);//correct  width of generating text to fit one line
		}
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gen_symbols_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_symbols:
			String [] strItems = new String[abc.length()];
			final boolean[] boolItems= new boolean[abc.length()];
			//forming arrays for multiList 
			for(int i = 0; i < abc.length(); i++){
				strItems[i] = String.valueOf(abc.toCharArray()[i]);
				boolItems[i] = (currentSymbols.contains(strItems[i])) ? true : false;
			}
			
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setTitle(R.string.symbol_dialog_title)
				.setMultiChoiceItems(strItems, boolItems, new DialogInterface.OnMultiChoiceClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						boolItems[which] = isChecked;
						
					}
				})
				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreferences.Editor editor = preferences.edit();
						StringBuilder strBuilder = new StringBuilder();
						for(int i = 0; i < abc.length();i++){
							if(boolItems[i]){
								strBuilder.append(abc.toCharArray()[i]);
							}
						}
						if(locale == TextCodec.RUS)
							editor.putString(SettingsActivity.SYMBOLS_RUS, strBuilder.toString());
						else
							editor.putString(SettingsActivity.SYMBOLS_INT, strBuilder.toString());
						currentSymbols = strBuilder.toString();
						generator.setAbc(currentSymbols);
						refill();
						editor.commit();
					}
				})
				.create().show();
			break;
		case R.id.action_message_type:
			
			AlertDialog.Builder adb_type = new AlertDialog.Builder(this);
			adb_type.setSingleChoiceItems(getResources().getTextArray(R.array.message_types), type, new DialogInterface.OnClickListener() {
				
							@Override
							public void onClick(DialogInterface dialog, int which) {
								type = which;					
							}
						})
					.setTitle(R.string.type_dialog_title)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SharedPreferences.Editor editor = preferences.edit();
							editor.putInt(SettingsActivity.SEQUENCE_TYPE, type);
							generator.setSequenceType(type);
							if(generator.getSequenceType() == Generator.CHARS){
								generalViewButton.setText("����������".substring(0, generator.getSize()));
								correctWidth(generalViewButton, width);//correct  width of generating text to fit one line
							}
							refill();
							editor.commit();
						}
					})
					.create().show();
			break;

		default:
			break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.generator_view_button:
			inputText = "";
			statusAnswerTextView.setVisibility(View.INVISIBLE);
			if (generalViewButton.getText() instanceof Spannable)
			{
				Spannable text = (Spannable)generalViewButton.getText();
				ForegroundColorSpan[] spans = text.getSpans(0, text.length(), ForegroundColorSpan.class);
		        for (ForegroundColorSpan span: spans) {
		            text.removeSpan(span);		           
		        }
		        generalViewButton.setText(text);
			}
			
			stackView.clear();
			Toast.makeText(this, getString(R.string.try_again), Toast.LENGTH_SHORT).show();
			inputFinished = false;
			break;
		case R.id.dot_button:
			if(!inputFinished){
				stackView.push('.');
				inputText += '.';
			}
			break;
		case R.id.dash_button:
			if(!inputFinished){
				stackView.push('-');
				inputText += '-';
			}
			break;
		case R.id.space_button:
			if(!inputFinished){
				stackView.push(' ');
				inputText += ' ';
			}
			break;
		case R.id.end_of_word_button:
			//compare inputed string and encoded string
			if(inputFinished)
				return;
			inputText = inputText.trim();
			String[] inputedChars = inputText.split(" "); // here inputed morse codes  
			
			String[] sourceChars = codec.getEncodedText().split(" ");//here right sequence of morse codes
			stackView.clear();
			SpannableStringBuilder spanString = new SpannableStringBuilder(generalViewButton.getText()); //need to change of color character where been made mistake
			Boolean madeMistake = false; 
			if(inputedChars.length < sourceChars.length)
			{
				Toast.makeText(this, R.string.input_less_seq, Toast.LENGTH_SHORT).show();
				madeMistake = true;
				for(int i = 0; i < inputedChars.length; i++)
				{
					if(!sourceChars[i].equals(inputedChars[i]))
					{	
						spanString.setSpan(new ForegroundColorSpan(Color.GRAY), i, i+1, 0);//change color
					}
					
				}
				for(int i = inputedChars.length; i < sourceChars.length; i++)
				{
					spanString.setSpan(new ForegroundColorSpan(Color.GRAY), i, i+1, 0);//change color
				}
			}else 
			{
				for(int i = 0; i < sourceChars.length; i++)
				{
					if(!sourceChars[i].equals(inputedChars[i]))
					{
						madeMistake = true;
						spanString.setSpan(new ForegroundColorSpan(Color.GRAY), i, i+1, 0);
					}
				}
					
				if(inputedChars.length > sourceChars.length)
				{
					Toast.makeText(this, R.string.input_more_seq, Toast.LENGTH_SHORT).show();
				}else
				{
					if(!madeMistake)
					{
						JSONObject inputResult = new JSONObject();
						try {
							String textGenerated = generalViewButton.getText().toString().replace('�', '�');
							inputResult.put(MainActivity.TEST_VALUE, textGenerated);
							String currentTxt = codec.getDecodedText();
							codec.setMorse(inputText);
							inputResult.put(MainActivity.INPUT_VALUE, codec.getDecodedText().toUpperCase());
							codec.setText(currentTxt);
							resultJSONArray.put(inputResult);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						Toast.makeText(this, R.string.answer_is_correct, Toast.LENGTH_SHORT).show();
						refill();
						return;
					}
				}
			}
			//put out correct answer
			statusAnswerTextView.setVisibility(View.VISIBLE);
			Toast.makeText(this, R.string.wrong_answer, Toast.LENGTH_SHORT).show();
			
			for(char symbol: codec.getEncodedText().toCharArray())
			{
				stackView.push(symbol);
			}
			generalViewButton.setText(spanString, BufferType.SPANNABLE);

			JSONObject inputResult = new JSONObject();
			try {
				String textGenerated = generalViewButton.getText().toString().replace('�', '�');
				inputResult.put(MainActivity.TEST_VALUE, textGenerated);
				String currentTxt = codec.getDecodedText();
				codec.setMorse(inputText);
				inputResult.put(MainActivity.INPUT_VALUE, codec.getDecodedText().toUpperCase());
				codec.setText(currentTxt);
				resultJSONArray.put(inputResult);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			inputFinished = true;
			break;
		case R.id.refill_button: 
			refill();
			break;
		case R.id.sca_ok_send_act_button:
			//here put a result
			Intent intent = new Intent();
			intent.putExtra(MainActivity.RESULT, resultJSONArray.toString());
			setResult(RESULT_OK, intent);
			
			finish();
			break;
		case R.id.sca_handbook_button:
			Intent handbookActivityIntent = new Intent();
			handbookActivityIntent.setClass(this, HandbookActivity.class);
			startActivity(handbookActivityIntent);
			break;
		case R.id.sca_settings_button:
			openOptionsMenu();
			break;
		}
		
	}

	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPause();
	}
}
