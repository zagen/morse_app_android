package mu.zz.zagen.morse.Achievements;

import mu.zz.zagen.morse.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class NewbieAchievement extends Achievement {

	public static final String NAME = "NEWBIE_NAME";

	public NewbieAchievement(Context context, int rightAnswerAmount, int totalCharAmount) {
		super(context, rightAnswerAmount, totalCharAmount);
	}

	@Override
	public String name() {
		return context.getString(R.string.newbie_trophy_name);
	}

	@Override
	public Bitmap getPictureTrophy() {
		return BitmapFactory.decodeResource(context.getResources(), R.drawable.first_time_trophy);
	}

	@Override
	public String desription() {
		return context.getString(R.string.newbie_trophy_description);
	}

	
}
