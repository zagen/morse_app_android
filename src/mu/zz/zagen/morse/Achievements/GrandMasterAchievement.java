/**
 * 
 */
package mu.zz.zagen.morse.Achievements;

import mu.zz.zagen.morse.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @author zagen
 *
 */
public class GrandMasterAchievement extends Achievement {
	public static final String NAME = "GRANDMASTER_NAME";
	public GrandMasterAchievement(Context context, int rightAnswerAmount,
			int totalCharAmount) {
		super(context, rightAnswerAmount, totalCharAmount);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#name()
	 */
	@Override
	public String name() {
		return context.getString(R.string.grandmaster_trophy_name);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#getPictureTrophy()
	 */
	@Override
	public Bitmap getPictureTrophy() {
		return BitmapFactory.decodeResource(context.getResources(), R.drawable.grandmaster_trophy);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#desription()
	 */
	@Override
	public String desription() {
		return context.getString(R.string.grandmaster_trophy_description);
	}

}
