/**
 * 
 */
package mu.zz.zagen.morse.Transmitters;

import mu.zz.zagen.morse.R;
import mu.zz.zagen.morse.SettingsActivity;
import mu.zz.zagen.morse.General.TextCodec;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.widget.Toast;

/**
 * @author zagen
 *
 */
public class ToneTransmitter extends Transmitter {

	
	public ToneTransmitter(Context context, TextCodec codec) {
		super(context, codec);
		init();
	}

	public ToneTransmitter(final Context context, TextCodec codec, Transmitter.OnTransmittanceEndedListener listener) {
		super(context, codec, listener);
		init();
	}
	/* (non-Javadoc)
	 * @see mu.zz.zagen.morse.Transmitters.Transmitter#transmit()
	 */
	private double duration  = getDuration();     					// milliseconds
	private double frequence;           				// hz
	private int sampleRate = 8000;             			// a number

	private double dnumSamples;
    
	private int numSamples;
	private double sample[];
	private byte generatedSnd[];

	private AudioTrack audioTrack = null;
	
	private int position = 0;
	
	private int volume;
	private boolean isTransmissionInProgress;
	
	
	private void init(){
		//reading preferences
		SharedPreferences settings = getContext().getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		volume = settings.getInt(SettingsActivity.VOLUME, 10);
		AudioManager manager = (AudioManager)getContext().getSystemService(Context.AUDIO_SERVICE);
		//setting volume from our preference
		manager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		frequence = settings.getInt(SettingsActivity.FREQUENCE, 1000);
	}

	/**
	 * @param entext - encoded text, for that will calculating duration of track 
	 * @return - whole duration of future track
	 */
	private int getTrackDuration(String entext){
		int wholeDuration = 0;
		for(char c : entext.toCharArray()){
			switch(c){
			case '-':
				wholeDuration += duration * 4; 
				break;
			case '.':
				wholeDuration += duration * 2 ;
				break;
			case ' ':
				wholeDuration += duration * 3;
				break;
				default:
					wholeDuration += duration * 7;
			}
		}
		return wholeDuration;
	}
	
	/**
	 * @param entext - morse code for generating track
	 */
	private void generateTrack(String entext){
		entext += '\u0000';
		int trackDuration = getTrackDuration(entext);
		dnumSamples = Math.ceil(trackDuration * sampleRate/1000);
		numSamples = (int) dnumSamples;
    	sample= new double[numSamples];
    	generatedSnd = new byte[2 * numSamples];
  	
    	for (int i = 0; i < numSamples; ++i) {      // Fill the sample array
            sample[i] = Math.sin(this.frequence * 2 * Math.PI * i / (sampleRate));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalized.
        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
    	int currentSample = 0;
        int idx = 0;
        int i = 0 ;
		for(char c : entext.toCharArray()){
			switch(c){
			case '-': 
			{
				//generating dash simple
				int oneSoundSamples = (int) Math.floor(duration * 3 * sampleRate/1000);						// quantity of samples
				int ramp = oneSoundSamples / 20 ;                                    						// Amplitude ramp as a percent of sample count


		        for (i = currentSample; i< currentSample + ramp; ++i) {                                     // Ramp amplitude up (to avoid clicks)
		            double dVal = sample[i];
		            // Ramp up to maximum
		            final short val = (short) ((dVal * Short.MAX_VALUE) * ((double)i - currentSample)/(ramp*2));
		            // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }


		        for (; i< currentSample + oneSoundSamples - ramp; ++i) {                        // Max amplitude for most of the samples
		            double dVal = sample[i];
		                                                                        // scale to maximum amplitude
		            final short val = (short) ((dVal * Short.MAX_VALUE));
		                                                                        // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }

		        for (; i< currentSample + oneSoundSamples; ++i) {                               // Ramp amplitude down
		            double dVal = sample[i];
		                                                                        // Ramp down to zero
		            final short val = (short) ((dVal * Short.MAX_VALUE) * (currentSample + oneSoundSamples - i)/((double)ramp*2));
		                                                                        // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }
		        currentSample += oneSoundSamples;
		        
		        //silence in 1 dot period
		        int silentSamples = (int) Math.floor(duration * sampleRate/1000);
		        for(i = currentSample; i < currentSample + silentSamples; i++){
		        	generatedSnd[idx++] = 0;
		            generatedSnd[idx++] = 0;
		        }
		        currentSample += silentSamples;
			}
				break;
			case '.':
			{
				int oneSoundSamples = (int) Math.floor(duration * sampleRate/1000);
				int ramp = oneSoundSamples / 50 ;                                    // Amplitude ramp as a percent of sample count


		        for (i = currentSample; i< currentSample + ramp; ++i) {                                     // Ramp amplitude up (to avoid clicks)
		            double dVal = sample[i];
		                                                                        // Ramp up to maximum
		            final short val = (short) ((dVal * Short.MAX_VALUE) *  ((double)i - currentSample)/(ramp*2));
		                                                                        // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }


		        for (; i< currentSample + oneSoundSamples - ramp; ++i) {                        // Max amplitude for most of the samples
		            double dVal = sample[i];
		                                                                        // scale to maximum amplitude
		            final short val = (short) ((dVal * Short.MAX_VALUE));
		                                                                        // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }

		        for (; i< currentSample + oneSoundSamples; ++i) {                               // Ramp amplitude down
		            double dVal = sample[i];
		                                                                        // Ramp down to zero
		            final short val = (short) ((dVal * Short.MAX_VALUE) * (currentSample + oneSoundSamples - i)/((double)ramp*2));
		                                                                        // in 16 bit wav PCM, first byte is the low order byte
		            generatedSnd[idx++] = (byte) (val & 0x00ff);
		            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		        }
		        currentSample += oneSoundSamples;
		        int silentSamples = (int) Math.floor(duration * sampleRate/1000);
		        for(i = currentSample; i < currentSample + silentSamples; i++){
		        	generatedSnd[idx++] = 0;
		            generatedSnd[idx++] = 0;
		        }
		        currentSample += silentSamples;
			}
				break;
			case ' ':
				{
					int silentSamples = (int) Math.floor(duration *3* sampleRate/1000);
				
			        for(i = currentSample; i < currentSample + silentSamples; i++){
			        	generatedSnd[idx++] = 0;
			            generatedSnd[idx++] = 0;
			        }
			        currentSample += silentSamples;
				}
				break;
				default:
				{
					int silentSamples = (int) Math.floor(duration *7* sampleRate/1000);
			        for(i = currentSample; i < currentSample + silentSamples; i++){
			        	generatedSnd[idx++] = 0;
			            generatedSnd[idx++] = 0;
			        }
			        currentSample += silentSamples;
				}
			}
		}
	}
	/* (non-Javadoc)
	 * @see mu.zz.zagen.morse.Transmitters.Transmitter#transmit()
	 */
	@Override
	public void transmit() {
		isTransmissionInProgress = true;
		if(audioTrack != null)
			synchronized (audioTrack) {
				if(audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PAUSED){
					audioTrack.setPlaybackHeadPosition(position);
					audioTrack.play();
					return;
				}
			}
		
		final String encodedText = getCodec().getEncodedText(); 
		
		stopFlag = false;
		if(encodedText.length() == 0)
		{
			Toast.makeText(getContext(), getContext().getResources().getString(R.string.empty_encoded_text), Toast.LENGTH_LONG).show();
			if(transmittanceEndedListener != null)
				transmittanceEndedListener.end();
			return;
		}
		
		
        Thread playThread = new Thread(new Runnable() {
			@Override
				public void run() {

					/*if(audioTrack != null){
						audioTrack.flush();
						audioTrack = null;
					}*/
						
					stopFlag = false;
					generateTrack(encodedText);
					try {
			            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
			                    sampleRate, AudioFormat.CHANNEL_OUT_MONO,
			                    AudioFormat.ENCODING_PCM_16BIT, (int)numSamples*2,
			                    AudioTrack.MODE_STATIC);
			            audioTrack.write(generatedSnd, 0, generatedSnd.length);     // Load the track
			            audioTrack.play();                                          // Play the track
			        }
			        catch (Exception e){
			            e.printStackTrace();
			        }

			        int x = 0;
			        do{                                                     // Monitor playback to find when done 
			           	 try{
			           		 x = audioTrack.getPlaybackHeadPosition();
			           	 }catch(Exception e){
			           		 x = numSamples;
			           	 }
			                         
			        }while ((x<numSamples) && !stopFlag);
			        
			        if (audioTrack != null) audioTrack.release();    
					
					if(transmittanceEndedListener != null)
						transmittanceEndedListener.end();
					isTransmissionInProgress = false;
				}
			});
		playThread.start();
	}

	@Override
	 public void pause(){
		 if(audioTrack != null && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING)
			 synchronized (audioTrack) {
				position = audioTrack.getPlaybackHeadPosition();
				audioTrack.pause();
			 }
	 }
	 public boolean isPaused(){
		 return (audioTrack != null && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PAUSED);
	 }
	 @Override
	 public void stop(){
		 super.stop();
		 if(audioTrack != null){
			 
			 synchronized (audioTrack) {
				  if(audioTrack.getState() == AudioTrack.STATE_INITIALIZED){
					  try {
						  audioTrack.flush();
						  audioTrack.pause();
						} catch (Exception e) {
							
						}finally{
							audioTrack = null;
						}
				  }
			 }
			 audioTrack = null;
		}
		System.gc();
	 }
	 
	 public boolean isTransmissionInProgress(){
		 return isTransmissionInProgress;
	 }
}
