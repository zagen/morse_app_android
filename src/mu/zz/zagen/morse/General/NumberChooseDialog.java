/**
 * 
 */
package mu.zz.zagen.morse.General;

import java.util.regex.Pattern;

import mu.zz.zagen.morse.R;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * @author zagen
 *
 */
public class NumberChooseDialog extends Builder {
	Context context;

	public interface OnFinishDialogListener{
		void onFinish(int number);
	}
	
	
	private OnFinishDialogListener onFinishListener;
	private EditText editText = null;
	private int min = 1;
	private int max = 100;
	private InputFilter percentInputFilter = new InputFilter() {
		
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
								   Spanned dest, int dstart, int dend) {
			 String checkedText = dest.subSequence(0, dstart).toString() +
			        source.subSequence(start, end) +
			        dest.subSequence(dend,dest.length()).toString();
			 String pattern = getPattern();
			   
			 if(!Pattern.matches(pattern, checkedText)) {
			      return "";
			 }else{
				 int number = 0;
				 try{
					 number = Integer.parseInt(checkedText);
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				 if(number > max || number < min)
					 return "";
			 }
			   
			 return null;
		}
		private String getPattern() {
			String pattern = "[0-9]{0,3}";  
			return pattern;
		}
	};
	
	public NumberChooseDialog(Context arg0) {
		super(arg0);
		this.context = arg0;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = inflater.inflate(R.layout.percent_choose_dialog_layout, null);
		setView(view);
		editText = (EditText)view.findViewById(R.id.pcd_edittext); 
		editText.setFilters(new InputFilter[]{percentInputFilter});
		editText.setText(Integer.toString(min));
		setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String numberStr = editText.getText().toString();
				if(onFinishListener != null && numberStr.length() != 0){
					onFinishListener.onFinish(Integer.parseInt(editText.getText().toString()));
				}
			}
		});
		setNegativeButton(R.string.cancel, null);
	}

	
	
	

	/**
	 * @param onFinishListener the onFinishListener to set
	 */
	public NumberChooseDialog setOnFinishListener(OnFinishDialogListener onFinishListener) {
		this.onFinishListener = onFinishListener;
		return this;
	}
	
	public NumberChooseDialog setRange(int min, int max){
		this.min = (min < max) ? min : max;
		this.max = (min < max) ? max : min;
		return this;
	}
	public NumberChooseDialog setDefault(int number){
		if(editText != null){
			editText.setText(Integer.toString(number));
		}
		return this;
	}
}
