package mu.zz.zagen.morse.General;

import java.util.HashMap;
import java.util.Map;

public class TextCodec {
	public static final int ALL = 2;
	public static final int RUS = 1;
	public static final int INT = 0;
	private int currentLocale = INT;
	
	private int localeByDefault;
	private String decodedText = "";
	private String encodedText = "";

	private final static Map<Character, String> encMapIntLet = new HashMap<Character, String>();
	static {
		encMapIntLet.put('a', ".-");
		encMapIntLet.put('b', "-...");
		encMapIntLet.put('c', "-.-.");
		encMapIntLet.put('d', "-..");
		encMapIntLet.put('e', ".");
		encMapIntLet.put('f', "..-.");
		encMapIntLet.put('g', "--.");
		encMapIntLet.put('h', "....");
		encMapIntLet.put('i', "..");
		encMapIntLet.put('j', ".---");
		encMapIntLet.put('k', "-.-");
		encMapIntLet.put('l', ".-..");
		encMapIntLet.put('m', "--");
		encMapIntLet.put('n', "-.");
		encMapIntLet.put('o', "---");
		encMapIntLet.put('p', ".--.");
		encMapIntLet.put('q', "--.-");
		encMapIntLet.put('r', ".-.");
		encMapIntLet.put('s', "...");
		encMapIntLet.put('t', "-");
		encMapIntLet.put('u', "..-");
		encMapIntLet.put('v', "...-");
		encMapIntLet.put('w', ".--");
		encMapIntLet.put('x', "-..-");
		encMapIntLet.put('y', "-.--");
		encMapIntLet.put('z', "--..");		
		encMapIntLet.put('.', ".-.-.-");
		encMapIntLet.put(',', "--..--");
	}
	private final static Map<String, String> decMapIntLet = new HashMap<String, String>();
	static{
		for (Character key: encMapIntLet.keySet()) {
			decMapIntLet.put(encMapIntLet.get(key), key.toString());			
		}
	}
	private final static Map<Character, String> encMapRusLet = new HashMap<Character, String>();
	static{
		encMapRusLet.put('�', ".-");
		encMapRusLet.put('�', "-...");
		encMapRusLet.put('�', ".--");
		encMapRusLet.put('�', "--.");
		encMapRusLet.put('�', "-..");
		encMapRusLet.put('�', ".");
		encMapRusLet.put('�', ".");
		encMapRusLet.put('�', "...-");
		encMapRusLet.put('�', "--..");
		encMapRusLet.put('�', "..");
		encMapRusLet.put('�', ".---");
		encMapRusLet.put('�', "-.-");
		encMapRusLet.put('�', ".-..");
		encMapRusLet.put('�', "--");
		encMapRusLet.put('�', "-.");
		encMapRusLet.put('�', "---");
		encMapRusLet.put('�', ".--.");
		encMapRusLet.put('�', ".-.");
		encMapRusLet.put('�', "...");
		encMapRusLet.put('�', "-");
		encMapRusLet.put('�', "..-");
		encMapRusLet.put('�', "..-.");
		encMapRusLet.put('�', "....");
		encMapRusLet.put('�', "-.-.");
		encMapRusLet.put('�', "---.");
		encMapRusLet.put('�', "----");
		encMapRusLet.put('�', "--.-");
		encMapRusLet.put('�', "--.--");
		encMapRusLet.put('�', "-.--");
		encMapRusLet.put('�', "-..-");
		encMapRusLet.put('�', "..-..");
		encMapRusLet.put('�', "..--");
		encMapRusLet.put('�', ".-.-");
		encMapRusLet.put('.', "......");
		encMapRusLet.put(',', ".-.-.-");
	}
	private final static Map<String, String> decMapRusLet = new HashMap<String, String>();
	static{
		for (Character key: encMapRusLet.keySet()) {
			decMapRusLet.put(encMapRusLet.get(key), key.toString());			
		}
	}
	private final static Map<Character, String> encMapSymbols = new HashMap<Character, String>();
	static{
		encMapSymbols.put('1', ".----");
		encMapSymbols.put('2', "..---");
		encMapSymbols.put('3', "...--");
		encMapSymbols.put('4', "....-");
		encMapSymbols.put('5', ".....");
		encMapSymbols.put('6', "-....");
		encMapSymbols.put('7', "--...");
		encMapSymbols.put('8', "---..");
		encMapSymbols.put('9', "----.");
		encMapSymbols.put('0', "-----");
		encMapSymbols.put(':', "---...");
		encMapSymbols.put(';', "-.-.-.");
		encMapSymbols.put('(', "-.--.");
		encMapSymbols.put(')', "-.--.-");
		encMapSymbols.put('\'', ".----.");
		encMapSymbols.put('"', ".-..-.");
		encMapSymbols.put('-', "-....-");
		encMapSymbols.put('/', "-..-.");
		encMapSymbols.put('?', "..--..");
		encMapSymbols.put('!', "--..--");
		encMapSymbols.put('\n', "-...-");
		encMapSymbols.put('^', "........");
		encMapSymbols.put('@', ".--.-.");
		encMapSymbols.put('~', "..-.-");
		encMapSymbols.put(' ', "^");
	
	}
	private final static Map<String, String> decMapSymbols = new HashMap<String, String>();
	static{
		for (Character key: encMapSymbols.keySet()) {
			decMapSymbols.put(encMapSymbols.get(key), key.toString());			
		}
	}
	public void setLocale (final int locale) 
	{
		if(locale == RUS  || locale == INT  || locale == ALL)
			currentLocale = locale;
		else
		{
			currentLocale = INT;
		}
	}
	
	public void setText(final String text)
	{
		decodedText = text;
		encode();
	}
	public void setMorse(final String text)
	{
		StringBuilder strBuilder = new StringBuilder(text);
		int pos = 0;
		while(pos < strBuilder.length())
		{
			if(strBuilder.charAt(pos) != '-' && strBuilder.charAt(pos) != '.' && strBuilder.charAt(pos) != ' ')
			{
				strBuilder.deleteCharAt(pos);
			}
			else
				pos++;
		}
		encodedText = strBuilder.toString();
		decode();
	}
	
	private void encode()
	{
		StringBuilder strBuilder = new StringBuilder();
		int pos = 0;
		while(pos < decodedText.length())
		{
			char currChar = Character.toLowerCase(decodedText.charAt(pos));
			if(currentLocale == INT)
			{
				if(	encMapIntLet.containsKey(currChar))	//if current character is latin, then encode it with map;
					
					{
						strBuilder.append(encMapIntLet.get(currChar));
					}
				else 
				{
					if(encMapSymbols.containsKey(currChar))
					{
						strBuilder.append(encMapSymbols.get(currChar));
					}
				}
				
			}
			if(currentLocale == RUS)
			{
				if(encMapRusLet.containsKey(currChar))
					{
						strBuilder.append(encMapRusLet.get(currChar));
					}
				else 
				{
					if(encMapSymbols.containsKey(currChar))
					{
						strBuilder.append(encMapSymbols.get(currChar));
					}
				}
				
			}
			if(currentLocale == ALL)
			{
				//cause period and comma has different Morse code for each locale, it need extra check
				if(currChar == '.' || currChar == ','){
					if(localeByDefault == RUS){
						strBuilder.append(encMapRusLet.get(currChar));
					}else{
						strBuilder.append(encMapIntLet.get(currChar));
					}
				}else{
				
					if(encMapIntLet.containsKey(currChar)){
						strBuilder.append(encMapIntLet.get(currChar));
					}
					else{
						if(encMapRusLet.containsKey(currChar))
						{
							strBuilder.append(encMapRusLet.get(currChar));
						}else
							if(encMapSymbols.containsKey(currChar))
							{
								strBuilder.append(encMapSymbols.get(currChar));
							}
					}
				}
			}
			pos++;
			strBuilder.append(' ');
		}
		encodedText = strBuilder.toString();
		encodedText = encodedText.trim();
	}
	private void decode()
	{
		StringBuilder strBuilder = new StringBuilder();
		String [] characters = encodedText.split(" ");
		
		if(currentLocale == INT){
			for (String character : characters) {
				if(decMapIntLet.containsKey(character)){
					strBuilder.append(decMapIntLet.get(character));
				}else if(decMapSymbols.containsKey(character)){
					strBuilder.append(decMapSymbols.get(character));
				}
			}
		}
		if(currentLocale == RUS){
			for (String character : characters) {
				if(decMapRusLet.containsKey(character)){
					strBuilder.append(decMapRusLet.get(character));
				}else if(decMapSymbols.containsKey(character)){
					strBuilder.append(decMapSymbols.get(character));
				}
			}
		}
		decodedText = strBuilder.toString();
	}
	public final String getEncodedText()
	{
		return encodedText;
	}
	public final String getDecodedText()
	{
		return decodedText;
	}

	/**
	 * @return the localeByDefault
	 */
	public int getLocaleByDefault() {
		return localeByDefault;
	}

	/**
	 * @param localeByDefault the localeByDefault to set
	 */
	public void setLocaleByDefault(int localeByDefault) {
		this.localeByDefault = localeByDefault;
	}
}
