/**
 * 
 */
package mu.zz.zagen.morse.Achievements;

import mu.zz.zagen.morse.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @author zagen
 *
 */
public class ApprenticeAchievement extends Achievement {

	public static final String NAME = "APPRENTICE_NAME";
	public ApprenticeAchievement(Context context, int rightAnswerAmount,
			int totalCharAmount) {
		super(context, rightAnswerAmount, totalCharAmount);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#name()
	 */
	@Override
	public String name() {
		return context.getString(R.string.apprentice_trophy_name);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#getPictureTrophy()
	 */
	@Override
	public Bitmap getPictureTrophy() {
		return BitmapFactory.decodeResource(context.getResources(), R.drawable.apprentice_trophy);
	}

	/* (non-Javadoc)
	 * @see Achievements.Achievement#desription()
	 */
	@Override
	public String desription() {
		return context.getString(R.string.apprentice_trophy_description);
	}

	

}
