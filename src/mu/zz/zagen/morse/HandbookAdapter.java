/**
 * 
 */
package mu.zz.zagen.morse;

import java.util.ArrayList;

import mu.zz.zagen.morse.General.StackMorseView;
import mu.zz.zagen.morse.General.TextCodec;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author zagen
 * This is adapter for handbook list
 */
public class HandbookAdapter extends BaseAdapter {

	private Context context;
	private TextCodec codec = new TextCodec();
	private static char[] intLet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,:;()\"'-/?!@".toCharArray();
	private static char[] rusLet = "�����Ũ��������������������������1234567890.,:;()\"'-/?!@".toCharArray();
	private ArrayList<String> displayedCharacters = new ArrayList<String>();
	private ArrayList<String> displayedCodes = new ArrayList<String>();
	
	private LayoutInflater inflater;
	
	public HandbookAdapter(Context context){
		super();
		this.context = context;
		SharedPreferences settings = this.context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		
		int locale = settings.getInt(SettingsActivity.LOCALE, 0);
		codec.setLocale(locale);
		
		
		inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(locale == TextCodec.RUS)
		{
			for(char symbol: rusLet){
				displayedCharacters.add(Character.toString(symbol));
				codec.setText(Character.toString(symbol));
				displayedCodes.add(codec.getEncodedText());
			}
		}else
		{
			for(char symbol: intLet){
				displayedCharacters.add(Character.toString(symbol));
				codec.setText(Character.toString(symbol));
				displayedCodes.add(codec.getEncodedText());
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return displayedCharacters.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public String getItem(int position) {
		return displayedCharacters.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = inflater.inflate(R.layout.handbook_list_item_layout, null);
		
		TextView textView = (TextView)view.findViewById(R.id.handbook_character);
		textView.setText(displayedCharacters.get(position));
		StackMorseView stack = (StackMorseView)view.findViewById(R.id.handbook_stack_view);
		stack.clear();
		stack.push(displayedCodes.get(position));
		return view;
	}

}
