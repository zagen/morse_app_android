package mu.zz.zagen.morse.General;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;


public class StackMorseView extends View {

	
	private int dotSize = 10; //diameter of dot 
	private int margin = 20;
	private List<Character> symbols = new ArrayList<Character>();
	private Paint paint = new Paint();
	public StackMorseView(Context context) {
		super(context);
		setBackgroundColor(Color.BLACK);
		paint.setColor(Color.WHITE);
	}


	public StackMorseView(Context context, AttributeSet attrs) {
	    super(context, attrs);
		setBackgroundColor(Color.BLACK);
		paint.setColor(Color.WHITE);
	}

	public StackMorseView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
		setBackgroundColor(Color.BLACK);
		paint.setColor(Color.WHITE);
	}

	public void push(Character symbol){
		symbols.add(0, symbol);
		invalidate();
		
	}
	public void push(String symbolsStr){
		for(char symbol : symbolsStr.toCharArray()){
			symbols.add(0, symbol);
		}
		invalidate();
	}
	public void clear()
	{
		
		symbols.removeAll(symbols);
		invalidate();
	}
	@Override
	protected void onDraw(Canvas canvas) {

		Iterator<Character> iterator = symbols.iterator();
		int posX = getWidth() - margin;
		
		int alpha = 255;
		int stepAlpha = 10;
		while(iterator.hasNext())
		{
			Character currentShape = iterator.next();
						
			if(posX >= 0)
			{
				paint.setAlpha(alpha);
				if(currentShape == '-')
				{
					posX -= dotSize * 3;
					canvas.drawRect(posX, margin, posX + dotSize * 3,  margin + dotSize, paint);
					alpha -= stepAlpha;
				}
				else
				{if(currentShape == '.'){
						posX -= dotSize/2;
						canvas.drawCircle(posX, margin + dotSize / 2, dotSize/2, paint);
						posX -= dotSize/2;
						alpha -= stepAlpha;
				}else if(currentShape == ' '){
					posX -= dotSize*2;
				}else
				{
					posX -= dotSize*6;
				}
				}
				posX -= dotSize;
			}else break;
			
		}
		while(iterator.hasNext())
		{
			iterator.remove();
			iterator.next();
		}
		super.onDraw(canvas);
	}
}
