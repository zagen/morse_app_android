/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.TextCodec;
import mu.zz.zagen.morse.Transmitters.ToneTransmitter;
import mu.zz.zagen.morse.Transmitters.Transmitter;
import mu.zz.zagen.morse.Transmitters.VibroTransmitter;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.IBinder;

/**
 * @author zagen
 *
 */
public class SMSPlayerService extends Service {

	private Transmitter transmitterTone;
	private Transmitter transmitterVibro;
	private Transmitter currentTransmitter;
	private TextCodec textCodec;
	private final IntentFilter theFilter = new IntentFilter();
    /** Check if screen off*/
	private BroadcastReceiver mPowerKeyReceiver= null;
	private SharedPreferences settings;
  
	private AudioManager audioManager;
	private int ringerMode;
	public static final String DATA = "data";
	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		textCodec = new TextCodec();
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		
		Transmitter.OnTransmittanceEndedListener onStoplistener = new Transmitter.OnTransmittanceEndedListener() {
			
			@Override
			public void end() {
				SMSPlayerService.this.stopSelf();		
				audioManager.setRingerMode(ringerMode);
			}
		} ;
		transmitterTone = new ToneTransmitter(this, textCodec, onStoplistener);
		transmitterVibro = new VibroTransmitter(this, textCodec, onStoplistener);
		
		textCodec.setLocale(TextCodec.ALL);
		
	    theFilter.addAction(Intent.ACTION_SCREEN_OFF);

	    mPowerKeyReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            String strAction = intent.getAction();

	            if (strAction.equals(Intent.ACTION_SCREEN_OFF) ) {
	            	stopSelf();
	            	audioManager.setRingerMode(ringerMode);
	            }
	        }
	    };

	    registerReceiver(mPowerKeyReceiver, theFilter);

	    settings = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		transmitterTone.stop();
		transmitterTone = null;
		transmitterVibro.stop();
		transmitterVibro = null;
		audioManager.setRingerMode(ringerMode);
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		ringerMode = audioManager.getRingerMode();
		boolean isTransmitterVibro = settings.getBoolean(SettingsActivity.IS_SMS_TRANSMITTER_VIBRO, false);
		audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		currentTransmitter = (isTransmitterVibro) ? transmitterVibro : transmitterTone;
	    String message = intent.getExtras().getString(DATA);
	    textCodec.setText(message.toLowerCase());
	    currentTransmitter.transmit();
	    return Service.START_NOT_STICKY;
	}

	
}
