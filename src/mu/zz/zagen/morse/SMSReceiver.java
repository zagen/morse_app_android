package mu.zz.zagen.morse;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {

        private static final String TAG = "SMSReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
             Log.i(TAG, "Intent recieved: " + intent.getAction());

             SharedPreferences settings = context.getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
             boolean isSmsPlaying = settings.getBoolean(SettingsActivity.IS_SMS_RECEIVING, false);
             
             if (isSmsPlaying) {
            	 String message = "";
            	 Intent serviceStopIntent = new Intent();
            	 serviceStopIntent.setClass(context, SMSPlayerService.class);
            	 context.stopService(serviceStopIntent);
            	 Bundle bundle = intent.getExtras();
                 if (bundle != null) {
                	 Object[] pdus = (Object[])bundle.get("pdus");
                     final SmsMessage[] messages = new SmsMessage[pdus.length];
                     for (int i = 0; i < pdus.length; i++) {
                     	messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        message += messages[i].getMessageBody();
                     }

                }
                if(message.length() >= 1){
                	Intent serviceStartIntent = new Intent();
                	serviceStartIntent.setClass(context, SMSPlayerService.class);
                	serviceStartIntent.putExtra(SMSPlayerService.DATA, message);
                	context.startService(serviceStartIntent);
                	
                }
           }
        }
    }