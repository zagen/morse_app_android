/**
 * 
 */
package mu.zz.zagen.morse.General;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import mu.zz.zagen.morse.R;

import android.content.Context;
import android.widget.Toast;

/**
 * @author zagen
 *
 */

public class WordsAccessor {
	 private static volatile WordsAccessor instance;
	 private final ArrayList<String> wordsRus = new ArrayList<String>();
	 private final ArrayList<String> wordsEng = new ArrayList<String>();
	 private boolean loadedSuccess = true;
	 private final Random random = new Random();
	 private WordsAccessor(Context context){
			InputStream instFile = context.getResources().openRawResource(R.raw.rus);
			
			InputStreamReader ioReader  = new InputStreamReader(instFile);
			BufferedReader bfReader		= new BufferedReader(ioReader);
			String word;
			
			try {
				while((word = bfReader.readLine()) != null){
					wordsRus.add(word.trim());
				}
				
				bfReader.close();
				ioReader.close();
				instFile.close();
			} catch (IOException e) {
				Toast.makeText(context, R.string.io_error, Toast.LENGTH_SHORT)
					 .show();
				loadedSuccess = false;
				e.printStackTrace();
			} 
			
			instFile = context.getResources().openRawResource(R.raw.eng);
			
			ioReader  = new InputStreamReader(instFile);
			bfReader  = new BufferedReader(ioReader);
						
			try {
				while((word = bfReader.readLine()) != null){
					wordsEng.add(word.trim());
				}
				bfReader.close();
				ioReader.close();
				instFile.close();
				
			} catch (IOException e) {
				Toast.makeText(context, R.string.io_error, Toast.LENGTH_SHORT)
					 .show();
				loadedSuccess = false;
				e.printStackTrace();
			}

	 }
     public static WordsAccessor getInstance(Context context) {
     WordsAccessor localInstance = instance;
     if (localInstance == null) {
         synchronized (WordsAccessor.class) {
             localInstance = instance;
             if (localInstance == null) {
                 instance = localInstance = new WordsAccessor(context);
             }
         }
     }
     return localInstance;
  }
     public String getRandomWord(int locale){
    	 if(!loadedSuccess)
    		 return "";
    	 if(locale == Generator.RUS){
    		 return wordsRus.get(random.nextInt(wordsRus.size()));
    	 }else
    		 return wordsEng.get(random.nextInt(wordsEng.size()));
     }
}