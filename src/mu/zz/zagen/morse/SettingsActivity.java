package mu.zz.zagen.morse;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;

public class SettingsActivity extends Activity implements OnClickListener, OnSeekBarChangeListener{

	/**
	 * id of preferences for this apps
	 */
	public static final String PREFS_NAME = "MorsePrefFile";
	
	/**
	 * id for locale setting
	 */
	public static final String LOCALE = "locale";
	
	/**
	 * id for type of transmitter setting
	 */
	public static final String TRANSMITTER = "transmitter";
	
	/**
	 * id for locale setting
	 */
	public static final String DURATION = "duration";
	
	/**
	 * id for volume setting
	 */
	public static final String VOLUME = "volume";
	
	/**
	 * id for setting brightness in playing with screen mode
	 */
	public static final String BRIGHTNESS = "brightness";
	
	/**
	 * id for setting length of random generating sequence
	 */
	public static final String GEN_LENGTH = "gen_length";
	public static final String SYMBOLS_RUS = "symbols_rus";
	public static final String SYMBOLS_INT = "symbols_int";
	
	/**
	 * id for sequence type setting. Can be either random sequence or words
	 */
	public static final String SEQUENCE_TYPE = "sequence_type";
	
	/**
	 * id for frequence setting
	 */
	public static final String FREQUENCE = "frequence";

	/**
	 * id for setting control playing incoming sms
	 */
	public static final String IS_SMS_RECEIVING = "is_sms_receiving";
	
	/**
	 * id for setting control playing incoming sms with vibro
	 */
	public static final String IS_SMS_TRANSMITTER_VIBRO = "sms_transmitter_vibro";
	
	/**
	 * id for setting amount of group on receiving
	 */
	public static final String GROUP_NUMBER = "group_number";
	
	public static final int MAX_GROUP_NUMBER = 30;
	
	public static final int MAX_DELAY_BEFORE_RECEIVE_EXER = 5;
	
	public static final String DELAY_BEFORE_RECEIVE_EXER = "delay_before_receive_exercise" ;
	
	
	
	public static final String IS_MAIN_MENU_ANIMATION_ON = "is_main_menu_animation_on"; 
	
	public static final String LAST_FILE_URI_STRING = "last_file_uri_string";
	
	public static final String LAST_FILE_POSITION_LONG = "last_file_position_long";
	/**
	 * min wpm available 
	 */
	private static final int WPM_LEFT_BORDER = 5;

	/**
	 * here stores settings
	 */
	private SharedPreferences settings;
	
	/**
	 * labels for settings
	 */
	private TextView volumeTxtView, brightnessTxtView, durationTxtView, genLengthTxtView, frequenceTxtView, groupsNumberTxtView;
	
	/**
	 * seekbars for settings
	 */
	private SeekBar volumeSeekBar, brightnessSeekBar, durationSeekBar, genLengthSeekBar, frequenceSeekbar, groupsNumberSeekbar;

	
	private CheckBox playSmsChkBox, onAnimatedMenuChkBox;
	private RadioGroup smsTransmittersGroup;
	
	/**
	 * for volume changing
	 */
	private AudioManager am;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.settings_layout);
		setTitle(R.string.settings);
		volumeTxtView = (TextView)findViewById(R.id.set_volume_txt);
		brightnessTxtView = (TextView)findViewById(R.id.set_brightness_txt);
		durationTxtView = (TextView)findViewById(R.id.set_duration_txt);
		genLengthTxtView = (TextView)findViewById(R.id.set_generator_length_txt);
		frequenceTxtView = (TextView)findViewById(R.id.set_frequence_txt);
		groupsNumberTxtView=(TextView)findViewById(R.id.set_groups_number_txt);
		
		volumeSeekBar = (SeekBar)findViewById(R.id.set_volume_seekbar);
		volumeSeekBar.setOnSeekBarChangeListener(this);
		brightnessSeekBar = (SeekBar)findViewById(R.id.set_brightness_seekbar);
		brightnessSeekBar.setOnSeekBarChangeListener(this);
		durationSeekBar = (SeekBar)findViewById(R.id.set_duration_seekbar);
		durationSeekBar.setOnSeekBarChangeListener(this);
		genLengthSeekBar = (SeekBar)findViewById(R.id.set_generator_length_seekbar);
		genLengthSeekBar.setOnSeekBarChangeListener(this);
		frequenceSeekbar = (SeekBar)findViewById(R.id.set_frequence_seekbar);
		frequenceSeekbar.setOnSeekBarChangeListener(this);
		groupsNumberSeekbar=(SeekBar)findViewById(R.id.set_groups_number_seekbar);
		groupsNumberSeekbar.setOnSeekBarChangeListener(this);
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		
		//
		smsTransmittersGroup = (RadioGroup)findViewById(R.id.set_sms_transmitters_group);
		boolean isVibro = settings.getBoolean(IS_SMS_TRANSMITTER_VIBRO, false);
		if(isVibro)
			((RadioButton)(smsTransmittersGroup.getChildAt(1))).setChecked(true);
		else
			((RadioButton)(smsTransmittersGroup.getChildAt(0))).setChecked(true);
		
		playSmsChkBox = (CheckBox)findViewById(R.id.set_smsplaying_chxbox);
		boolean isSmsReceiving = settings.getBoolean(IS_SMS_RECEIVING, false);
		for(int i = 0; i < smsTransmittersGroup.getChildCount(); i++)
	    	((RadioButton)smsTransmittersGroup.getChildAt(i)).setEnabled(isSmsReceiving);
	    	
		playSmsChkBox.setChecked(isSmsReceiving);
		playSmsChkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences.Editor editor = settings.edit();
				for(int i = 0; i < smsTransmittersGroup.getChildCount(); i++){
			    	((RadioButton)smsTransmittersGroup.getChildAt(i)).setEnabled(isChecked);
				}
				editor.putBoolean(IS_SMS_RECEIVING, isChecked);
				editor.commit();
			}
		});
		
		
		int locale = settings.getInt(LOCALE, 0);
		if(locale == 0)
			((RadioButton)findViewById(R.id.set_locale_radio_1)).setChecked(true);
		else
			((RadioButton)findViewById(R.id.set_locale_radio_2)).setChecked(true);
		
		int transmitter = settings.getInt(TRANSMITTER, 0);
		switch(transmitter){
			case 0:
				((RadioButton)findViewById(R.id.set_tranmitter_radio_0)).setChecked(true);
			break;
			case 1:
				((RadioButton)findViewById(R.id.set_tranmitter_radio_1)).setChecked(true);
			break;
			case 2:
				((RadioButton)findViewById(R.id.set_tranmitter_radio_2)).setChecked(true);
			break;
			case 3:
				((RadioButton)findViewById(R.id.set_tranmitter_radio_3)).setChecked(true);
			break;
			case 4:
				((RadioButton)findViewById(R.id.set_tranmitter_radio_4)).setChecked(true);
			break;
		}

		int duration = settings.getInt(DURATION, 100);
		int wpm = Math.round(1200f/(duration));
		durationSeekBar.setProgress(wpm - WPM_LEFT_BORDER);
		
		durationTxtView.setText(getString(R.string.signal_duration) + ": " + wpm);
		
		int volume = settings.getInt(VOLUME, 10);
		volumeSeekBar.setProgress(volume);
		volumeTxtView.setText(getString(R.string.volume) + ": " + volume);

		
		int brightness = settings.getInt(BRIGHTNESS, 100);
		brightnessSeekBar.setProgress(brightness - 1);
		brightnessTxtView.setText(getString(R.string.brightness) + ": " + brightness);
		

		int length = settings.getInt(GEN_LENGTH, 2);
		genLengthSeekBar.setProgress(length - 1);
		genLengthTxtView.setText(getString(R.string.generator_length) + ": " + length);
		
		int frequence = settings.getInt(FREQUENCE, 1000);
		frequenceSeekbar.setProgress((frequence - 400)/20);
		frequenceTxtView.setText(getString(R.string.frequence_setting) + ": " + frequence);
		
		int groupsNumber = settings.getInt(GROUP_NUMBER, 1);
		groupsNumberSeekbar.setProgress(groupsNumber-1);
		groupsNumberSeekbar.setMax(MAX_GROUP_NUMBER - 1);
		groupsNumberTxtView.setText(getString(R.string.group_number_on_recieving) + groupsNumber);
		
		boolean isAnimatedMenuOn = settings.getBoolean(IS_MAIN_MENU_ANIMATION_ON, true);
		onAnimatedMenuChkBox = (CheckBox)findViewById(R.id.set_animation_main_menu_on_chkbox);
		onAnimatedMenuChkBox.setChecked(isAnimatedMenuOn);
		onAnimatedMenuChkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(IS_MAIN_MENU_ANIMATION_ON, isChecked);
				editor.commit();
			}
		});
		
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
			
		//and set current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
	}

	@Override
	public void onClick(View v) {
		SharedPreferences.Editor editor = settings.edit();
	    

		switch(v.getId()){
		case R.id.set_locale_radio_1:
			editor.putInt(LOCALE, 0);
			break;
		case R.id.set_locale_radio_2:
			editor.putInt(LOCALE, 1);
			break;
		case R.id.set_tranmitter_radio_0:
			editor.putInt(TRANSMITTER, 0);
			break;
		case R.id.set_tranmitter_radio_1:
			editor.putInt(TRANSMITTER, 1);
			break;
		case R.id.set_tranmitter_radio_2:
			editor.putInt(TRANSMITTER, 2);
			break;
		case R.id.set_tranmitter_radio_3:
			editor.putInt(TRANSMITTER, 3);
			break;
		case R.id.set_tranmitter_radio_4:
			editor.putInt(TRANSMITTER, 4);
			break;
		case R.id.set_sms_tranmitter_vibro:
			if(((RadioButton)(smsTransmittersGroup.getChildAt(1))).isEnabled())
				editor.putBoolean(IS_SMS_TRANSMITTER_VIBRO, true);
			break;
		case R.id.set_sms_tranmitter_tone:
			if(((RadioButton)(smsTransmittersGroup.getChildAt(0))).isEnabled())
				editor.putBoolean(IS_SMS_TRANSMITTER_VIBRO, false);
			break;
		}
		// Commit the edits!
		editor.commit();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		switch(seekBar.getId()){
		
		case R.id.set_duration_seekbar:
			durationTxtView.setText(getString(R.string.signal_duration) + ": " + (progress + WPM_LEFT_BORDER));
			break;
		case R.id.set_volume_seekbar:
			volumeTxtView.setText(getString(R.string.volume) + ": " + progress);
			break;
		case R.id.set_generator_length_seekbar:
			genLengthTxtView.setText(getString(R.string.generator_length) + ": " + (progress + 1));
			break;
		case R.id.set_brightness_seekbar:
			brightnessTxtView.setText(getString(R.string.brightness) + ": " + (progress + 1));
			break;
		case R.id.set_frequence_seekbar:
			frequenceTxtView.setText(getString(R.string.frequence_setting) + ": " + (progress*20 + 400));
			break;
		case R.id.set_groups_number_seekbar:
			groupsNumberTxtView.setText(getString(R.string.group_number_on_recieving) + (progress+1));
			break;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		SharedPreferences.Editor editor = settings.edit();

		switch(seekBar.getId()){
		
		case R.id.set_duration_seekbar:
			editor.putInt(DURATION, 
					Math.round(1200f/(seekBar.getProgress() + WPM_LEFT_BORDER)));
			break;
		case R.id.set_volume_seekbar:
			editor.putInt(VOLUME, seekBar.getProgress());
			am.setStreamVolume(AudioManager.STREAM_MUSIC, seekBar.getProgress(), 0);
			break;
		case R.id.set_generator_length_seekbar:
			editor.putInt(GEN_LENGTH, seekBar.getProgress() + 1);
			break;
		case R.id.set_brightness_seekbar:
			editor.putInt(BRIGHTNESS, seekBar.getProgress());
			break;
		case R.id.set_frequence_seekbar:
			editor.putInt(FREQUENCE, seekBar.getProgress()*20 + 400);
			break;
		case R.id.set_groups_number_seekbar:
			editor.putInt(GROUP_NUMBER, seekBar.getProgress() + 1);
		}
		editor.commit();
	}
}
