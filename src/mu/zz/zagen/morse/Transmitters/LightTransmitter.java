package mu.zz.zagen.morse.Transmitters;



import mu.zz.zagen.morse.SettingsActivity;
import mu.zz.zagen.morse.General.TextCodec;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;

public class LightTransmitter extends Transmitter {
	private Camera camera;
	private Camera.Parameters parameters;
	private boolean hasCamera = false;
	private boolean isLoop = false;
	private boolean isPause;
	
	private boolean useCameraFlashLight = false;
	public LightTransmitter(Context context, TextCodec codec) {
		super(context, codec);
		
	}
	public LightTransmitter(Context context, TextCodec codec, Transmitter.OnTransmittanceEndedListener listener) {
		super(context, codec,listener);
	}
		
	
    public void initCamera() {	
    	if (!hasCamera) {
			try {
		    	camera = Camera.open();
		    	hasCamera = (camera != null);
			} catch (RuntimeException e) {
				Log.e("Morse app", "Could not open Camera"+ e);
		    	hasCamera = false;
				return;
			}
	        
    	}
    }
    
    public void lightOff() {		//turning off the camera light
    	if (hasCamera) {
    		synchronized (camera) {
    			if (parameters == null) {
        			parameters = camera.getParameters();
        		}
    	        parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
    	        camera.setParameters(parameters);
			}
    		
    	}
    }

    public void lightOn() {			//turning on light
    	if (hasCamera) {
    		synchronized (camera) {
    			if (parameters == null) {
        			parameters = camera.getParameters();
        		}
    	        parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
    	        camera.setParameters(parameters);
			}
    		
    	} 
    }
    
    public boolean hasCamera() {
    	return hasCamera;
    }
	@Override
	public void transmit() {
		if(isPause){
			isPause = false;
			return;
		}

		stopFlag = false;
		initCamera();							// camera initialization 
		if(!hasCamera || !useCameraFlashLight) 	// checking possibility of using camera flash light or if flag set 
		{
			Intent intent = new Intent();		// transmitting with screen color;
			SharedPreferences settings = getContext().getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
			float brightness = (float)settings.getInt(SettingsActivity.BRIGHTNESS, 100)/100;
			intent.putExtra(LightTransmitterActivity.MORSECODE_ID, getCodec().getEncodedText());
			intent.putExtra(LightTransmitterActivity.DURATION_ID, getDuration());
			intent.putExtra(LightTransmitterActivity.BRIGHTNESS_ID, brightness);
			intent.putExtra(LightTransmitterActivity.IS_LOOP, isLoop);
			intent.setClass(getContext(), LightTransmitterActivity.class);
			getContext().startActivity(intent);
			
		}else
		{
			final String encodedText = getCodec().getEncodedText(); 
			if(encodedText.length() == 0)
			{
				//Toast.makeText(getContext(), getContext().getResources().getString(R.string.empty_encoded_text), Toast.LENGTH_LONG).show();
				if(transmittanceEndedListener != null)
					transmittanceEndedListener.end();				
				return;
			}
			
			
			Thread playThread = new Thread(new Runnable() {


				@Override
					public void run() {
						int currentPos = 0;
						String encodedText = getCodec().getEncodedText();
						int duration = getDuration();
						while(currentPos < encodedText.length() && !stopFlag)
						{
							if(!isPause){
								try {
									if(encodedText.charAt(currentPos) == '-')
									{				
										lightOn();
										Thread.sleep(duration * 3);
										lightOff();
										Thread.sleep(duration);
									}else{ if(encodedText.charAt(currentPos) == '.')
									{
										lightOn();
										Thread.sleep(duration);
										lightOff();
										Thread.sleep(duration);
										  					
									}else{ if(encodedText.charAt(currentPos) == ' '){
										Thread.sleep(duration*3);
									}else{
										Thread.sleep(duration*7);
										    
										}}}
								 } catch (InterruptedException e) {
								      e.printStackTrace();
								}
								currentPos++;
							}
						}
						camera.release();
						hasCamera = false;
						if(transmittanceEndedListener != null)
							transmittanceEndedListener.end();
						
					}
					
				});
			playThread.start();
			

		}
			}

	public void setUseCameraFlashLight(boolean usingFlag)
	{
		useCameraFlashLight = usingFlag;
	}
	public boolean getUseCameraFlashLight()
	{
		return useCameraFlashLight;
	}
	/**
	 * @return the isLoop
	 */
	public boolean isLoop() {
		return isLoop;
	}
	/**
	 * @param isLoop the isLoop to set
	 */
	public void setLoop(boolean isLoop) {
		this.isLoop = isLoop;
	}
	
	@Override
	public void pause(){
		if(useCameraFlashLight){
			isPause = true;
		}
	}

}
