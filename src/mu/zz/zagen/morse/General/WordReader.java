package mu.zz.zagen.morse.General;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;



/**
 * class for accessing words and page in file
 * @author zagen
 *
 */
public class WordReader {

	/**
	 * file name
	 */
	private String filename;
	
	/**
	 * object help for reading bytes from file non-sequentially 
	 */
	private RandomAccessFile raf;
	
	/**
	 * current position in the file in bytes
	 */
	private long currentPosition = 0;
	
	/**
	 * position of previous word 
	 */
	private long previousWordPosition = 0;
	
	/**
	 * start position for files(0 for 1-byte code pages and 2 for utf-16) 
	 */
	private long beginPosition = 0;
	
	/**
	 * name of file charset(code page)
	 */
	private String charset;
	
	/**
	 * amount of bytes in one character ( 1 - ANSI, 2 - UTF-16)
	 */
	private int bytesInOneChar = 1;
	
	/**
	 * size of one page of text( in bytes)
	 */
	private long pageSize;
	
	
	private long fileLength;
	
	/**
	 * public constructor
	 * @param filename of reading file
	 */
	public WordReader(String filename) {
		this.filename = filename;
		try {
			raf = new RandomAccessFile(filename, "r");
			fileLength = raf.length();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		setCharsetGuess();
		
	}


	private final static int BUFFER_SIZE = 40;
	
	/**
	 * give the next word from currentPosition
	 * @return
	 */
	public String nextWord(){

		previousWordPosition = currentPosition;
		//check and set charset
		if(charset == null){
			setCharsetGuess();
		}
		try {
			raf.seek(currentPosition);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		//here'll store our word
		StringBuilder wordSb = new StringBuilder();
		
		//store raw string in bytes from file
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		//store decoded string
		String bufferString = "";
		
		int readed = 0;
		try {
			//try to read string and transform into current charset
			readed = raf.read(bufferBytes);
			if(readed > 0){
				bufferString = new String(bufferBytes, charset);
			}else{
				return "";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String trimmedString = bufferString.trim();
		
		if(trimmedString.length() == 0){
			currentPosition += readed *bytesInOneChar;
			return "";
		}
		
		//index of first non-space character
		int beginOffset = bufferString.indexOf(trimmedString);
		
		
		int i = beginOffset;
		boolean isSpaceFinded = false;
		//sequentially take symbols from string and put into result word while current symbol isn't space
		for(i = beginOffset; i < readed/bytesInOneChar; i++){
			char currentChar = bufferString.charAt(i);
			wordSb.append(currentChar);
			
			if(Character.isWhitespace(currentChar)){
				isSpaceFinded = true;
				break;
			}
		}
		//offset current position on length of read word 
		currentPosition += i*bytesInOneChar;
		String result = wordSb.toString().trim();
		//if were find space add space at the and (other case - too long word and no one space reached)
		if(isSpaceFinded){
			result += " ";
		}
		return result;
	}
	
	/**
	 * 
	 * @return previous word (left to currentPosition)
	 */
	public String previousWord(){
		int localBufferSize = 0;
		//check and set charset
		if(charset == null){
			setCharsetGuess();
		}
		//check if somehow current position was set in appropriate value 
		if(currentPosition <= beginPosition){
			currentPosition = previousWordPosition = beginPosition;
			return "";
		}else{
			//size of read buffer may be less then BUFFER_SIZE if currentPosition place in the beginning of file
			localBufferSize = (int) ((currentPosition - BUFFER_SIZE < beginPosition)? (currentPosition - beginPosition) : BUFFER_SIZE); 
		}

		try {
			raf.seek(currentPosition - localBufferSize);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		StringBuilder wordSb = new StringBuilder();
		
		byte[] bufferBytes = new byte[localBufferSize];
		String bufferString = "";
		
		int readed = 0;
		try {
			//read raw bytes and convert into string 
			readed = raf.read(bufferBytes);
			if(readed > 0){
				bufferString = new String(bufferBytes, charset);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String trimmedString = bufferString.trim();
		
		if(trimmedString.length() == 0){
			currentPosition -= readed * bytesInOneChar;
			return "";
		}
		
		//find last non-space character
		int endOffset = bufferString.indexOf(trimmedString) + trimmedString.length() - 1;
		
		
		boolean isSpaceFinded = false;
		int i = endOffset;
		//read word
		for(i = endOffset; i >= 0; i--){
			char currentChar = bufferString.charAt(i);
			wordSb.append(currentChar);
			
			if(Character.isWhitespace(currentChar)){
				isSpaceFinded = true;
				break;
			}
		}
		//offset current position on length of read word
		currentPosition -= (bufferString.length() - i - 1)*bytesInOneChar;
		previousWordPosition = currentPosition;
		
		String result = wordSb.reverse().toString().trim();
		if(isSpaceFinded)
			result += " ";
		return result;
	}
	
	
	/**
	 * guess what the charset for current file is
	 */
	public void setCharsetGuess()
	{
	    CharsetMatch match;
	    try {
	        File file = new File(filename);
	        int fileDataLength= 200;
	        fileDataLength = ((int)file.length() < fileDataLength)? (int) file.length() : fileDataLength;
	        
	        byte [] fileData = new byte[fileDataLength];
	        DataInputStream dis = new DataInputStream(new FileInputStream(file));
	        dis.read(fileData);
	        dis.close();

	        match = new CharsetDetector().setText(fileData).detect();
	        
	        if(match != null){
	        	charset = match.getName();
	        	if(charset.contains("16")){
	        		beginPosition = 2;
	        		bytesInOneChar = 2;
	        	}else{
	        		beginPosition = 0;
	        		bytesInOneChar = 1;
	        	}
	        }
	        previousWordPosition = currentPosition = beginPosition;
	        fileData = null;
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	}

	/**
	 * get string from file (size is pageSize)
	 * @param number
	 * @return
	 */
	public String getPage(int number){
		//if charset isn't set try to guess
		if(charset == null){
			setCharsetGuess();
		}
		
		//calculate offset from beginning of file to our page
		currentPosition = pageSize * number + beginPosition;
		try {
			if(currentPosition >= raf.length() || number < 0){
				return "";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			raf.seek(currentPosition);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// try to read all the page
		int readedAll = 0;
		StringBuilder sb = new StringBuilder();
		while(readedAll < pageSize){
			//needed if page less then usual ones
			int localBufferSize = (int) ((pageSize - readedAll > BUFFER_SIZE)? BUFFER_SIZE : pageSize - readedAll);
			byte[] buffer = new byte[localBufferSize];
			try {
				raf.read(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				sb.append(new String(buffer, charset));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			readedAll += localBufferSize;
		}
		return sb.toString();
	}

	/**
	 * @return the pageSize
	 */
	public long getPageSize() {
		return pageSize;
	}

	
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}
	
	public double getCurrentPercent(){
		double result = (previousWordPosition - beginPosition)/(double)(fileLength - beginPosition);

		return result * 100;
	}
	
	public boolean isEnded(){
		return currentPosition >= fileLength;
	}
	
	public boolean isBegin(){
		return currentPosition <= beginPosition;
	}
	public void reset(){
		currentPosition = previousWordPosition = beginPosition;
	}

	public long moveToPercent(double percent){
		
		//if charset isn't set try to guess
		if(charset == null){
			setCharsetGuess();
		}
		
		long length = 0;
		
		length = fileLength - beginPosition;
		long position = (long) (length * (percent/100));
		if(bytesInOneChar == 2 && position % 2 != 0){
			position++;
		}
		byte[] buffer = new byte[bytesInOneChar];
		String checkString;
		
		long spacePosition = 0;
		do{
			
			checkString = "";
			spacePosition = position;
			
			if(position == 0){
				break;
			}
			try {
				raf.seek(position);
				raf.read(buffer);
				checkString = new String(buffer, charset);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			position -= bytesInOneChar;
			
		}while(checkString.trim().length() > 0);
		
		currentPosition = spacePosition;
		return currentPosition;
	}
	
	public long getPreviousWordPosition(){
		return previousWordPosition;
	}
	
	public void setCurrentPosition(long position){
		if(bytesInOneChar == 2 && position % 2 != 0){
			position++;
		}
		currentPosition = previousWordPosition = position;
	}
	
	public boolean isEmpty(){
		if(fileLength <= beginPosition){
			return true;
		}
		return false;
	}
}
