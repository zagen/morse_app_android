/**
 * 
 */
package mu.zz.zagen.morse;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

/**
 * @author zagen
 *
 */
public class TransmitterActionActivity extends Activity implements View.OnClickListener{
	static public final String RESULT = "result";

	private Button keyboardButton, touchPadButton, timeTestButton, smsSendButton;

	private AudioManager am;

	private SharedPreferences preferences;
	static private final int ACTION_TRANSMIT_KEYBOARD = 1002;
	static private final int ACTION_TRANSMIT_TOUCHPAD = 1003;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.transmitt_select_activity_layout);
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		int width = displaymetrics.widthPixels;
		//int height = displaymetrics.heightPixels;
		
		int margin = 10;
		int sideOfButton = (width/2 - margin * 4);

		
		keyboardButton = (Button)findViewById(R.id.tsa_keyboard_select);
		touchPadButton = (Button)findViewById(R.id.tsa_touchpad_select);
		timeTestButton = (Button)findViewById(R.id.tsa_timetest_select);
		smsSendButton  = (Button)findViewById(R.id.tsa_send_sms);
		
		RelativeLayout.LayoutParams layoutParams = (LayoutParams) keyboardButton.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		keyboardButton.setLayoutParams(layoutParams);
		
		layoutParams = (LayoutParams) touchPadButton.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		touchPadButton.setLayoutParams(layoutParams);
		
		layoutParams = (LayoutParams) timeTestButton.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		timeTestButton.setLayoutParams(layoutParams);
		
		layoutParams = (LayoutParams) smsSendButton.getLayoutParams();
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		smsSendButton.setLayoutParams(layoutParams);
		
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tsa_keyboard_select:
			Intent transmitKeyboardActivityIntent = new Intent();
			transmitKeyboardActivityIntent.setClass(this, SendingCheckActivity.class);
			startActivityForResult(transmitKeyboardActivityIntent, ACTION_TRANSMIT_KEYBOARD);
			break;

		case R.id.tsa_touchpad_select:
			Intent transmitTouchPadActivityIntent = new Intent();
			transmitTouchPadActivityIntent.setClass(this, SendingCheckTouchPadActivity.class);
			startActivityForResult(transmitTouchPadActivityIntent, ACTION_TRANSMIT_TOUCHPAD);
			break;

		case R.id.tsa_timetest_select:
			Intent transmitTimeTestActivityIntent = new Intent();
			transmitTimeTestActivityIntent.setClass(this, SendingCheckTimeTestActivity.class);
			startActivity(transmitTimeTestActivityIntent);
			break;
		case R.id.tsa_send_sms:
			Intent smsSendActivityIntent = new Intent();
			smsSendActivityIntent.setClass(this, FreepadActivity.class);
			startActivity(smsSendActivityIntent);
			break;
		default:
			break;
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case ACTION_TRANSMIT_KEYBOARD:
			
		case ACTION_TRANSMIT_TOUCHPAD:
			if(resultCode == RESULT_OK){
				String jsonstring = data.getStringExtra(RESULT);
				if(!jsonstring.equals("[]")){
					Intent resultIntent = new Intent();
					resultIntent.setClass(getApplicationContext(), ResultActivity.class);
					resultIntent.putExtra(RESULT, jsonstring);
					startActivity(resultIntent);
					
				}
			}
			break;
		default:
				
		}
		finish();
	}

	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPause();
	}
}
