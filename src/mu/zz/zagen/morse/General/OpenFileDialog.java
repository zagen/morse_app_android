package mu.zz.zagen.morse.General;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Scogun
 * Edited: zagen
 * Date: 27.11.13
 * Time: 10:47
 */
public class OpenFileDialog extends AlertDialog.Builder {

	/**
	 * need for compatability with api below v11
	 */
	private Context context;
	
	/**
	 * here store path of current directory
	 */
    private String currentPath = Environment.getExternalStorageDirectory().getPath();
    
    /**
     * list of files of currentPath
     */
    private List<File> files = new ArrayList<File>();
    
    /**
     * title textView of Dialog. Contain currentPath string value
     */
    private TextView title;
    
    /**
     * list of files/dirs of current path
     */
    private ListView listView;
    
    /**
     * filter of filename. Txt files only for our purpose
     */
    private FilenameFilter filenameFilter;
    
    /**
     * index of selected list item
     */
    private int selectedIndex = -1;
    
    /**
     * called then dialog done successfully
     */
    private OpenDialogListener listener;

    /**
     * message, show when user try open directories for which he hasn't access
     */
    private String accessDeniedMessage = "";

    public interface OpenDialogListener {
        public void OnSelectedFile(String fileName);
    }

    
    /**
     * custom adapter for list view. Contains files in currentPath
     * @author zagen
     *
     */
    private class FileAdapter extends ArrayAdapter<File> {

        public FileAdapter(Context context, List<File> files) {
            super(context, android.R.layout.simple_list_item_1, files);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            File file = getItem(position);
            if (view != null) {
                view.setText(file.getName());
                view.setTextColor(Color.BLACK);
                if (!file.isDirectory()) {
                    if (selectedIndex == position)
                        view.setBackgroundColor(Color.LTGRAY);
                    else
                        view.setBackgroundColor(Color.WHITE);
                }else{
                	view.setBackgroundColor(Color.WHITE);
                }
            }
            return view;
        }


    }

    public OpenFileDialog(Context context) {
        super(context);
        this.context = context;
        title = createTitle(context);
        changeTitle();
        LinearLayout linearLayout = createMainLayout(context);
        listView = createListView(context);
        linearLayout.addView(listView);
        setCustomTitle(title)
                .setView(linearLayout)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (selectedIndex > -1 && listener != null) {
                            listener.OnSelectedFile(listView.getItemAtPosition(selectedIndex).toString());
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
    }

    @Override
    public AlertDialog show() {
        files.addAll(getFiles(currentPath));
        listView.setAdapter(new FileAdapter(context, files));
        return super.show();
    }

    /**
     * set filter for showing files(regular expression)
     * @param filter
     * @return
     */
    public OpenFileDialog setFilter(final String filter) {
        filenameFilter = new FilenameFilter() {

            @Override
            public boolean accept(File file, String fileName) {
                File tempFile = new File(String.format("%s/%s", file.getPath(), fileName));
                
                try {
                	if(tempFile.isFile())
                        return tempFile.getName().matches(filter);
				} catch (Exception e) {
					return false;
				}
                return true;
            }
        };
        return this;
    }

    public OpenFileDialog setOpenDialogListener(OpenDialogListener listener) {
        this.listener = listener;
        return this;
    }

 
    public OpenFileDialog setAccessDeniedMessage(String message) {
        this.accessDeniedMessage = message;
        return this;
    }

    private static Display getDefaultDisplay(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }

    private static Point getScreenSize(Context context) {
        Point screenSize = new Point();
        DisplayMetrics dm = new DisplayMetrics();
        
        getDefaultDisplay(context).getMetrics(dm);
        screenSize.x = dm.widthPixels;
        screenSize.y = dm.heightPixels;
        return screenSize;
    }

    private static int getLinearLayoutMinHeight(Context context) {
        return getScreenSize(context).y;
    }

    private LinearLayout createMainLayout(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setBackgroundColor(Color.WHITE);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setMinimumHeight(getLinearLayoutMinHeight(context));
        return linearLayout;
    }

    private int getItemHeight(Context context) {
        TypedValue value = new TypedValue();
        DisplayMetrics metrics = new DisplayMetrics();
        context.getTheme().resolveAttribute(android.R.attr.listPreferredItemHeight, value, true);
        getDefaultDisplay(context).getMetrics(metrics);
        return (int) TypedValue.complexToDimension(value.data, metrics);
    }

    private TextView createTextView(Context context) {
        TextView textView = new TextView(context);
        textView.setTextColor(Color.BLACK);
        textView.setBackgroundColor(Color.WHITE);
        int itemHeight = getItemHeight(context);
        textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, itemHeight));
        textView.setMinHeight(itemHeight);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(15, 0, 0, 0);
        return textView;
    }

    private TextView createTitle(Context context) {
        TextView textView = createTextView(context);
        return textView;
    }

    public int getTextWidth(String text, Paint paint) {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.left + bounds.width() + 80;
    }

    /**
     * change title of Title textView. set like name of currentPath. if it need cut it off 
     */
    private void changeTitle() {
        String titleText = currentPath;
        int screenWidth = getScreenSize(context).x;
        int maxWidth = (int) (screenWidth * 0.99);
        if (getTextWidth(titleText, title.getPaint()) > maxWidth) {
            while (getTextWidth("..." + titleText, title.getPaint()) > maxWidth) {
                int start = titleText.indexOf("/", 2);
                if (start > 0)
                    titleText = titleText.substring(start);
                else
                    titleText = titleText.substring(2);
            }
            title.setText("..." + titleText);
        } else {
            title.setText(titleText);
        }
    }

    /**
     * get list of all file in directoryPath + ".."
     * @param directoryPath
     * @return
     */
    private List<File> getFiles(String directoryPath) {
        File directory = new File(directoryPath);
        List<File>fileList =  new ArrayList<File>();
        try {
	        fileList.add(new File(".."));
	        fileList.addAll(Arrays.asList(directory.listFiles(filenameFilter)));
	        Collections.sort(fileList, new Comparator<File>() {
	            @Override
	            public int compare(File file, File file2) {
	                if (file.isDirectory() && file2.isFile())
	                    return -1;
	                else if (file.isFile() && file2.isDirectory())
	                    return 1;
	                else
	                    return file.getPath().compareTo(file2.getPath());
	            }
	        });
        } catch (Exception e) {
        	String message = context.getResources().getString(android.R.string.unknownName);
            if (!accessDeniedMessage.equals(""))
                message = accessDeniedMessage;
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		}
        return fileList;
    }

    /**
     * refill list of file for currentPath
     * @param adapter
     */
    private void RebuildFiles(ArrayAdapter<File> adapter) {
        try {
            List<File> fileList = getFiles(currentPath);
            files.clear();
            selectedIndex = -1;
            files.addAll(fileList);
            adapter.notifyDataSetChanged();
            changeTitle();
        } catch (NullPointerException e) {
            String message = context.getResources().getString(android.R.string.unknownName);
            if (!accessDeniedMessage.equals(""))
                message = accessDeniedMessage;
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    private ListView createListView(Context context) {
        ListView listView = new ListView(context);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                final ArrayAdapter<File> adapter = (FileAdapter) adapterView.getAdapter();
                File file = adapter.getItem(index);
                if(file.toString().equals("..")){
                	file = new File(currentPath).getParentFile();
                    if (file != null) {
                        currentPath = file.getPath();
                        RebuildFiles(adapter);
                    }
                }else
                if (file.isDirectory()) {
                    currentPath = file.getPath();
                    RebuildFiles(adapter);
                } else {
                    if (index != selectedIndex)
                        selectedIndex = index;
                    else
                        selectedIndex = -1;
                    adapter.notifyDataSetChanged();
                }
            }
        });
        listView.setBackgroundColor(Color.WHITE);
        return listView;
    }
}
