/**
 * 
 */
package mu.zz.zagen.morse.General;

import java.util.regex.Pattern;

import mu.zz.zagen.morse.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * @author zagen
 *
 */
public class PercentChooseDialogBuilder extends AlertDialog.Builder {

	public interface OnFinishDialogListener{
		void onFinish(double percent);
	}
	
	
	private OnFinishDialogListener onFinishListener;
	private EditText editText = null;
	private InputFilter percentInputFilter = new InputFilter() {
		
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
								   Spanned dest, int dstart, int dend) {
			 String checkedText = dest.toString() + source.toString();
			 String pattern = getPattern();
			   
			 if(!Pattern.matches(pattern, checkedText)) {
			      return "";
			 }
			   
			 return null;
		}
		private String getPattern() {
			//DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			String ds = ".";//String.valueOf(dfs.getDecimalSeparator());
			String pattern = "[0-9]{0,2}+([" + ds + "]{1}||[" + ds + "]{1}[0-9]{1,2})?";  
			return pattern;
		}
	};
	
	public PercentChooseDialogBuilder(Context context) {
		super(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = inflater.inflate(R.layout.percent_choose_dialog_layout, null);
		setView(view);
		editText = (EditText)view.findViewById(R.id.pcd_edittext); 
		editText.setFilters(new InputFilter[]{percentInputFilter});
		setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(onFinishListener != null){
					onFinishListener.onFinish(Double.parseDouble(editText.getText().toString()));
				}
			}
		});
		setNegativeButton(R.string.cancel, null);
	}

/*
	public double getPercent(){
		return Double.parseDouble(editText.getText().toString());
	}
*/


	/**
	 * @param onFinishListener the onFinishListener to set
	 */
	public PercentChooseDialogBuilder setOnFinishListener(OnFinishDialogListener onFinishListener) {
		this.onFinishListener = onFinishListener;
		return this;
	}
}
