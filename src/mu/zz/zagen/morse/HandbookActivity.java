
package mu.zz.zagen.morse;

import java.util.HashMap;

import mu.zz.zagen.morse.General.StackMorseView;
import mu.zz.zagen.morse.General.TextCodec;
import mu.zz.zagen.morse.Transmitters.SoundTransmitter;
import mu.zz.zagen.morse.Transmitters.ToneTransmitter;
import mu.zz.zagen.morse.Transmitters.Transmitter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author zagen
 *
 */
public class HandbookActivity extends Activity implements View.OnClickListener{

	/**
	 * codec for translating character into Morse code
	 */
	private TextCodec codec = new TextCodec();
	
	/**
	 * transmitter for playing current character
	 */
	private Transmitter transmitter;
	
	/**
	 * for list presentation of handbook
	 */
	private ListView listView;
	
	/**
	 * true if playing Morse code of any character
	 */
	private Boolean isPlaying = false; 	
	
	/**
	 * latin locale  menu item id 
	 */
	public static final int IDM_LATIN = 101;
	
	/**
	 * russian locale  menu item id 
	 */
	public static final int IDM_CYRILLIC = 102;
	
	/**
	 * name of current letter. Ex.: 'C' - Charlie
	 */
	private TextView letterNameTxt = null;
	
	/**
	 *   Say that below lay mnemonic phrase
	 */
	private TextView mnemonicTitleTxt = null;
	
	/**
	 *  Show current letter on the screen
	 */
	private Button	letterValueBtn = null;
	
	/**
	 * label for phrase for better remembering Morse code of character
	 */
	private Button	mnemValueBtn = null;
	
	/**
	 * show Morse code of current symbol
	 */
	private StackMorseView stackView = null;
	
	/**
	 * current state of presentation handbook. On load it's list
	 */
	private State currentState = State.LIST;
	
	/**
	 * current symbol value
	 */
	private String currentValue = "";
	
	/**
	 * animations for listing symbols
	 */
	private Animation right_in  ;
	private Animation left_in ;
	private Animation left_out;
	
	/**
	 * object which contain detailed information for symbol
	 */
	private LetterDetailedInfo letterInfo;
	
	/**
	 * current locale
	 */
	private int locale;
	
	/**
	 * available states of presentation handbook
	 * @author zagen
	 *
	 */
	public enum State{
		LIST,
		DETAILED
	}
	
	
	/**
	 * count of all elements in the list
	 */
	private int countElements;
	
	/**
	 * position in list of current character
	 */
	private int currentPosition;
	
	/**
	 * Change state of handbooks presentation 
	 * @param state
	 */
	public void setState(State state){
		currentState = state;
		if(state == State.LIST){
			//if it list, hide all peaces of information for current symbol
			letterNameTxt.setVisibility(View.INVISIBLE);
			mnemonicTitleTxt.setVisibility(View.INVISIBLE);
			letterValueBtn.setVisibility(View.INVISIBLE);
			mnemValueBtn.setVisibility(View.INVISIBLE);
			stackView.setVisibility(View.INVISIBLE);
			
			//and show list
			listView.setVisibility(View.VISIBLE);
			
		}else{
			//inverting state of all views
			listView.setVisibility(View.INVISIBLE);
			letterNameTxt.setVisibility(View.VISIBLE);
			mnemonicTitleTxt.setVisibility(View.VISIBLE);
			letterValueBtn.setVisibility(View.VISIBLE);
			mnemValueBtn.setVisibility(View.VISIBLE);
			stackView.setVisibility(View.VISIBLE);
			
			//set current text for showing it
			letterValueBtn.setText(currentValue);
			//set it in codec
			codec.setText(currentValue);
			//clear and set new Morse code in morse code view 
			stackView.clear();
			stackView.push(codec.getEncodedText());
			//extract letter name and mnemonic value for current character and show it 
			letterNameTxt.setText(letterInfo.name(currentValue));
			mnemValueBtn.setText(letterInfo.mnemonic(currentValue));
			
			//animation in
			letterNameTxt.startAnimation(right_in);
			mnemonicTitleTxt.startAnimation(right_in);
			letterValueBtn.startAnimation(right_in);
			mnemValueBtn.startAnimation(right_in);
			stackView.startAnimation(right_in);
			
			//show up hint how to play Morse code of symbol
			Toast.makeText(this, R.string.handbook_greetings, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * coords for recognizing type of gesture
	 */
	private float x1 = -1;
	private float x2 = -1;
	private float y1 = -1;
	private float y2 = -1;
	
	private AudioManager am;
	private SharedPreferences preferences;

	@Override
	public void onCreate(Bundle savedInstanceState){
		//no title fullscreen 
		super.onCreate(savedInstanceState);
		setTitle(R.string.handbook);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.handbook_layout);
		
		
		letterNameTxt = (TextView)findViewById(R.id.handbook_lettername_txt);
		mnemonicTitleTxt = (TextView)findViewById(R.id.handbook_mnemo_title);
		letterValueBtn = (Button)findViewById(R.id.handbook_letter_value);
		mnemValueBtn = (Button)findViewById(R.id.handbook_mnemo_btn);
		stackView = (StackMorseView)findViewById(R.id.handbook_Letter_stack_view);
		
		listView = (ListView)findViewById(R.id.handbook_listview);
		listView.setAdapter(new HandbookAdapter(getApplicationContext()));
		
		right_in = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_in_right);
		left_in = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
		left_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
		
		//set flag that playing stop when it really stop playing)
		Transmitter.OnTransmittanceEndedListener listener = new Transmitter.OnTransmittanceEndedListener() {
			
			@Override
			public void end() {
				
				isPlaying = false;
			}
		};
		
		//here reading settings;
		preferences = getApplicationContext().getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		locale = preferences.getInt(SettingsActivity.LOCALE, 0);
		codec.setLocale(locale);
		
		letterInfo = new LetterDetailedInfo(locale); 
		
		//if set sound transmitter in setting that create it
		if(preferences.getInt(SettingsActivity.TRANSMITTER, 0) == 1)
			transmitter = new SoundTransmitter(getApplicationContext(), codec, listener);
		else //otherwise its tone transmitter
			transmitter = new ToneTransmitter(getApplicationContext(), codec, listener);
		countElements = listView.getAdapter().getCount();
		
		//on select item of list change state of presentation in DETAILED and set current position 
		listView.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View view,
	                int position, long id) {
				currentValue = ((TextView) view.findViewById(R.id.handbook_character)).getText().toString();
				setState(State.DETAILED);
				currentPosition = position;
	           }
	       });
		
		letterValueBtn.setOnClickListener(this);

		//on load state is LIST
		setState(State.LIST);
		
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);

	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//for detecting finger movement direction
		float  dx, dy;
		
		
		switch(event.getAction()) {
		
		    case(MotionEvent.ACTION_DOWN):
		        x1 = event.getX();
		        y1 = event.getY();
		        break;
		    case(MotionEvent.ACTION_UP):{
		    
		    if(x1 != -1 && y1 != -1)	
			{
		    	x2 = event.getX();
				y2 = event.getY();
				dx = x2 - x1;
				dy = y2 - y1;
			    // Use dx and dy to determine the direction
				if(Math.abs(dx) > Math.abs(dy)) {
					if(dx > 0){
						currentPosition --;
						if(currentPosition < 0)
							currentPosition += countElements ;
					
						letterNameTxt.startAnimation(left_in);
						mnemonicTitleTxt.startAnimation(left_in);
						letterValueBtn.startAnimation(left_in);
						mnemValueBtn.startAnimation(left_in);
						stackView.startAnimation(left_in);
					}else{
						currentPosition++;
						if(currentPosition >= countElements){
							currentPosition %= countElements;
						}
						
						letterNameTxt.startAnimation(right_in);
						mnemonicTitleTxt.startAnimation(right_in);
						letterValueBtn.startAnimation(right_in);
						mnemValueBtn.startAnimation(right_in);
						stackView.startAnimation(right_in);
					}
					currentValue = (String)listView.getAdapter().getItem(currentPosition);
					letterValueBtn.setText(currentValue);
					codec.setText(currentValue);
					stackView.clear();
					stackView.push(codec.getEncodedText());
					letterNameTxt.setText(letterInfo.name(currentValue));
					mnemValueBtn.setText(letterInfo.mnemonic(currentValue));
				}
				
			}
		}//end case UP
	    
		}//end switch
		return super.onTouchEvent(event);
	}

	/**
	 * override action on pressing back button 
	 */
	@Override
	public void onBackPressed() {
		if(currentState == State.DETAILED){
			//if state non list, set back to list otherwise close activity
			letterNameTxt.startAnimation(left_out);
			mnemonicTitleTxt.startAnimation(left_out);
			letterValueBtn.startAnimation(left_out);
			mnemValueBtn.startAnimation(left_out);
			stackView.startAnimation(left_out);
			setState(State.LIST);
			currentValue = "";
			listView.setSelection(currentPosition);
		}
		else
			super.onBackPressed();
	}

	/**
	 * Class that provide detailed information about character(like mnemonic, name and mnemonic image(feature))
	 * @author zagen
	 *
	 */
	public class LetterDetailedInfo{
		/**
		 * current locale
		 */
		private int locale; 
		
		/**
		 * list of mnemonics for all letter
		 */
		private final HashMap<String, String> mnemonicsLet = new HashMap<String, String>();
		
		/**
		 * list of symbols mnemonic specific for russian locale
		 */
		private final HashMap<String, String> mnemonicsSymRus = new HashMap<String, String>();
		
		/**
		 * list of symbols mnemonic specific for latin locale
		 */
		private final HashMap<String, String> mnemonicsSymInt = new HashMap<String, String>();
		
		/**
		 * names for characters 
		 */
		private final HashMap<String, String> names = new HashMap<String, String>();
		
		/**
		 * mnemonic images for characters
		 */
		private final HashMap<String, Integer> images = new HashMap<String, Integer>();
		
		/**
		 * @param character
		 * @return true if character has a name
		 */
		public boolean hasName(String character){
			return names.containsKey(character);			
		}
		
		/** 
		 * @param character
		 * @return name of character
		 */
		public String name(String character){
			if(hasName(character))
				return names.get(character);
			else
				return "";				
		}
		
		/**
		 * 
		 * @param character
		 * @return true if character has mnemonic image
		 */
		public boolean hasImage(String character){
			return images.containsKey(character);			
		}
		
		/**
		 * 
		 * @param character
		 * @return mnemonic image if it exist
		 */
		public int image(String character){
			return images.get(character);
		}
		
		/**
		 * get mnemonic
		 * @param character
		 * @return mnemonic phrase for character
		 */
		public String mnemonic(String character){
			if(locale == TextCodec.RUS){
				if(mnemonicsLet.containsKey(character)){
					return mnemonicsLet.get(character);
				}else{
					return mnemonicsSymRus.get(character);
				}
			}else{
				if(mnemonicsLet.containsKey(character)){
					return mnemonicsLet.get(character);
				}else{
					return mnemonicsSymInt.get(character);
				}
			}
		}
		/**
		 * public constructor
		 * @param locale
		 */
		public LetterDetailedInfo(int locale){
			this.locale = locale;
			mnemonicsLet.put("�", "��-���");//, ��-���");
			mnemonicsLet.put("�", "���-��-��-���");//, ����-��-��-���");
			mnemonicsLet.put("�", "��-���-���");//, ���-���-���");
			mnemonicsLet.put("�", "���-���-�");//, ���-���-���");
			mnemonicsLet.put("�", "���-��-��");
			mnemonicsLet.put("�", "����");
			mnemonicsLet.put("�", "����");
			mnemonicsLet.put("�", "�-���-��-���");//,��-��-���-���, ��-��-��-����,  ��-��-��-���");
			mnemonicsLet.put("�", "���-���-��-��");//, ���-���-��-��, ���-���-��-��");
			mnemonicsLet.put("�", "�-��");
			mnemonicsLet.put("�", "�-�����-���-��");//, ���-���-���-���, ���-���-���-���");
			mnemonicsLet.put("�", "����-��-���");//, ����-��-����, ���-����-���");
			mnemonicsLet.put("�", "��-���-��-��");
			mnemonicsLet.put("�", "���-���");//, ����-���");
			mnemonicsLet.put("�", "���-���");//, ���-��");
			mnemonicsLet.put("�", "��-���-���");
			mnemonicsLet.put("�", "��-���-���-��");//, ��-���-���-��");
			mnemonicsLet.put("�", "��-���-��");//, ��-���-��");
			mnemonicsLet.put("�", "��-��-��");//, ��-��-�, ��-��-�");
			mnemonicsLet.put("�", "����");
			mnemonicsLet.put("�", "�-���-���");//, �-��-���");
			mnemonicsLet.put("�", "��-��-����-���");
			mnemonicsLet.put("�", "��-��-��-��");
			mnemonicsLet.put("�", "���-���-���-��");//, ���-���-���-���, ���-���-���-���, ���-��-���-��, ���-���-���-���");
			mnemonicsLet.put("�", "���-���-���-���");//, ���-���-���-���");
			mnemonicsLet.put("�", "���-���-���-���");//, ���-���-���-���");
			mnemonicsLet.put("�", "���-���-��-���");//, ���-����-��-���");
			mnemonicsLet.put("�", "��-���-���-����-�����");//, �⸸�-����-��-����-����");
			mnemonicsLet.put("�", "��-��-���-���");
			mnemonicsLet.put("�", "���-���-���-�����");
			mnemonicsLet.put("�", "�-��-�����-��-��");//,�-��-���-��-��");
			mnemonicsLet.put("�", "�-��-��-���");
			mnemonicsLet.put("�", "�-����-�-����");
			
			mnemonicsLet.put("A", "al-FA");
			mnemonicsLet.put("B", "YEAH! *clap* *clap* *clap*");
			mnemonicsLet.put("C", "CHAR-lie's AN-gels");
			mnemonicsLet.put("D", "NEW or-leans");
			mnemonicsLet.put("E", "hey!");
			mnemonicsLet.put("F", "*step**step**BRUSH**step*");
			mnemonicsLet.put("G", "HOLE IN one!");
			mnemonicsLet.put("H", "ho-li-day-inn");
			mnemonicsLet.put("I", "bom-bay");
			mnemonicsLet.put("J", "where-FORE ART THOU?");
			mnemonicsLet.put("K", "POUND for POUND");
			mnemonicsLet.put("L", "li-MA pe-ru");
			mnemonicsLet.put("M", "LIKE MIKE");
			mnemonicsLet.put("N", "AU-tumn");
			mnemonicsLet.put("O", "SUN-NY-DAY");
			mnemonicsLet.put("P", "of DOC-TOR good");
			mnemonicsLet.put("Q", "A-LOU-et-TUH");
			mnemonicsLet.put("R", "ro-MER-o");
			mnemonicsLet.put("S", "ne-va-da");
			mnemonicsLet.put("T", "*DIP*");
			mnemonicsLet.put("U", "u-ni-FORM");
			mnemonicsLet.put("V", "the first 4 notes of Beethoven's 5th");
			mnemonicsLet.put("W", "jack AND COKE");
			mnemonicsLet.put("X", "ON-ly the BONES");
			mnemonicsLet.put("Y", "ON a PO-NY");
			mnemonicsLet.put("Z", "SHA-KA zu-lu");
			
			names.put("A", "Alpha");
			names.put("B", "Bravo");
			names.put("C", "Charlie");
			names.put("D", "Delta");
			names.put("E", "Echo");
			names.put("F", "Foxtrot");
			names.put("G", "Golf");
			names.put("H", "Hotel");
			names.put("I", "India");
			names.put("J", "Juliet");
			names.put("K", "Kilo");
			names.put("L", "Lima");
			names.put("M", "Mike");
			names.put("N", "November");
			names.put("O", "Oscar");
			names.put("P", "Papa");
			names.put("Q", "Quebec");
			names.put("R", "Romeo");
			names.put("S", "Sierra");
			names.put("T", "Tango");
			names.put("U", "Uniform");
			names.put("V", "Victor");
			names.put("W", "Whiskey");
			names.put("X", "X-ray");
			names.put("Y", "Yankee");
			names.put("Z", "Zulu");
			
			mnemonicsSymRus.put("1", "�-�����-���-��-����");
			mnemonicsSymRus.put("2", "���-��-���-���-���");//, �-��-����-���-����");
			mnemonicsSymRus.put("3", "���-��-��-���-���");//, �-���-���-���-���, ��-��-��-���-����");
			mnemonicsSymRus.put("4", "��-���-��-��-���");
			mnemonicsSymRus.put("5", "��-��-��-��-�");//, ��-��-��-��-���");
			mnemonicsSymRus.put("6", "���-���-��-��-��");//, ������-��-��-��-��");
			mnemonicsSymRus.put("7", "����-����-��-��-����");//, �����-�����-��-��-��, ���-���-��-��-��, ����-����-��-��-���, ���-����-��-��-���");
			mnemonicsSymRus.put("8", "���-�����-���-�-��");//, ���-���-���-��-���");
			mnemonicsSymRus.put("9", "���-���-���-���-��");//, ���-���-���-���-���, ���-���-����-����-���");
			mnemonicsSymRus.put("0", "�����-���-��-���-���");//, ���-����-�����-����-�����");
			mnemonicsSymRus.put(".", "��-���-��-��-���-��");
			mnemonicsSymRus.put(",", "���-����-���-����-���-����");
			mnemonicsSymRus.put(":", "����-��-���-��-�-�����");
			mnemonicsSymRus.put(";", "���-���-���-��-���-�");
			mnemonicsSymRus.put("(", "����-���-������-����-���-������");//, ����-���-���-����-��-���");
			mnemonicsSymRus.put(")", "����-���-������-����-���-������");//, ����-���-���-����-��-���");
			mnemonicsSymRus.put("'", "���-����-���-�����-����-�����");
			mnemonicsSymRus.put("\"", "��-���-���-��-���-���");//, ��-���-���-��-����-����");
			mnemonicsSymRus.put("-", "����-���-��-���-��-����");//, ����-���-��-��-��-���");
			mnemonicsSymRus.put("/", "������-�����-����-������-��,");// ���-��-��-���-���");
			mnemonicsSymRus.put("?", "��-��-���-����-���-��");//, ��-���-���-���-�-��, �-���-���-���-��-��, �-��-���-����-��-��");
			mnemonicsSymRus.put("!", "��-���-���-���-���-��� ");
			mnemonicsSymRus.put("@", "��-���-���-��-���-��");//, ��-���-���-��-���-��");
			
			
			mnemonicsSymInt.put("1", "i-WON-WON-WON-WON");
			mnemonicsSymInt.put("2", "cut-it-SAW-SAW-SAW");
			mnemonicsSymInt.put("3", "make-it-a-THREE-SOME");
			mnemonicsSymInt.put("4", "did-he-hol-ler-FOUR?");
			mnemonicsSymInt.put("5", "sit-a-hal-i-but");
			mnemonicsSymInt.put("6", "SIX-hav-ing-a-fit");
			mnemonicsSymInt.put("7", "SEV-EN-bet-on-it");
			mnemonicsSymInt.put("8", "MOM-IN-LAW-ate-it");
			mnemonicsSymInt.put("9", "NINE-NINE-NINE-NINE-zip");
			mnemonicsSymInt.put("0", "NUL-NUL-NUL-NUL-NUL");
			mnemonicsSymInt.put(".", "a STOP a STOP a STOP");
			mnemonicsSymInt.put(",", "COM-MA, it's a COM-MA");
			mnemonicsSymInt.put(":", "HA-WA-II stan-dard time");
			mnemonicsSymInt.put(";", "A-list, B-list, C-list");
			mnemonicsSymInt.put("(", "AU-tumn HOLE-IN-one!");
			mnemonicsSymInt.put(")", "AU-tumn A-LOU-et-TUH");
			mnemonicsSymInt.put("'", "and THIS STUFF GOES TO me!");
			mnemonicsSymInt.put("\"", "six-TY-six nine-TY-nine");
			mnemonicsSymInt.put("-", "*DIP* ho-li-day-inn *DIP*");
			mnemonicsSymInt.put("/", "SHAVE and a HAIR-cut");
			mnemonicsSymInt.put("?", "it's a QUES-TION, is it?");
			mnemonicsSymInt.put("!", "AU-tumn ON a PO-NY");
			mnemonicsSymInt.put("@", "al-FA CHAR-lie's AN-gels");
		}
	}

	@Override
	public void onClick(View v) {
		//check if its already playing
		if(!isPlaying){
            String symbol = (String) letterValueBtn.getText();

            Toast.makeText(getApplicationContext(), getString(R.string.playing_code) + symbol, Toast.LENGTH_SHORT).show();
            codec.setText(symbol);
            isPlaying = true;
            transmitter.transmit();
		}
		
	}
	
	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPause();
	}
}