/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.Generator;
import mu.zz.zagen.morse.General.StackMorseView;
import mu.zz.zagen.morse.General.TextCodec;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

/**
 * @author zagen
 *
 */
public class SendingCheckTouchPadActivity extends Activity implements View.OnClickListener{

	/**
	 * View for display Morse code
	 */
	private StackMorseView 					stackView;
	
	/**
	 * Generator of random sequences or words
	 */
	private Generator 						generator ;
	
	/**
	 * alphabet for current locale
	 */
	private static String 					abc;
	
	/**
	 * selected symbols for training
	 */
	private String 							currentSymbols;
	
	/**
	 * for view current sequence(word)
	 */
	private Button 							generalViewButton;
	
	/**
	 * for translate Morse code -> characters and via verse
	 */
	private TextCodec 						codec = new TextCodec();
	
	/**
	 * store current sequence of Morse code before translating in character
	 */
	private String 							inputText = "";
	
	/**
	 * width of activity
	 */
	private int 							width;
	
	/**
	 * height of activity
	 */
	private int 							height;
	
	/**
	 * show if answer is incorrect 
	 */
	private TextView 						statusAnswerTextView;
	
	/**
	 * storage of preferences for app
	 */
	private SharedPreferences 				preferences;
	
	/**
	 * current locale
	 */
	private int	    						locale = 0;
	
	/**
	 * type of sequence (0 - random sequence , 1 - words); 
	 */
	private int								type = 0;
	
	/**
	 * touchpad for input
	 */
	private TouchPad						touchPad = null;
	
	/**
	 * JSON Array for result. Contain inputed text and correct text
	 */
	private JSONArray 						resultJSONArray = new JSONArray();
	/*
	 * maybe memory leak?
	 */
	@SuppressLint("HandlerLeak")
	private final Handler 					handler = new Handler(){
		
		/**
		 * handle message came from touchpad
		 */
		@Override
		public void handleMessage (Message msg){
			Bundle data = msg.getData();
			char character = data.getChar(TouchPad.MESSAGE_DATA_CHARACTER);
			if(character == TouchPad.LONG_DELAY_SYMBOL)
				return;
			//here checking if our input is a right answer?
			if(character == ' '){ //space tells us that inputing Morse code of current character is ended
				
				String[] inputedChars = inputText.split(" "); // here inputed morse codes  
				
				//length of source character have to be equal length inputed ones
				if(inputedChars.length == generalViewButton.getText().length()){
					String[] sourceChars = codec.getEncodedText().split(" ");//here right sequence of morse codes
					stackView.clear();
					SpannableStringBuilder spanString = new SpannableStringBuilder(generalViewButton.getText()); //need to change of color character where been made mistake
					Boolean madeMistake = false; 
					//check for each character
					for(int i = 0; i < sourceChars.length; i++)
					{
						if(!sourceChars[i].equals(inputedChars[i]))
						{
							madeMistake = true;
							spanString.setSpan(new ForegroundColorSpan(Color.GRAY), i, i+1, 0);
						}
					}
					if(!madeMistake)
					{
						JSONObject inputResult = new JSONObject();
						try {
							String textGenerated = generalViewButton.getText().toString().replace('�', '�');
							inputResult.put(MainActivity.TEST_VALUE, textGenerated);
							
							String currentTxt = codec.getDecodedText();
							codec.setMorse(inputText);
							inputResult.put(MainActivity.INPUT_VALUE, codec.getDecodedText().toUpperCase());
							codec.setText(currentTxt);
							resultJSONArray.put(inputResult);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
						Toast.makeText(getApplicationContext(), R.string.answer_is_correct, Toast.LENGTH_SHORT).show();
						refill();
						return;
					}
					statusAnswerTextView.setVisibility(View.VISIBLE);
					Toast.makeText(getApplicationContext(), R.string.wrong_answer, Toast.LENGTH_SHORT).show();
					
					for(char symbol: codec.getEncodedText().toCharArray())
					{
						stackView.push(symbol);
					}
					generalViewButton.setText(spanString, BufferType.SPANNABLE);

					JSONObject inputResult = new JSONObject();
					try {
						String textGenerated = generalViewButton.getText().toString().replace('�', '�');
						inputResult.put(MainActivity.TEST_VALUE, textGenerated);
						String currentTxt = codec.getDecodedText();
						codec.setMorse(inputText);
						inputResult.put(MainActivity.INPUT_VALUE, codec.getDecodedText().toUpperCase());
						codec.setText(currentTxt);
						resultJSONArray.put(inputResult);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					if(touchPad != null){
						touchPad.setAvailable(false);
					}
					
					
				}else{
					inputText += character;
					stackView.push(character);
				}
				
			}else{

				inputText += character;
				stackView.push(character);
			}
		}


	};
	/**
	 * font for general view 
	 */
	private Typeface typeface;
	
	/**
	 * for changing volume
	 */
	private AudioManager am;
	
	
	
	
	//for fitting text into button;
	private void correctWidth(Button button, int desiredWidth)
	{
		Paint paint = new Paint();
	    Rect bounds = new Rect();

	    paint.setTypeface(button.getTypeface());
	    float textSize = button.getTextSize() + 20;
	    //button.getTextSize();
	    paint.setTextSize(textSize);
	    String text = button.getText().toString();
	    paint.getTextBounds(text, 0, text.length(), bounds);
	    
	    while (bounds.width() > desiredWidth ||  bounds.height() >= height/7)
	    {
	        textSize--;
	        paint.setTextSize(textSize);
	        paint.getTextBounds(text, 0, text.length(), bounds);
	    }

	    button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
	}
	
	private void refill()
	{
		//generation new character sequence
		//all statuses set to none
		statusAnswerTextView.setVisibility(View.INVISIBLE);
		
		inputText = "";
		stackView.clear();
		
		String txt = generator.toString();
		generalViewButton.setText(txt);
		codec.setText(txt);	
		//encode to morse code

		if(generator.getSequenceType() == Generator.WORDS)
			correctWidth(generalViewButton, width - 100);//correct  width of generating text to fit one line
		if(touchPad != null){
			touchPad.setAvailable(true);
		}
	}
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.sca_touchpad_layout);
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		width = displaymetrics.widthPixels;
		height = displaymetrics.heightPixels;
		
		
		Button refillButton = (Button)findViewById(R.id.refill_button);
		refillButton.setText("\u21BA");
		Button okButton = (Button)findViewById(R.id.sca_ok_send_act_button);
		okButton.setText("\u2714");
		
		stackView = (StackMorseView)findViewById(R.id.stack_view);
		
		generalViewButton = (Button)findViewById(R.id.generator_view_button);
		
		statusAnswerTextView = (TextView)findViewById(R.id.sca_status_answer);
		
		generator = new Generator(this);

		
		preferences =  getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get locale 
		locale = preferences.getInt(SettingsActivity.LOCALE, 0);
		generator.setLocale(locale);
		codec.setLocale(locale);
		
		//get size
		int size = preferences.getInt(SettingsActivity.GEN_LENGTH, 2);
		generator.setSize(size);
		
		if(locale == TextCodec.RUS){
			abc = "��������������������������������1234567890.,:;()\"'-/?!";
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_RUS, "��������������������������������");
		}else
		{
			abc = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!"; 
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_INT, "abcdefghijklmnopqrstuvwxyz");
		}
		generator.setAbc(currentSymbols);
		
		
		type = preferences.getInt(SettingsActivity.SEQUENCE_TYPE, Generator.CHARS);
		generator.setSequenceType(type);
		//int desiredWidth = width - 100;
		
		typeface = Typeface.createFromAsset(getAssets(), "fonts/general.ttf");
		generalViewButton.setTypeface(typeface);
		
		//generalViewButton.setText("����������".substring(0, size));
		//correctWidth(generalViewButton, width);//correct  width of generating text to fit one line

		String txt = generator.toString();
		generalViewButton.setText(txt);
		codec.setText(txt);//encode to morse code
		
		//correctWidth(generalViewButton, width);//correct  width of generating text to fit one line
		touchPad = (TouchPad)findViewById(R.id.freepad_touchpad);
		touchPad.setHandler(handler);
		
		if(generator.getSequenceType() == Generator.CHARS){
			generalViewButton.setText("����������".substring(0, size));
			correctWidth(generalViewButton, width);//correct  width of generating text to fit one line
			generalViewButton.setText(txt);
		}else{
			correctWidth(generalViewButton, width - 100);//correct  width of generating text to fit one line
		}
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.generator_view_button:
			inputText = "";
			statusAnswerTextView.setVisibility(View.INVISIBLE);
			if (generalViewButton.getText() instanceof Spannable)
			{
				Spannable text = (Spannable)generalViewButton.getText();
				ForegroundColorSpan[] spans = text.getSpans(0, text.length(), ForegroundColorSpan.class);
		        for (ForegroundColorSpan span: spans) {
		            text.removeSpan(span);		           
		        }
		        generalViewButton.setText(text);
			}
			
			stackView.clear();
			Toast.makeText(this, getString(R.string.try_again), Toast.LENGTH_SHORT).show();
			if(touchPad != null){
				touchPad.setAvailable(true);
			}
			break;


		case R.id.refill_button: 
			refill();
			break;
		case R.id.sca_ok_send_act_button:
			//here put a result
			Intent intent = new Intent();
			intent.putExtra(MainActivity.RESULT, resultJSONArray.toString());
			setResult(RESULT_OK, intent);
			
			finish();
			break;
		case R.id.sca_handbook_button:
			Intent handbookActivityIntent = new Intent();
			handbookActivityIntent.setClass(this, HandbookActivity.class);
			startActivity(handbookActivityIntent);
			break;
		case R.id.sca_settings_button:
			openOptionsMenu();
			break;
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gen_symbols_menu, menu);
		return true;
	}
	
	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		super.onPause();
	}
	@Override
	protected void onDestroy() {
		touchPad.release();
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_symbols:
			String [] strItems = new String[abc.length()];
			final boolean[] boolItems= new boolean[abc.length()];
			//forming arrays for multiList 
			for(int i = 0; i < abc.length(); i++){
				strItems[i] = String.valueOf(abc.toCharArray()[i]);
				boolItems[i] = (currentSymbols.contains(strItems[i])) ? true : false;
			}
			
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setTitle(R.string.symbol_dialog_title)
				.setMultiChoiceItems(strItems, boolItems, new DialogInterface.OnMultiChoiceClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						boolItems[which] = isChecked;
						
					}
				})
				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreferences.Editor editor = preferences.edit();
						StringBuilder strBuilder = new StringBuilder();
						for(int i = 0; i < abc.length();i++){
							if(boolItems[i]){
								strBuilder.append(abc.toCharArray()[i]);
							}
						}
						if(locale == TextCodec.RUS)
							editor.putString(SettingsActivity.SYMBOLS_RUS, strBuilder.toString());
						else
							editor.putString(SettingsActivity.SYMBOLS_INT, strBuilder.toString());
						currentSymbols = strBuilder.toString();
						generator.setAbc(currentSymbols);
						refill();
						editor.commit();
					}
				})
				.create().show();
			break;
		case R.id.action_message_type:
			
			AlertDialog.Builder adb_type = new AlertDialog.Builder(this);
			adb_type.setSingleChoiceItems(getResources().getTextArray(R.array.message_types), type, new DialogInterface.OnClickListener() {
				
							@Override
							public void onClick(DialogInterface dialog, int which) {
								type = which;					
							}
						})
					.setTitle(R.string.type_dialog_title)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SharedPreferences.Editor editor = preferences.edit();
							editor.putInt(SettingsActivity.SEQUENCE_TYPE, type);
							generator.setSequenceType(type);
							if(generator.getSequenceType() == Generator.CHARS){
								generalViewButton.setText("����������".substring(0, generator.getSize()));
								correctWidth(generalViewButton, width);//correct  width of generating text to fit one line
							}
							refill();
							editor.commit();
						}
					})
					.create().show();
			break;

		default:
			break;
		}
		return true;
	}
}
