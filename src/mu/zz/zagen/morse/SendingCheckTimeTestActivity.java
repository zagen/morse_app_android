/**
 * 
 */
package mu.zz.zagen.morse;

import java.util.List;

import mu.zz.zagen.morse.Achievements.Achievement;
import mu.zz.zagen.morse.Achievements.AchievementDialog;
import mu.zz.zagen.morse.General.Generator;
import mu.zz.zagen.morse.General.StackMorseView;
import mu.zz.zagen.morse.General.TextCodec;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author zagen
 *
 */
public class SendingCheckTimeTestActivity extends Activity implements View.OnClickListener {

	/**
	 * view for displaying Morse code
	 */
	private StackMorseView 					stackView;
	
	/**
	 * generator for random sequences or words
	 */
	private Generator 						generator ;
	
	/**
	 * alphabet for current locale
	 */
	private static String 					abc;
	
	/**
	 * view for displaying current sequence of generated symbols need to input
	 */
	private Button 							generalViewButton;
	
	/**
	 * Codec for translation text into Morse code and otherwise
	 */
	private TextCodec 						codec = new TextCodec();
	
	/**
	 * here stores inputed Morse code
	 */
	private String 							inputText = "";
	
	/**
	 * true if test is in progress
	 */
	private Boolean 						isTestInProgress = false;
	
	/**
	 * count of given symbols to input 
	 */
	private int								totalAnswers = 0;
	
	/**
	 * count of correct inputed symbols
	 */
	private int								correctAnswers = 0;
	
	/**
	 * width of activity
	 */
	private int 							width;
	
	/**
	 * height of activity
	 */
	private int								height;
	
	/**
	 * text view for display number of total symbols and correct answers in test progress
	 */
	private TextView 						testProgressTextView;
	
	/**
	 * timer progress text view
	 */
	private TextView						timerTextView;
	
	/**
	 * for volume control
	 */
	private AudioManager 					am;
	
	/**
	 * preferences storage
	 */
	private SharedPreferences				preferences;
	
	/**
	 * current value of locale
	 */
	private int	    						locale = 0;
	
	/**
	 * TouchPad view
	 */
	private TouchPad						touchPad = null;
	
	/**
	 * value of timer in ms
	 */
	private long 							timerStartTime = 0;
	
	/**
	 * JSON array stores pair inputed answer - correct answer
	 */
	private JSONArray 						resultJSONArray = null;
	
	/**
	 * current given sequence for input
	 */
	private String 							currentString;
	
	/**
	 * size of text general view
	 */
	private float							charSize;	
	/*
	 * maybe memory leak?
	 */
	@SuppressLint("HandlerLeak")
	private final Handler 					handler = new Handler(){
		
		@Override
		public void handleMessage (Message msg){
			Bundle data = msg.getData();
			char character = data.getChar(TouchPad.MESSAGE_DATA_CHARACTER);
			if(character == TouchPad.LONG_DELAY_SYMBOL)
				return; 
			//here checking if our input is a right answer?
			if(character == ' '){
				String sourceCode = codec.getEncodedText();//here right sequence of morse codes
				stackView.clear();
				//check for each character
				JSONObject inputResult = new JSONObject();
				try {
					inputResult.put(MainActivity.TEST_VALUE, currentString.toUpperCase().replace('�', '�'));
					//String currentTxt = codec.getDecodedText();
					codec.setMorse(inputText);
					inputResult.put(MainActivity.INPUT_VALUE, codec.getDecodedText().toUpperCase());
					//codec.setText(currentTxt);
					resultJSONArray.put(inputResult);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				if(sourceCode.equals(inputText))
				{
					correctAnswers++;
				}
				totalAnswers++;
				testProgressTextView.setText(String.format("%d/%d", correctAnswers, totalAnswers));
				if(timerRunning)
					refill();
				else
				{
					//if timer stop set elements unavailable  
					touchPad.setAvailable(false);
					//show status that test is over
	            	testProgressTextView.setText(R.string.test_is_over);
	            	timerTextView.setText(R.string.show_result);
	            	generalViewButton.setText(R.string.restart);
	            	correctWidth(generalViewButton, width-100);
	            	//reset timer
	            	endTimerHandler.sendMessage(endTimerHandler.obtainMessage());
	            	isTestInProgress = false;
	            	//get list of achievements and if its exist show it in a dialog
	    			List<Achievement> achievements = Achievement.getAchievement(getApplicationContext(), resultJSONArray.toString());
	    			if(achievements != null && achievements.size() > 0)
	    			{
	    				new AchievementDialog(SendingCheckTimeTestActivity.this, achievements).init(false).show();
	    			}
				}
			}else{
				//if receiving character isnt space put it into current morse code sequence
				inputText += character;
				stackView.push(character);
			}
		}


	};

	
	/**
	 * for handle timer event
	 */
	private Handler 						timerHandler = new Handler();
	/**
	 * true if timer is running
	 */
	private boolean							timerRunning = false;
	
	/**
	 * handle end of time event
	 */
	@SuppressLint("HandlerLeak")
	private final Handler				 endTimerHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);
		}
		
	};
	private Runnable 						timerRunnable = new Runnable() {

	        @Override
	        public void run() {
	            long millis = System.currentTimeMillis() - timerStartTime;
	            int seconds = (int) (millis / 1000);
	            int minutes = (60 - seconds)/ 60;
	            seconds = (60 - seconds) % 60;

	            if(millis < 61000L){
	            	timerTextView.setText(String.format("%02d:%02d", minutes, seconds));
	            	timerHandler.postDelayed(this, 500);
	            }else{
	            	timerRunning = false;
	            	timerHandler.removeCallbacks(this);
	            }
	        }
	    };
	/**
	 * typeface for displaying need to input text  
	 */
	private Typeface typeface;


		//for fitting text into button;
		private void correctWidth(Button button, int desiredWidth)
		{
			Paint paint = new Paint();
		    Rect bounds = new Rect();

		    paint.setTypeface(button.getTypeface());
		    float textSize = button.getTextSize() + 20;
		    //button.getTextSize();
		    paint.setTextSize(textSize);
		    String text = button.getText().toString();
		    paint.getTextBounds(text, 0, text.length(), bounds);
		    
		    while (bounds.width() > desiredWidth ||  bounds.height() >= height/7)
		    {
		        textSize--;
		        paint.setTextSize(textSize);
		        paint.getTextBounds(text, 0, text.length(), bounds);
		    }

		    button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		}
	private void refill()
	{
		//generation new character sequence
		//all statuses set to none
				
		inputText = "";
		stackView.clear();
		
		currentString = generator.toString();
		generalViewButton.setText(currentString);
		codec.setText(currentString);	
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.sca_for_time_layout);
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		width = displaymetrics.widthPixels;
		height= displaymetrics.heightPixels;
		
	
		stackView = (StackMorseView)findViewById(R.id.stack_view);
		
		generalViewButton = (Button)findViewById(R.id.generator_view_button);
		
		testProgressTextView = (TextView)findViewById(R.id.sca_test_progress_txt);
		
		generator = new Generator(this);

		
		preferences =  getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get locale 
		locale = preferences.getInt(SettingsActivity.LOCALE, 0);
		generator.setLocale(locale);
		codec.setLocale(locale);
		
		
		generator.setSize(1);
		
		if(locale == TextCodec.RUS){
			abc = "��������������������������������1234567890.,:;()\"'-/?!";
			//currentSymbols = settings.getString(SettingsActivity.SYMBOLS_RUS, "��������������������������������");
		}else
		{
			abc = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!"; 
			//currentSymbols = settings.getString(SettingsActivity.SYMBOLS_INT, "abcdefghijklmnopqrstuvwxyz");
		}
		generator.setAbc(abc);
		
		
		generator.setSequenceType(Generator.CHARS);
		int desiredWidth = width - 100;
		String txt = getString(R.string.start).toUpperCase();
		typeface = Typeface.createFromAsset(getAssets(), "fonts/general.ttf");
		generalViewButton.setText(txt);
		generalViewButton.setTypeface(typeface);

		correctWidth(generalViewButton, desiredWidth);//correct  width of generating text to fit one line
		charSize = generalViewButton.getTextSize();
		touchPad = (TouchPad)findViewById(R.id.freepad_touchpad);
		touchPad.setHandler(handler);
		touchPad.setAvailable(false);
		
		timerTextView = (TextView)findViewById(R.id.sca_timer_txt);
		
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.generator_view_button:
			if(!isTestInProgress)
			{
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.are_you_ready)
					   .setMessage(R.string.time_test_dialog_discription)
				       .setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
					
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//start task in timer thread, set touchpad available
								resultJSONArray = new JSONArray();
								isTestInProgress = true;
								touchPad.setAvailable(true);
								timerStartTime = System.currentTimeMillis();
								timerRunning = true;
								refill();
								generalViewButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,charSize);
								testProgressTextView.setVisibility(View.VISIBLE);
								testProgressTextView.setText("");
								timerHandler.postDelayed(timerRunnable,0);
								totalAnswers = 0;
								correctAnswers = 0;
							}
						})
						.setNegativeButton(R.string.cancel, null)
						.show();
				
			}
			break;

		case R.id.sca_timer_txt:
			if(timerTextView.getText().equals(getString(R.string.show_result)))
			{
				
				Intent resultActivityIntent = new Intent();
				resultActivityIntent.setClass(this, ResultActivity.class);
				resultActivityIntent.putExtra(MainActivity.RESULT, resultJSONArray.toString());
				startActivity(resultActivityIntent);
				
			
			}
			break;
		default:
			break;
		}
		
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		return super.onPrepareOptionsMenu(menu);
	}
	@Override
	protected void onDestroy() {
		timerHandler.removeCallbacksAndMessages(null);
		touchPad.release();
		super.onDestroy();
	}

	@Override
	public void onPanelClosed(int featureId, Menu menu) {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPanelClosed(featureId, menu);
	}
}
