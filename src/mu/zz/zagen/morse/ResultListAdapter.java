/**
 * 
 */
package mu.zz.zagen.morse;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author zagen
 *
 */
/*
 * 
 */
public class ResultListAdapter extends BaseAdapter {

	private Context 		context;
	private JSONArray 		resultJSONArray ;
	private LayoutInflater	inflater;
	private float firstFieldTextSize = 30, secondFieldTextSize = 30;
	
	//for fitting text into button;
	private float getFitTextSize(Typeface typeface, String text, int desiredWidth)
	{
		final float defaultTextSize = 7f;
		
		if(desiredWidth < 10)
			return defaultTextSize;
		
		Paint paint = new Paint();
		Rect bounds = new Rect();
		paint.setTypeface(typeface);
		float textSize = 180;

		paint.setTextSize(textSize);
		paint.getTextBounds(text, 0, text.length(), bounds);
		while (bounds.width() > desiredWidth)
		{
			textSize--;
			paint.setTextSize(textSize);
			paint.getTextBounds(text, 0, text.length(), bounds);
		}

		return (--textSize > 30) ? 30 : textSize;

	}
		
	public ResultListAdapter(Context context, String jsonsource, int screenWidth){
		this.context = context;
		inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		try {
			resultJSONArray = new JSONArray(jsonsource);
			int indexOfMaxFieldFirst = 0;
			int indexOfMaxFieldSecond = 0;
			for(int i = 1; i < resultJSONArray.length(); i++){
				if(resultJSONArray.getJSONObject(i).getString(MainActivity.TEST_VALUE).length() 
						> resultJSONArray.getJSONObject(indexOfMaxFieldFirst).getString(MainActivity.TEST_VALUE).length())
					indexOfMaxFieldFirst = i;
				if(resultJSONArray.getJSONObject(i).getString(MainActivity.INPUT_VALUE).length() 
						> resultJSONArray.getJSONObject(indexOfMaxFieldSecond).getString(MainActivity.INPUT_VALUE).length())
					indexOfMaxFieldFirst = i;
			}
			
			
			View view = inflater.inflate(R.layout.result_list_item, null);
			TextView firstField = (TextView)view.findViewById(R.id.result_generating_seq);
			firstFieldTextSize = getFitTextSize(firstField.getTypeface(),
					resultJSONArray.getJSONObject(indexOfMaxFieldFirst).getString(MainActivity.TEST_VALUE),
					screenWidth/2 - 10);
			TextView secondField= (TextView)view.findViewById(R.id.result_inputed_seq);
			secondFieldTextSize = getFitTextSize(secondField.getTypeface(),
					resultJSONArray.getJSONObject(indexOfMaxFieldSecond).getString(MainActivity.INPUT_VALUE),
					screenWidth/2 - 10);
			
			
		} catch (JSONException e) {
			resultJSONArray = null;
			e.printStackTrace();
		}
		
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		int count = 0;
		if(resultJSONArray != null)
			count = resultJSONArray.length();
		return count;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public String getItem(int arg0) {
		
		String stringItem;
		try {
			stringItem = resultJSONArray.getJSONObject(arg0).getString(MainActivity.TEST_VALUE) +  " " +
					   resultJSONArray.getJSONObject(arg0).getString(MainActivity.INPUT_VALUE);
		} catch (JSONException e) {
			stringItem = "";
			e.printStackTrace();
		}
		return stringItem;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = inflater.inflate(R.layout.result_list_item, null);
		
		TextView genTxtView = (TextView)view.findViewById(R.id.result_generating_seq);
		TextView inpTxtView = (TextView)view.findViewById(R.id.result_inputed_seq);
		genTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, firstFieldTextSize);
		inpTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, secondFieldTextSize);
		try {
			genTxtView.setText(resultJSONArray.getJSONObject(position).getString(MainActivity.TEST_VALUE).toUpperCase());
			inpTxtView.setText(resultJSONArray.getJSONObject(position).getString(MainActivity.INPUT_VALUE).toUpperCase());
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		return view;
	}

}
