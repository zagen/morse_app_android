package mu.zz.zagen.morse.Transmitters;

import mu.zz.zagen.morse.SettingsActivity;
import mu.zz.zagen.morse.General.TextCodec;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.ToneGenerator;

public class SoundTransmitter extends Transmitter {
	
	private int volume;
	

	private ToneGenerator tg;
	
	private boolean isPause = false;

	public SoundTransmitter(final Context context, TextCodec codec) {
		super(context, codec);
		SharedPreferences settings = getContext().getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		volume = settings.getInt(SettingsActivity.VOLUME, 10);
		tg = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
}
	public SoundTransmitter(final Context context, TextCodec codec, Transmitter.OnTransmittanceEndedListener listener) {
		super(context, codec, listener);
		SharedPreferences settings = getContext().getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		volume = settings.getInt(SettingsActivity.VOLUME, 10);
		tg = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
}
	@Override
	public void transmit() {
		if(isPause){
			isPause = false;
			return;
		}
		final String encodedText = getCodec().getEncodedText(); 
		
		stopFlag = false;
		if(encodedText.length() == 0)
		{
			//Toast.makeText(getContext(), getContext().getResources().getString(R.string.empty_encoded_text), Toast.LENGTH_LONG).show();
			if(transmittanceEndedListener != null)
				transmittanceEndedListener.end();
			return;
		}
		
		
		Thread playThread = new Thread(new Runnable() {
			@Override
				public void run() {
					
					stopFlag = false;
					int currentPos = 0;
					AudioManager audio = (AudioManager) getContext()
				            .getSystemService(Context.AUDIO_SERVICE);
					
					int ringerMode = audio.getRingerMode();
					
					audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
					int duration = getDuration();

					while(currentPos < encodedText.length() && !stopFlag)
					{
						if(!isPause){
							try {
								if(encodedText.charAt(currentPos) == '-')
								{				
									
									int ss = duration * 3;
									
									tg.startTone(ToneGenerator.TONE_DTMF_1, ss);
									Thread.sleep(ss);
									tg.stopTone();
									Thread.sleep(duration);
								}else{ if(encodedText.charAt(currentPos) == '.')
								{
									tg.startTone(ToneGenerator.TONE_DTMF_1, duration);
									Thread.sleep(duration);
									tg.stopTone();
									Thread.sleep(duration);
									  					
								}else{ if(encodedText.charAt(currentPos) == ' '){
									Thread.sleep(duration*3);
								}else{
									Thread.sleep(duration*7);
									    
									}}}
							 } catch (InterruptedException e) {
							      e.printStackTrace();
							}
							currentPos++;
						}
					}
					
					audio.setRingerMode(ringerMode);
					if(transmittanceEndedListener != null)
						transmittanceEndedListener.end();
				}
			});
		playThread.start();
	}


	//accessors for volume 
	public void setVolume(final int vol)
	{
		if(vol >= 0 && vol <= 100)
			volume = vol;
	}
	
	public int getVolume()
	{
		return volume;
	}
	@Override
	public void pause() {
		isPause = true;
		
	}

}


