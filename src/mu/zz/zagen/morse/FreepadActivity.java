/**
 * 
 */
package mu.zz.zagen.morse;

import mu.zz.zagen.morse.General.TextCodec;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author zagen
 * activity for inputing text with touchpod 
 */
public class FreepadActivity extends Activity implements View.OnClickListener{
	/**
	 * buttons for deleting last symbol and sharing text
	 */
	private Button deleteSymbolBtn,  okBtn;
	
	/**
	 * here displays inputed text
	 */
	private TextView messageTxtView;
	
	/**
	 * for translating inputed Morse code into text
	 */
	private TextCodec textCodec;
	
	/**
	 * button for switching locale
	 */
	private Button localeButton;
	
	/**
	 * current locale for codec
	 */
	private int currentLocale;
	
	/**
	 * id of choosing local menu
	 */
	public static final int LOCALE_MENU_ITEM = 2001;
	
	/**
	 * id of rus item menu
	 */
	public static final int LOCALE_RUS_ITEM = 2011;
	
	/**
	 * id of international locale item menu
	 */
	public static final int LOCALE_INT_ITEM = 2012;
	
	/**
	 * touchpad for input
	 */
	private TouchPad						touchPad = null;
	
	/*
	 * maybe memory leak ... TODO check later
	 */
	/**
	 * handler that receive dash/dot and translate it with codec into current locale text
	 */
	@SuppressLint("HandlerLeak")
	private final Handler 					handler = new Handler(){
		/**
		 * decoded into current locale character
		 */
		private String decodedCharacter="";
		
		/**
		 * here store inputed morse code
		 */
		private StringBuilder strBuilder = new StringBuilder();
		@Override
		public void handleMessage (Message msg){
			Bundle data = msg.getData();
			char character = data.getChar(TouchPad.MESSAGE_DATA_CHARACTER);
			decodedCharacter = "";
			//check if symbol space
			//here decoding received dash/dots and add decoded symbol into our message
			if(character == TouchPad.LONG_DELAY_SYMBOL){
				decodedCharacter = " ";
				
			}else if(character == ' '){
				if(strBuilder.length() != 0){
					textCodec.setMorse(strBuilder.toString().trim());
					strBuilder.delete(0, strBuilder.length());
					decodedCharacter = textCodec.getDecodedText();
				}
			}else{
				//otherwise put it into current morse code sequence
				strBuilder.append(character);
				
			}
			//put decoded character into message shown on the screen
			messageTxtView.setText(messageTxtView.getText() + decodedCharacter);
			
		}


	};

	/**
	 * app storage for preferences
	 */
	private SharedPreferences preferences;

	/**
	 * need for changing and saving volume
	 */
	private AudioManager am;
	
	/**
	 * switch locale method
	 * @param locale setting locale
	 */
	private void setLocaleText(int locale){
		if(localeButton == null)
			return;
		
		if(locale == TextCodec.RUS){
			localeButton.setText(getString(R.string.cyr));
		}else{
			localeButton.setText(getString(R.string.inter));
		}
	}
	
	/**
	 * creating activity
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//no title,fullscreen
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.freepad_layout);
		deleteSymbolBtn = (Button)findViewById(R.id.freepad_deletelastsymbol_btn);
		
		okBtn = (Button)findViewById(R.id.freepad_send_btn);
		
		touchPad   = (TouchPad)findViewById(R.id.freepad_touchpad);
		touchPad.setHandler(handler);
		deleteSymbolBtn.setText("\u2190");
		
		// sdk version bellow 11 doesnt support symbol of envelope
		if (Build.VERSION.SDK_INT >= 11)
			okBtn.setText("\u2709");
	    else
	    	okBtn.setText("\u2714");
		
		Button cleanButton = (Button)findViewById(R.id.freepad_clean);
		cleanButton.setText("\u21BA");
		messageTxtView = (TextView)findViewById(R.id.freepad_message_txtview);
		textCodec = new TextCodec();
		
		///get preferences for app
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		
		currentLocale = preferences.getInt(SettingsActivity.LOCALE, 0);
		textCodec.setLocale(currentLocale);
		localeButton = (Button)findViewById(R.id.freepad_locale_button);
		setLocaleText(currentLocale);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	@Override
	public void onClick(View v) {
		String message;
		switch(v.getId()){
		case R.id.freepad_deletelastsymbol_btn:
			message = (String) messageTxtView.getText();
			if(message.length() > 0)
				messageTxtView.setText(message.substring(0, message.length()-1));
			break;
		
		
		case R.id.freepad_send_btn:
			message = (String)(messageTxtView.getText());
			if(message.trim().length() == 0){
				finish();
			}
			else{
				//sharing action if message isnt empty 
	    		final Intent intent = new Intent(Intent.ACTION_SEND);
	    		intent.setType("text/plain");
	    		intent.putExtra(Intent.EXTRA_TEXT, message.trim());
	    		startActivity(Intent.createChooser(intent, getString(R.string.share_with_your_friend)));
			}
			break;
		case R.id.freepad_clean:
			//clean all the message. Before show alert dialog
			AlertDialog.Builder cleaningAdb = new AlertDialog.Builder(this);
			cleaningAdb.setTitle(getString(R.string.a_you_sure_about_cleaning))
					.setNegativeButton(getString(R.string.cancel), null)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							messageTxtView.setText("");
						}
					})
					.create()
					.show();
			
			break;
		case R.id.freepad_handbook_button:
			//open handbook activity
			Intent handbookActivityIntent = new Intent();
			handbookActivityIntent.setClass(this, HandbookActivity.class);
			startActivity(handbookActivityIntent);
			break;
		case R.id.freepad_locale_button:
			//change locale for codec and change local button label
			if(currentLocale == TextCodec.RUS){
				currentLocale = TextCodec.INT;
			}else{
				currentLocale = TextCodec.RUS;
			}
				
			textCodec.setLocale(currentLocale);
			setLocaleText(currentLocale);

			break;
		}
	}
	
	/**
	 * items for choosing local menu
	 */
	private MenuItem itemInternational,itemRussian;
	
	/**
	 * creating menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//menu contain one submenu for select locale
		SubMenu subMenu = menu.addSubMenu(R.string.pr_locale);
		//two checkable item
		itemInternational = subMenu.add(LOCALE_MENU_ITEM, LOCALE_INT_ITEM, 0, R.string.latin);
		itemRussian = subMenu.add(LOCALE_MENU_ITEM, LOCALE_RUS_ITEM, 0, R.string.cyrillic);
		itemInternational.setCheckable(true);
		itemRussian.setCheckable(true);
		int locale = preferences.getInt(SettingsActivity.LOCALE, 0);
		if(locale == 0){
			itemInternational.setChecked(true);
		}else
			itemRussian.setChecked(true);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		if(item.getItemId() == LOCALE_INT_ITEM){
			item.setChecked(true);
			itemRussian.setChecked(false);
			currentLocale = TextCodec.INT;
			textCodec.setLocale(currentLocale);
		}
		if(item.getItemId() == LOCALE_RUS_ITEM){
			item.setChecked(true);
			currentLocale = TextCodec.RUS;
			textCodec.setLocale(currentLocale);
			itemInternational.setChecked(false);
			
		}
		setLocaleText(currentLocale);
		return super.onMenuItemSelected(featureId, item);
	}
	@Override
	protected void onPause() {
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
		
		super.onPause();
	}

}
