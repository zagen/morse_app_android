package mu.zz.zagen.morse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mu.zz.zagen.morse.General.Generator;
import mu.zz.zagen.morse.General.NumberChooseDialog;
import mu.zz.zagen.morse.General.TextCodec;
import mu.zz.zagen.morse.General.Timer;
import mu.zz.zagen.morse.Transmitters.LightTransmitter;
import mu.zz.zagen.morse.Transmitters.SoundTransmitter;
import mu.zz.zagen.morse.Transmitters.ToneTransmitter;
import mu.zz.zagen.morse.Transmitters.Transmitter;
import mu.zz.zagen.morse.Transmitters.VibroTransmitter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

public class ReceiveCheckActivity extends Activity implements OnClickListener {

	/**
	 * animation of radio working 
	 */
	private AnimationDrawable 	animation = null;
	
	/**
	 * text field in which will show correct answer
	 */
	private TextView 			rightAnswerTxtView;
	
	/**
	 * edit text field for input received message
	 */
	private EditText 			input;
	
	/**
	 * need to encode/decode into Morse code
	 */
	private TextCodec			codec 		= new TextCodec();
	
	/**
	 * transmitter with help which play encoded message
	 */
	private Transmitter			transmitter = null;
	
	/**
	 * generator for random message
	 */
	private Generator 			generator ;
	
	/**
	 * here store generated text (size of array = number of group)
	 */
	private String[] 			text;
	
	/**
	 * number of playing now group
	 */
	private int 				currentPlayingGroup;
	
	/**
	 * here show whether correct answer or not
	 */
	private TextView			statusRightTxtView;
	
	/**
	 * set true if playing Morse code is in progress
	 */
	private Boolean				transmittanceInProccess = false;

	/**
	 * here put answer for furthermore showing it in result activity;
	 */
	private JSONArray 			resultJSONArray = new JSONArray();
	
	/**
	 * array of possible transmitters
	 */
	private SparseArray<Transmitter>	transmitters =  new SparseArray<Transmitter>();
	
	/**
	 * set of all character for current local
	 */
	private String 				abc;
	
	/**
	 * current set of character for generating sequence
	 */
	private String 				currentSymbols;
	
	/**
	 * current locale
	 */
	private int 				locale = 0;
	
	/**
	 * Type of training: receiving words or random sequences
	 */
	private int 				type;
	
	/**
	 * count of generating group
	 */
	private int 				groupCount = 1;
	
	/**
	 * need for choosing group count from menu options
	
	private int 				groupCountTempValue = groupCount; */
	
	/**
	 * need for getting/setting volume of sounds
	 */
	private AudioManager am;
	
	/**
	 * settings repo
	 */
	private SharedPreferences preferences;
	
	private int delay = 0;
	
	
	/**
	 *  generating new sequence and refill all UI elements
	 */
	private void refill(){
		currentPlayingGroup = 0;
		//clear input EditText
		input.setText("");
		input.clearFocus();
		//hide soft keyboard
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
		
		//generate text sequence in accordance with generate sequence length and group count
		for(int i = 0; i < groupCount; i++){
			text[i] = generator.toString();
		}
		
		//set generated text to codec
		codec.setText(text[currentPlayingGroup]);
		
		//hide TextView that show correct answer
		rightAnswerTxtView.setVisibility(View.INVISIBLE);
		statusRightTxtView.setVisibility(View.INVISIBLE);
		
		//make toast)
		Toast.makeText(this, R.string.new_sequence_generated, Toast.LENGTH_SHORT)
			 .show();

	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//make activity fullscreen and without any titlebar and actiobar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//set content from layout
		setContentView(R.layout.rca_layout);
		
		preferences = getSharedPreferences(SettingsActivity.PREFS_NAME, 0);
		final int duration = preferences.getInt(SettingsActivity.DURATION, 50); 
		//put in sparse array all possible transmitters
		Transmitter.OnTransmittanceEndedListener onTransmittanceEndedListener = new Transmitter.OnTransmittanceEndedListener() {
			@Override
			public void end() {
					currentPlayingGroup++;
					//if all groups played end transmission
					if(currentPlayingGroup >= groupCount){
						currentPlayingGroup = 0;
						transmittanceInProccess = false;
						animation.stop();
					}else{
						//if transmission of word ended wait period between two words
						try {
							Thread.sleep(duration * 7);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						//set next group
						if(transmittanceInProccess){//need for prevent external changing some of instance value of RecieveCheckActivity class
							codec.setText(text[currentPlayingGroup]);
							transmitter.transmit();
						}
					}	
			}
		};
		transmitters.put(0, new ToneTransmitter(this, codec, onTransmittanceEndedListener));
		transmitters.put(1, new SoundTransmitter(this, codec, onTransmittanceEndedListener));
		transmitters.put(2, new LightTransmitter(this, codec, onTransmittanceEndedListener));
		transmitters.put(3, new LightTransmitter(this, codec, onTransmittanceEndedListener));
		transmitters.put(4, new VibroTransmitter(this, codec, onTransmittanceEndedListener));
		((LightTransmitter)transmitters.get(2)).setUseCameraFlashLight(true);
		((LightTransmitter)transmitters.get(3)).setUseCameraFlashLight(false);
		
		//set text on refill and ok buttons
		Button refillButton = (Button)findViewById(R.id.rca_refill_button);
		refillButton.setText("\u21BA");
		Button okButton = (Button)findViewById(R.id.rca_ok_button);
		okButton.setText("\u2714");
		
		//get from device physical width
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		int width = displaymetrics.widthPixels;
		
		// based on width calculate margin and side of button
		int margin = (int)(.05 * width); 
		int sideOfButton = (int)(width *  .2) ;
		Button enterBtn = (Button)findViewById(R.id.rca_enter_button);
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) enterBtn.getLayoutParams();

		//set buttons height and width from calculated above sideOfButton
		layoutParams.width = sideOfButton;
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		enterBtn.setLayoutParams(layoutParams);
		
		//likewise above
		input = (EditText)findViewById(R.id.rca_input);
		input.setTextSize(sideOfButton/5);
		layoutParams = (RelativeLayout.LayoutParams)input.getLayoutParams();
		layoutParams.width = (int)(width* .6);
		layoutParams.height = sideOfButton;
		layoutParams.setMargins(margin, margin, margin, margin);
		input.setLayoutParams(layoutParams);
		
		rightAnswerTxtView = (TextView)findViewById(R.id.rca_answer);
		rightAnswerTxtView.setTextSize(getResources().getDimension(R.dimen.rca_right_answer_text_size));
		rightAnswerTxtView.setMovementMethod(new ScrollingMovementMethod());
		
		Button radioBtn = (Button)findViewById(R.id.rca_radio);
		radioBtn.setBackgroundResource(R.anim.radio);
		animation = (AnimationDrawable)radioBtn.getBackground();
		
		width = width * 3 / 4;
		RelativeLayout.LayoutParams layoutParamsR = (RelativeLayout.LayoutParams) radioBtn.getLayoutParams();
		layoutParamsR.width = width - 10;
		layoutParamsR.height = width*3/4 ;
		radioBtn.setLayoutParams(layoutParamsR);
		
		statusRightTxtView = (TextView)findViewById(R.id.rca_status_answer);
		
		
		generator 	= new Generator(this);
		//get index value of transmitter from preferences 
		

		int valueOfTransmitter = preferences.getInt(SettingsActivity.TRANSMITTER, 0);
		transmitter = transmitters.get(valueOfTransmitter);
		
		//get size
		int size = preferences.getInt(SettingsActivity.GEN_LENGTH, 2);
		generator.setSize(size);
		
		//get locale 
		locale = preferences.getInt(SettingsActivity.LOCALE, 0);
		generator.setLocale(locale);

		//set symbols for generating
		if(locale == TextCodec.RUS){
			abc = "��������������������������������1234567890.,:;()\"'-/?!";
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_RUS, "��������������������������������");
		}else
		{
			abc = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!"; 
			currentSymbols = preferences.getString(SettingsActivity.SYMBOLS_INT, "abcdefghijklmnopqrstuvwxyz");
		}
		generator.setAbc(currentSymbols);
		
		type = preferences.getInt(SettingsActivity.SEQUENCE_TYPE, Generator.CHARS);
		generator.setSequenceType(type);
	
		//get groups amount
		groupCount = preferences.getInt(SettingsActivity.GROUP_NUMBER, 1);
		//groupCountTempValue = groupCount;
		
		delay = preferences.getInt(SettingsActivity.DELAY_BEFORE_RECEIVE_EXER, 0);
		
		text = new String[groupCount];
		
		refill();

		Toast.makeText(this, R.string.receive_greetings, Toast.LENGTH_SHORT).show();
		
		
		//get audio service for changing volume
		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		//get volume from settings
		int volume = preferences.getInt(SettingsActivity.VOLUME, 10);
		//and set it for current volume 
		am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	/**
	 * called when activity again shows on the screen
	 */
	@Override
	public void onResume()
	{
		super.onResume();
		transmittanceInProccess = false;
		if(animation != null)
			animation.stop();

	}

	@Override
	public void onPause(){
		super.onPause();
		transmittanceInProccess = false;
		transmitter.stop();
		
		//when activity get invisible ask current volume and save it into app settings 
		int volume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(SettingsActivity.VOLUME, volume);
		editor.commit();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rca_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		//
		case R.id.action_symbols:
			String [] strItems = new String[abc.length()];
			final boolean[] boolItems= new boolean[abc.length()];
			//forming arrays for multiList 
			for(int i = 0; i < abc.length(); i++){
				strItems[i] = String.valueOf(abc.toCharArray()[i]);
				boolItems[i] = (currentSymbols.contains(strItems[i])) ? true : false;
			}
			
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setTitle(R.string.symbol_dialog_title)
				.setMultiChoiceItems(strItems, boolItems, new DialogInterface.OnMultiChoiceClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						boolItems[which] = isChecked;
						
					}
				})
				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SharedPreferences.Editor editor = preferences.edit();
						StringBuilder strBuilder = new StringBuilder();
						for(int i = 0; i < abc.length();i++){
							if(boolItems[i]){
								strBuilder.append(abc.toCharArray()[i]);
							}
						}
						if(locale == TextCodec.RUS)
							editor.putString(SettingsActivity.SYMBOLS_RUS, strBuilder.toString());
						else
							editor.putString(SettingsActivity.SYMBOLS_INT, strBuilder.toString());
						currentSymbols = strBuilder.toString();
						generator.setAbc(currentSymbols);
						refill();
						editor.commit();
					}
				})
				.create().show();
			break;
		case R.id.action_message_type:
			
			AlertDialog.Builder adb_type = new AlertDialog.Builder(this);
			adb_type.setSingleChoiceItems(getResources().getTextArray(R.array.message_types), type, new DialogInterface.OnClickListener() {
				
							@Override
							public void onClick(DialogInterface dialog, int which) {
								type = which;					
							}
						})
					.setTitle(R.string.type_dialog_title)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SharedPreferences.Editor editor = preferences.edit();
							editor.putInt(SettingsActivity.SEQUENCE_TYPE, type);
							generator.setSequenceType(type);
							refill();
							editor.commit();
						}
					})
					.create()
					.show();
			break;

		case R.id.action_count_group:
			

			new NumberChooseDialog(this).setRange(1, SettingsActivity.MAX_GROUP_NUMBER)
										.setOnFinishListener(new NumberChooseDialog.OnFinishDialogListener() {
											
											@Override
											public void onFinish(int number) {
												groupCount = number;
												SharedPreferences.Editor editor = preferences.edit();
												editor.putInt(SettingsActivity.GROUP_NUMBER, groupCount);
												text = new String[groupCount];
												refill();
												editor.commit();
											}
										})
										.setDefault(groupCount)
										.setTitle(R.string.group_number_dialog_title)
										.show();
			break;
		case R.id.action_delay:
			new NumberChooseDialog(this).setRange(0, SettingsActivity.MAX_DELAY_BEFORE_RECEIVE_EXER)
										.setOnFinishListener(new NumberChooseDialog.OnFinishDialogListener() {
											
											@Override
											public void onFinish(int number) {
												if(number <= SettingsActivity.MAX_DELAY_BEFORE_RECEIVE_EXER){
													SharedPreferences.Editor editor = preferences.edit();
													editor.putInt(SettingsActivity.DELAY_BEFORE_RECEIVE_EXER, number);
													editor.commit();
													delay = number;
												}
											}
										})
										.setDefault(delay)
										.setTitle(getString(R.string.delay_before_begin_of_transmission) + "( 0 - " 
															+ Integer.toString(SettingsActivity.MAX_DELAY_BEFORE_RECEIVE_EXER)
															+ " )")
										.show();
			break;
		default:
			break;
		}
		return true;
	}
	@SuppressLint("DefaultLocale")
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.rca_ok_button:
			Intent intent = new Intent();
			intent.putExtra(MainActivity.RESULT, resultJSONArray.toString());
			setResult(RESULT_OK, intent);
			finish();
			break;
		
		case R.id.rca_enter_button: 
			//check if inputed text equal generated and add results in JSONResultArray
			if(!transmittanceInProccess){
				StringBuilder sb = new StringBuilder();
				for(String oneline : text){
					sb.append(oneline).append(" ");
				}
				
				//replace  because '�' and 'E' have the same Morse code
				String textGenerated = sb.toString().trim().toUpperCase().replace('�', '�');
				String textInputed = input.getText()
										  .toString()
										  .replace('�', '�')
										  .replace('�', 'E')
										  .replaceAll("[\\s]{1,}", " ")
										  .trim()
										  .toUpperCase();
				
				//check if generated text and inputed are the same
				if(!textInputed.equals(textGenerated)){

					//show right answer
					rightAnswerTxtView.setVisibility(View.VISIBLE);
					rightAnswerTxtView.setText(compareToSpannable(textGenerated.split(" "), 
							textInputed.split(" ")), BufferType.SPANNABLE);
					//rightAnswerTxtView.setText(sb.toString().trim().toUpperCase().replace(' ', '\n'));
					statusRightTxtView.setVisibility(View.VISIBLE);
					
					//hide soft keyboard
					InputMethodManager imm = (InputMethodManager)getSystemService(
						      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
					
					Toast.makeText(this, R.string.wrong_answer, Toast.LENGTH_SHORT).show();
					
					//split inputed and generated texts if it need so for adding into result JSON array
					String[] textValuesGenerated = textGenerated.split(" ");
					String[] textValuesInputed   = textInputed.split(" ");
					
					
					for(int i = 0; i < textValuesGenerated.length; i++){
						//each string add into JSON array
						JSONObject inputResult = new JSONObject();
						try {
							inputResult.put(MainActivity.TEST_VALUE, textValuesGenerated[i]);
							if(i < textValuesInputed.length)
								inputResult.put(MainActivity.INPUT_VALUE, textValuesInputed[i]);
							else
								inputResult.put(MainActivity.INPUT_VALUE, "");
							
							resultJSONArray.put(inputResult);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}else
				{
					String[] textValuesGenerated = textGenerated.split(" ");
					String[] textValuesInputed   = textInputed.split(" ");
					
					for(int i = 0; i < textValuesGenerated.length; i++){ //because answer is correct both string array have the same size, so we don't need correct if they have so
						JSONObject inputResult = new JSONObject();
						try {
							inputResult.put(MainActivity.TEST_VALUE, textValuesGenerated[i]);
							inputResult.put(MainActivity.INPUT_VALUE, textValuesInputed[i]);
														
							resultJSONArray.put(inputResult);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					//automatically refill generated text and UI elements
					refill();
					Toast.makeText(this, R.string.answer_is_correct, Toast.LENGTH_SHORT).show();
				}
				
				
			}else{
				Toast.makeText(this, getResources().getString(R.string.cant_use_before_transmission_is_over), 
						Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.rca_radio:
			if(!transmittanceInProccess){
				
				transmittanceInProccess = true;
				currentPlayingGroup = 0;
				codec.setText(text[currentPlayingGroup]);
				rightAnswerTxtView.setVisibility(View.INVISIBLE);
				statusRightTxtView.setVisibility(View.INVISIBLE);
				
				animation.start();

				//if choose transmitter - with screen combine all group into one string and put it into codec for transmission
				if(transmitter instanceof LightTransmitter){
					LightTransmitter lightTransmitter = (LightTransmitter)transmitter;
					if(!lightTransmitter.getUseCameraFlashLight()){
						StringBuilder sb = new StringBuilder();
						for(int i = 0; i < text.length; i++){
							sb.append(text[i]);
						}
						codec.setText(sb.toString().trim());
					}
				}
				
				new Timer(delay*1000, new Timer.OnTimerClockedListener() {
					
					@Override
					public void onTimerClocked() {
						transmitter.transmit();
						
					}
				}).run();
				

			}else{
				Toast.makeText(this, getResources().getString(R.string.cant_use_before_transmission_is_over), 
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.rca_refill_button:
			if(!transmittanceInProccess){
				refill();

			}else{
				Toast.makeText(this, getResources().getString(R.string.cant_use_before_transmission_is_over), 
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.rca_handbook_button:
			if(!transmittanceInProccess){
				Intent handbookActivityIntent = new Intent();
				handbookActivityIntent.setClass(this, HandbookActivity.class);
				startActivity(handbookActivityIntent);
			}else{
				Toast.makeText(this, getResources().getString(R.string.cant_use_before_transmission_is_over), 
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.rca_settings_button:
			openOptionsMenu();
			break;
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(transmittanceInProccess){
			Toast.makeText(this, getResources().getString(R.string.cant_use_before_transmission_is_over), 
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Compare each inputed string to the source one. If source character is different apart from  it h
	 * @param source
	 * @param inputed
	 * @return
	 */
	private SpannableStringBuilder compareToSpannable(String[] source, String[] inputed){
		SpannableStringBuilder ssb = new SpannableStringBuilder();
		int minWordsAmount = (source.length < inputed.length) ? source.length : inputed.length;
		
		int count = 0;
		int i;
		for(i = 0; i < minWordsAmount; i++){
			ssb.append(source[i]);
			int minLength = (source[i].length() < inputed[i].length()) ? source[i].length() : inputed[i].length();
			for(int j = 0; j < minLength; j++){
				if(source[i].charAt(j) != inputed[i].charAt(j))
					ssb.setSpan(new ForegroundColorSpan(Color.GRAY), count + j, count + j + 1, 0);
			}
			
			for(int j = minLength; j < source[i].length(); j++){
				ssb.setSpan(new ForegroundColorSpan(Color.GRAY), count + j, count + j + 1, 0);
			}
			ssb.append('\n');
			count += source[i].length() + 1;
			
		}
		for(; i < source.length;i++){
			ssb.append(source[i]);
			for(int j = 0; j < source[i].length(); j++){
				ssb.setSpan(new ForegroundColorSpan(Color.GRAY), count + j, count + j + 1, 0);
			}
			ssb.append('\n');
			count += source[i].length() + 1;
			
		}
		ssb.delete(count - 1, count);
		
		return ssb;
	}
	
}
